;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;;; Disable lockfiles to avoid bad Hugo interaction
;;; ref https://github.com/gohugoio/hugo/issues/6773
((nil . ((create-lockfiles . nil))))
