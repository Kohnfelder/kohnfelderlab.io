# Log: convert Hugo site into dirt

* Start with a copy, as is, set up for Hugo.

* First rough run needs work but it ran in about 1 second (faster than Hugo).

# Features

Features required for conversion, or suggested by the exercise

* Webpages named without ".html" suffix

* Blog directory index webpage linking to blog entries, showing excerpts

## Site config

* favicon = "/favicon.ico"
> `<link href='https://example.com/favicon.png' rel='icon' type='image/x-icon'/>`


* [meta.name] 

	* description = "text"
	* author = "text"
	>   `<meta name="description" content="text"/>`

* [meta.property] 

	* "name" = "text"
	>  `<meta property="name" content="text"/>`


