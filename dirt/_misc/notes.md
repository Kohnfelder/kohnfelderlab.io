TODO

pandoc -t markdown -o /tmp/ch1.md ~/DSS/book1-ms/copyedit/DesignSecureSoftware_c01_final.odt 
Periodic links check === links.py content/page/references.md 
Timeline: write entries (details below)
More FAQ
Authors are free to publish excerpts from their books, so long as no individual excerpt represents more than 10% of the entire work. We also ask that excerpts include appropriate references to the book and to No Starch Press as the publisher. 


LATER
(content for when the book is publishing)
* If you ~~bought~~*plan to buy* the book, thank you!
Check out supplementary material ~~here~~*coming soon*, including references and errata.

* If you are curious about the book, explore anything of interest here to learn more.

LINX
https://www.trapkit.de/books/bhd/    No Starch book companion site
https://gitlab.com/bkmgit/11ty       Eleventy static site generator


TOPICS
Writing a book for software pros means writing a book your friends and family cannot read.

CONTENT
Pulled references here from LMKA2 pre-prod file.
Accepted changes and simplified formatting in DOCX before converting.
pandoc supp/LMKA2_References_11.docx -o content/page/references.md
Fair amount of hand editing was needed to cleanup the converted MD file.

SETUP
Install hugo via rpi UI. (v0.55)
Fork Hugo from https://gitlab.com/pages/hugo
Local server   http://localhost:1313/
GitLab hosted  https://kohnfelder.gitlab.io/
GitLab project https://gitlab.com/Kohnfelder/kohnfelder.gitlab.io

REF
https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/
https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_forked_sample_project.html
https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/
https://gitlab.com/pages/hugo
https://gohugo.io/getting-started/usage/
https://www.markdownguide.org/basic-syntax/

BOOK TIMELINE
V0.95: Dec 2019 - Drop in new material - sayonara, Google Docs!
V0.94: Restructure to add code; remove 3rd party content
v0.93 : Added Bad behavior section, integer overflow
V0.92 Nov 2019
V0.89 Oct 2019
V0.85 Sep 2019
V0.81 Aug 2019
V0.94: 20 December 2019 - Restructure to add code
V0.93: 24 November 2019 - Added Bad behavior section, integer overflow
V0.92: 11 November 2019 (61K words)
V0.91: 7 November 2019
O’Reilly book proposal first draft: 5 November 2019 to John Devins
V0.89: 16 October 2019 - restructure: move Motivations, split Security Design & Review
V0.88: 15 October 2019 - Lots of feedback from Adam & John G
V0.85: 20 September 2019 - Add expanded Realization & Future chapters
V0.81: 20 August 2019 - Complete draft
(76 pages) May 2019
(52 pages) Mar 2019
V0.7: March 2019 - Restructure & Rewrite
V0.5: November 2018 - No Starch Publishing draft
V0.41: 11 October 2018 - Add Threat Modeling section.
V0.4: 5 October 2018 - Thorough review by Jon.
V0.31: 28 September 2018 - Add design document update
V0.3: 20 August 2018 - Review again from the top after taking a month away
V0.23: 16 July 2018 - Add section: Avoid Predictability
V0.22: 4 July 2018 - Principles reviewed (19.6K words)
V0.21: 25 June 2018 - Principles reviewed with minor additions (19.5K words)
V0.2: 7 June 2018 - Draft complete (18K words)
V0.1: 27 April 2018 - Introduction and Outline


BLOG EVENTS
./doit 2018-11-13 first-try 'Bill - love to see your manuscript so far.'
./doit 2018-11-27 too-short 'Bill - too short'
./doit 2019-09-30 proposal-submitted 'Bill MS? (same day response) 45K words'
./doit 2019-10-04 phone-bill 'Phone Bill (Python target)'
./doit 2019-10-16 negotiation 'Decline (books already out, lack of experience, reconsider my proposal which is design centric)'
./doit 2019-11-19 phone-again 'Phone Bill/Frances. Agreed to add coding chapters.'
./doit 2019-12-18 contract-signed 'Contract'
./doit 2020-01-03 author-guide 'Start with author guide etc.'
./doit 2020-01-21 exec-editor-chat 'Audio chat Barbara\'
./doit 2020-03-10 covid-19 'covid-19...'
./doit 2020-03-25 drive-share 'shared cloud folder'
./doit 2020-04-27 developmental-edit 'start developmental edit - color blind, styling problems, chapter order, minimum length 1/2" spine, use of repetition'
./doit 2021-02-08 manuscript-complete 'Tech review near complete (MS just over 100K words)'
./doit 2021-02-10 begin-production 'Begin prod with Katrina - copy edit start with 2 chapters (1/4);  graphic art; layout as chapters complete; proofreader; author PDF review is final - scheduling'
./doit 2021-02-10 foreward-request 'Foreword by Adam'
./doit 2021-02-11 index-marking 'LibreOffice indexing support'
./doit 2021-02-18 cover-design-ideas 'Bill cover design begins'
./doit 2021-02-19 developmental-edit-wrap 'Developmental wrap up appendices and preface done!'
./doit 2021-02-22 technical-review-done 'Tech review done!'
./doit 2021-02-22 copy-edit-starts 'Copy editor Rachel start'
./doit 2021-02-23 cover-design-concept 'Cover design concept'
./doit 2021-03-04 indexing-questions 'Indexing questions'
./doit 2021-03-16 marketing-questionnaire 'Marketing questionnaire'
./doit 2021-03-16 artwork-review 'Artwork ready for review'

./doit 2021-01-12 custom-shading 'Custom shading to highlight substrings'
./doit 2021-04-30 copy-edit-complete 'Copy editing complete'
./doit 2021-05-06 title-info-sheet 'Title Information Sheet review'
./doit 2021-05-10 appendix-design 'Layout design for Appendix A'
./doit 2021-05-20 book-cover 'Title, subtitle, and cover design'
./doit 2021-05-27 proofreading-process 'Reviewing proofreader changes'
./doit 2021-07-05 publication-date 'Publication date set'

REFERENCE LINKS

16 July 2021
$ links.py content/page/references.md
https://en.wikipedia.org/wiki/Data_flow_diagramxsg
      lost None
https://www.cloudflare.com/learning/security/glossary/bgp-hijacking/
    LOOKS OK
https://leginfo.legislature.ca.gov/faces/billTextClient.xhtml?bill_id=201720180SB327
 different LOOKS OK, reformatted
https://leginfo.legislature.ca.gov/faces/billTextClient.xhtml?bill_id=201720180SB327
http://archive.defense.gov/Transcripts/Transcript.aspx?TranscriptID=2636
    OBSOLETE: deleted
https://www.nature.com/articles/477404a
   changed FALSE ERROR DUE TO COOKIES
https://twitter.com/thezdi/status/1240360442310074370
 different LOOKS OK, reformatted
 https://twitter.com/thezdi/status/1240360442310074370
https://medium.com/lorenkohnfelder/unpatchable-security-flaws-8941c0cce790
   changed https://medium.com/@lorenkohnfelder/unpatchable-security-flaws-8941c0cce790
https://securephones.io/
    broken None
https://medium.com/@lorenkohnfelder/lets-move-beyond-the-ominous-going-dark-f2f20a932ca9
   changed https://medium.com/@lorenkohnfelder/lets-move-beyond-the-ominous-going-dark-f2f20a932ca9
https://rethinksecurity.io/posts/a-conversation-with-loren-kohnfelder/
      lost None

HPM 339.4 scene
“Computer! Painkillers”
“Additional dose available in three hours and four minutes.”
I frown. “Computer: What is the current time?”
“Seven-fifteen P.M., Moscow Standard Time.”
“Computer: Set time to eleven P.M. Moscow Standard Time.”
“Clock set complete.”
“Computer: painkillers.”
The arms hand me a package of pills and a bag of water. I gobble them down. What a stupid system. Astronauts trusted to save the world but not to monitor their painkiller doses? Stupid.


SUMMARY TOC


# PART I: CONCEPTS

### 1 Foundations
fundamentals, trust, Classic Principles, Gold Standard, privacy
### 2 Threats
adversaries, The Four Questions framework, threat modeling, assets, attack surfaces, trust boundaries, privacy and threat modeling beyond security
### 3 Mitigation
addressing threats, strategies, attack surface minimization, windows of vulnerability, data exposure, access policy and controls, interfaces, communication, storage
### 4 Patterns
Design Attributes, Economy of Design, Transparent Design, Exposure Minimization, Least Privilege, Least Information, Secure by Default, Allowlists over Blocklists, Avoid Predictability, Fail Securely, Strong Enforcement, Complete Mediation, Least Common Mechanism, Redundancy, Defense in Depth, Separation of Privilege, Trust and Responsibility, Reluctance to Trust, Accept Security Responsibility, anti-patterns: Confused Deputy, Backflow of Trust, Third-Party Hooks, Unpatchable Components
### 5 Cryptography
using crypto tools, random numbers, Message Authentication Codes to prevent tampering and replay attacks, symmetric and asymmetric encryption, digital signatures and certificates, key exchange


# PART II: DESIGN

### 6 Secure Design
security in design, assumptions, scope, requirements, integrating threat modeling, building mitigations, designing interfaces, data handling, privacy protections, lifecycle, trade-offs, simplicity
### 7 Security Design Review
review logistics, benefits, timing, documentation, review process in detail, assessment, Four Questions guidance, where to dig, privacy reviews, updates, managing disagreement, communicating, escalations, practice

# PART III: IMPLEMENTATION

### 8 Secure Programming
malicious influence, bugs and vulnerabilities, vulnerability chains, entropy, GotoFail, footguns, atomicity, timing attacks, serialization
### 9 Low-Level Coding Flaws
fixed-width integer and floating point vulnerabilities, safe arithmetic, memory allocation and access vulnerabilities, Heartbleed
### 10 Untrusted Input
input validation, validation criteria, rejecting and correcting invalid input, character string length and Unicode vulnerabilities, SQL injection, path traversal, regular expressions, XML vulnerabilities, mitigation strategies
### 11 Web Security
frameworks, Web security model, HTTP protocol, digital certificates and HTTPS, Same Origin policy, cookies, cross-site scripting (XSS) and request forgery (CSRF), and more
### 12 Security Testing
GotoFail example, functional testing, security test cases, input validation testing, XSS testing, fuzz testing, security regression testing, availability testing, resource consumption, threshold testing, best practices, Test-Driven Development, leveraging integration testing, catching up
### 13 Secure Development Best Practices
code quality and hygiene, exception and error handling, documentation, security code reviews, dependencies, securing interfaces, triage, DREAD assessments and triage decisions, secure development environment, software release

# Additional content

### Afterword
call to action, everyone’s job, baking in security, future security, software quality, managing complexity, transparency, authenticity, trust, responsibility, delivering the last mile, conclusion

### App. A - Sample Design Document
a private data logging component design document that demonstrates how security is built into a software design

### App. B - Glossary
definitions of security terminology used in the book

### App. C - Exercises
suggestions for further study and practice using the ideas presented in each chapter

### App. D - Cheat Sheets
classic security principles, Gold Standard, the Four Questions, STRIDE threat taxonomy, secure design patterns and anti-patterns, security design review process outline, DREAD vulnerability assessment


BONEYARD

[config.toml]
[[menu.main]]
    identifier = "samples"
    name = "Samples"
    weight = 2

[[menu.main]]
    parent = "samples"
    name = "Big Image Sample"
    url = "post/2017-03-07-bigimg-sample"
    weight = 1

[[menu.main]]
    parent = "samples"
    name = "Math Sample"
    url = "post/2017-03-05-math-sample"
    weight = 2

[[menu.main]]
    parent = "samples"
    name = "Code Sample"
    url = "post/2016-03-08-code-sample"
    weight = 3

