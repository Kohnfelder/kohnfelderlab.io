---
title: Custom domain for this website
date: 2021-08-26
tags: ["website"]
draft: false
---

It's time to point the domain name `designingsecuresoftware.com`
at this website. For reference, the docs I referenced for this are noted below.

The DNS record updates by the registrar were fast, gitlab domain verification
worked first time, and the integrated Let's Encrypt feature got a 
domain-validated (OID 2.23.140.1.2.1) certificate 
for the website within minutes.

After writing about Let's Encrypt (Chapter 11) it's nice to see it working
smoothly just as advertised. Flipping a switch in the settings was all it took
to get the certificate -- it was easier than editing the DNS records 
for the website hosting.

https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/

https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html

