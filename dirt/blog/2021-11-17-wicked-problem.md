---
title: "A Wicked Problem"
type: "article"
date: 2021-11-17
tags: ["security"]
---

A wicked problem is one that is difficult to even clearly describe because
of its diffuse and interconnected nature, and this is a useful lens to
view software security. How are we doing overall at software security? How
do you even define the standard to measure against?

Vulnerabilities are commonplace in most commercial software and the CVE
counts grow year over year, but that’s hardly an accurate metric for
the state of the industry as a whole. Ransomware is huge these days, but
is that a technology failing or just a powerful business model promoting
attack for profit? Security updates of proprietary platforms rarely provide
much detail at all, so how do we know what happened, much less how to
improve? Software security is a wicked problem for all these reasons,
and that makes getting a handle on it all the more difficult.

As my new book on the subject begins shipping, I'm thinking back on
the writing process which helped me to glimpse the hard parts of the
challenge. While I like to imagine that I have an intuitive idea about
software and how it’s made from my years working in the industry, things
change quickly and there is precious little public information. Who are
the several million (in the US alone) writing code, what are they doing
exactly, and are they competent at good security practices? I don’t
think anyone really knows, so in the end I fell back on writing about
what I know from personal experience. Keeping it as concise as possible,
just that ran 300 pages.

