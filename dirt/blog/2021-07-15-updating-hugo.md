---
title: Updating Hugo
date: 2021-07-15
tags: ["website"]
draft: false
---

Hugo has worked well for me building this static website overall. However, it remains generally somewhat mysterious and there have been some bumps.

After updating the `hugo` command it flagged several deprecated features baked into the layout template (beautifulhugo) so I found and updated that which fixed it.

The newer version of Hugo (0.85) does appear to have a new bug ([6773](https://github.com/gohugoio/hugo/issues/6773)). The basic problem is that one of the killer features of Hugo doesn't play well with Emacs. Hugo tracks file changes to thoroughly that it sees Emacs writing a temporary file (during `c-X c-S` file updates) and tries to process it, but since this file is immediately renamed -- which makes it disappear -- Hugo stumbles and fails with an awkward error message and a file that no longer even exists. This is one of those bugs that seem insoluable where two pieces of software dig in their heels -- or in this case, the big one is surely ignoring this issue -- and neither will budge. To my sensibility, Hugo needs to change, but that doesn't seem to be the prevailing view. The issue tracker does suggest workaround, and I found one that works.

With these bumps ironed out things seem to be working fine again.
