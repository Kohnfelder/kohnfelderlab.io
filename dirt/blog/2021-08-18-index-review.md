---
title: Index review
date: 2021-08-18
tags: ["publishing"]
draft: false
---

The penultimate review for the book is the index, and I just hit Save on it.
Creating the index was a surprisingly interesting as well as challenging part
of writing the book, and I'm very glad to have tackled it myself. 
Having taken book indexes for granted myself, I have a new appreciation.

During the copy editing phase I added index entries to the document (ODT format)
and the copy editor checked those and often added some as well.
(I assumed that often it was easier to just do some indexing than explain it.)

The publisher provided very specific guidance on indexing, and it certainly
is an art. I think of indexing as akin to software design in that it requires
anticipating how the user/reader will use it and then providing the necessary
UI/entries they will look for to accomplish what they want to do/find. 
Different people certainly use indexes in various ways, so part of the job
is to provide multiple paths to the same end. For example, the book includes
many secure design patterns, and these are listed by name and also as
sub-entries under "patterns".

I reviewed the index in several passes corresponding to issues to look for.

* Spelling: are all the words correctly spelled? (I used the `hunspell(1)` command and also scanned it manually.)

* Acronyms: consistent use where applicable (usually in parens after the spelled out words).

* Duplications: any duplications were cut or the entries combined.

* Styling: index entry styling should match usage in the text. 
(For example, HTTP header names appear in monospace literal font 
in the text and in the index.)

* "See" references: if there is a reference to another entry with just 
one page reference we provide that page number directly so as not to 
waste the reader's time.

* Visual: one last review to check that everything looks right, 
or to catch anything else that pops out.

What to index and how to construct the index entries is ultimately subjective
and a matter of taste. In the process I developed my own sensibilities while
trying to follow all the detailed guidance. 
It's tempting to consider software tools to aid if not largely automate the 
process, but it is an art and depends on understanding the material,
so as of yet there are no professional grade software solutions far as I know.
With luck my knowledge of the subject matter will help me make the right choices
more than my inexperience limits. 
