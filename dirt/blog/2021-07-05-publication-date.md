---
title: Publication date set
date: 2021-07-05
tags: ["publishing"]
draft: false
---

This Monday morning I started the week by discovering that a publication date is set for the book: October 19, 2021.

Three months out, there is a lot of work left to do reviewing proof pages and the index, but at the same time it should be very doable.

I used a Google Alert search with an RSS reader to track web references to my name, and this is how I learn the big news. One benefit of having an unusual last name and unpopular first name is that search references are (so far) always about me. The first webpage that I happened to stumble upon was [this](https://www.penguinrandomhouse.com/books/696989/designing-secure-software-by-loren-kohnfelder/).

Having a specific date makes the publication of the book feel very real for the first time.
