---
title: "Best Software Security Books of All Time"
date: 2021-11-04
tags: ["promo"]
---

I was surprised to see that my book, "Designing Secure Software:
A Guide for Developers", made it to BookAuthority's Best Software
Security Books of All Time ... especially since the first copies are
still being printed and not yet shipping! (The e-book is available from
the publisher at nostarch.com so it is getting out there in that form.)
https://bookauthority.org/books/best-software-security-books?t=2u40nr&s=award&book=1718501927

I'm unfamiliar with BookAuthority which seems to generate their selections
based on data signals, and for some reason they don't have the cover
graphic yet. Flattering as this is, "One of the best Software Security
books of all time" this early seems a little much. 
