---
title: Artwork ready for review
date: 2021-03-16
tags: ["publishing"]
draft: false
---

The artwork came back as a PDF with one drawing per page, labeled by filename.
The first batch was pretty good, considering that Richard (I learned the artist's name)
must have had no idea what the symbols meant.

I added annotations suggesting some changes, and it's now off to be worked out.
I'm not sure if I'll next see artwork in the layout pages or get another batch to review.

The manuscript contains production directive lines saying where the graphics are to go,
and the copy editor has diligently added comments about letter casing and such details.


