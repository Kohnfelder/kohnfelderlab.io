---
title: Book on sale tomorrow
date: 2021-12-21
tags: ["publishing"]
draft: false
---

Finally my software security book is available for general sale tomorrow (December 21, 2021). 
Recent events such as the Log4j vulnerability demonstrate that our work is far from over 
(and the latest Microsoft security update lists 35 CVE). 

In times when we click a button and then the book of our choice promptly appears, 
this year has been especially challenging. 
So I’m dedicating this announcement to everyone who made my book possible: 
the publisher, editors, proofreaders, designers, and many others at No Starch Press; 
the distributors; the printers; the truck drivers; the warehouse operations; 
the paper and ink suppliers; numerous booksellers large and small; and many more no doubt.

https://designingsecuresoftware.com/ 

