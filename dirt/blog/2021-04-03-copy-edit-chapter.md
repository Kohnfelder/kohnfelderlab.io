---
title: Copy edit of a chapter
date: 2021-04-03
tags: ["publishing"]
draft: false
---

Copy edit is the last chance to make significant changes to the text.
Most chapters involve plenty of sentence rewriting and a few moves of a
paragraph, but much of the delta is punctuation and word choice.

There are a handful of changes that I have made notes to weave in during
this phase where I have a new story to tell that I just recently learned,
or perhaps a new approach to a concept has occurred to me after the fact.
Working on a book really dials the mind into this material, and when I
read about a new exploit or other new development that fits into the work
it's worth considering incorporating it when it adds real value.

Chapter 10 was by far the most changed during copy edit, partly by plan,
and also because the copy editor's comments triggered some reassessment
of the organization and coverage. This is a tricky chapter because the
subject of untrusted inputs is very pervasive, potentially appearing in
a variety of forms, so choosing what to write about -- or not -- is a challenge.

I replaced what I thought was a lame made-up story demonstrating the
basic idea of injection attacks with the quirky story of Dmitry Agarkov.
Additionally, I heard about the sneaky software team named "No Game Scheduled"
and wanted to include that too. This was in addition to some tweaks in the
path traversal section to provide a more systematic view of mitigations.

As if that wasn't enough, while reviewing the first round copy edit I found
a bug in my path traversal code that allowed an unlikely but possible attack.
After testing the attack and confirming it on the code I had written to verify
it, I then adjusted the code to the correct fix, tested it, and modified the
code in the book.

Kudos to the copy editor gamely reviewing and editing all of this churn.
Most importantly, these changes made the book better without causing great
disruption to the process.
