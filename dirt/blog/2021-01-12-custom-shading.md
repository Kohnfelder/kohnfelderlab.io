---
title: Custom shading to highlight substrings
date: 2021-01-12
tags: ["publishing"]
draft: false
---

The section of the book on SQL injection needs to illustrate
how an attack string gets inserted into a text string representing
a SQL statement with unexpected results. 
The standard way of highlighting a part of a string available was 
to use **bold** like this:

INSERT INTO Grades (name, grade) VALUES ('**Robert', 'A+');--**', 'F');

However, it's difficult to clearly distinguish a bold single quote, for example, because it's so thin. 

**Edit:** Again, the No Starch folks were eager to find a solution. I won't attempt to match the font but here's how it appears in proof pages:

![example of shading in code excerpt](/img/LiteralCustom-example.png)

Now it's crystal clear.


