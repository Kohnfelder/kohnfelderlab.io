---
title: Coming Soon
date: 2021-10-13
tags: ["promo"]
draft: false
---

Awaiting the release of any book requires patience, but this year 
amidst numerous supply chain challenges it's particularly uncertain.

The original October target date is almost here, but I can report that
the publisher hopes to have copies of the print edition for sale in 
early November 
-- about a month ahead of general release now set for December 2021.

The e-book is already on sale [from the publisher](
https://nostarch.com/designing-secure-software), and if you pre-order the print book now they do immediately provide the free e-book as well.

It was a nice surprise to see the book sitting at #24 in
[Amazon Hot New Releases in Software Design & Engineering](
https://www.amazon.com/gp/new-releases/books/491316)
but since this updates on an hourly basis no doubt that will vary.
