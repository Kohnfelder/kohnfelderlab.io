<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/style/pico.min.css">
    <title>C-I-A (confidentiality, integrity, and availability) — Designing Secure Software</title>
  </head>
  <body>
    <main class="container">
      <h1><a href="/.">Designing Secure Software </a></h1>
<p><nav>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/page/about.html">About</a></li>
    <li><a href="/blog/">Blog</a></li>
    <li><a href="/page/text.html">Book text</a></li>
    <li><a href="/page/contents.html">Contents</a></li>
    <li><a href="/page/errata.html">Errata</a></li>
    <li><a href="/page/media.html">Media</a></li>
    <li><a href="/page/references.html">References</a></li>
    <li><a href="https%3A//nostarch.com/download/samples/Designing_Secure_Software_index.pdf">Index</a></li>
    <li><a href="/page/faq.html">FAQ</a></li>
  </ul>
</nav>
</p><a href="/text/ch0-preface.html">['Preface']</a>&nbsp;&nbsp;<a href="/text/ch1-classic.html">['Classic security principles']</a>
      <p><strong>The following is an excerpt from the
book <em>Designing Secure Software: A Guide for Developers</em> by
Loren Kohnfelder, Copyright 2022,
<a href="https://nostarch.com/designing-secure-software">No Starch Press</a></strong></p>
<hr />
<p>We traditionally build software security on three basic principles of 
information security: confidentiality, integrity, and availability. 
Formulated around the fundamentals of data protection, 
the meanings of the three pillars are intuitive:</p>
<p><strong>Confidentiality</strong>:
Allow only authorized data access—don’t leak information.</p>
<p><strong>Integrity</strong>: 
Maintain data accurately—don’t allow unauthorized modification or
deletion.</p>
<p><strong>Availability</strong>:
Preserve the availability of data—don’t allow significant delays or 
unauthorized shutdowns.</p>
<p>Each of these brief definitions describes the goal and defenses against
its subversion. In reviewing designs, it’s often helpful to think of ways one
might undermine security, and work back to defensive measures.</p>
<p>All three components of C-I-A represent ideals, and it’s crucial to avoid
insisting on perfection. For example, an analysis of even solidly encrypted
network traffic could allow a determined eavesdropper to deduce something 
about the communications between two endpoints, like the volume of
data exchanged. Technically, this exchange of data weakens the confidentiality 
of interaction between the endpoints; but for practical purposes, we
can’t fix it without taking extreme measures, and usually the risk is minor
enough to be safely ignored. (One way to conceal the fact of communication 
is for endpoints to always exchange a constant volume of data, adding
dummy packets as needed when actual traffic is lower.)</p>
<p>Notice that authorization is inherent in each component of C-I-A,
which mandates only the right disclosures, modifications of data, or controls
of availability. What constitutes “right” is an important detail, and an 
authorization policy needs to specify that, 
but it isn’t part of these fundamental data
protection primitive concepts. </p>
<h3>Confidentiality</h3>
<p>Maintaining confidentiality means disclosing private information in only an
authorized manner. This sounds simple, but in practice it involves a number
of complexities.</p>
<p>First, it’s important to carefully identify what information to consider
private. Design documents should make this distinction clear. While what
counts as sensitive might sometimes seem obvious, it’s actually surprising
how people’s opinions vary, and without an explicit specification, we risk
misunderstanding. The safest assumption is to treat all externally collected
information as private by default, until declared otherwise by an explicit
policy that explains how and why the designation can be relaxed.
Here are some oft-overlooked reasons to treat data as private:</p>
<ul>
<li>
<p>An end user might naturally expect their data to be private, unless
informed otherwise, even if revealing it isn’t harmful.</p>
</li>
<li>
<p>People might enter sensitive information into a text field intended for a
different use.</p>
</li>
<li>
<p>Information collection, handling, and storage might be subject to laws
and regulations that many are unaware of. (For example, if Europeans
browse your website, it may be subject to EU law, such as the General Data
Protection Regulation.)</p>
</li>
</ul>
<p>When handling private information, determine what constitutes proper
access. Deciding when and how to disclose information is ultimately a trust
decision, and it’s worth not only spelling out the rules, but also explaining
the subjective choices behind those rules.</p>
<p>Compromises of confidentiality happen on a spectrum. In a complete
disclosure, attackers acquire an entire dataset, including metadata. At the
lower end of the spectrum might be a minor disclosure of information,
such as an internal error message or similar leak of no real consequence.
As an example of a partial disclosure, consider the practice of assigning
sequential numbers to new customers: a wily competitor can sign up as a
new customer and get a new customer number from time to time, then
compute the successive differences to learn the numbers of customers
acquired during each interval. Any leakage details about protected data
is to some degree a confidentiality compromise.</p>
<p>It’s so easy to underestimate the potential value of minor disclosures.
Attackers might put data to use in a completely different way than the
developers originally intended, and combining tiny bits of information can
provide more powerful insights than any of the individual parts on their
own. Learning someone’s ZIP code might not tell you much, but if you also
know their approximate age and that they’re an MD, you could perhaps
combine this information to identify the individual in a sparsely populated
area -- a process known as deanonymization or reidentification. By analyzing a
supposedly anonymized dataset published by Netflix, researchers were able
to match numerous user accounts to IMDb accounts: it turns out that your
favorite movies are a very effective means of unique personal identification.</p>
<h3>Integrity</h3>
<p>Integrity, used in an information security context, is simply the authenticity
and accuracy of data, kept safe from unauthorized tampering or removal.
In addition to protecting against unauthorized modification, an accurate
record of the provenance of data—the original source, and any authorized
changes made—can be an important, and stronger, assurance of integrity.</p>
<p>One classic defense against many tampering attacks is to preserve versions 
of critical data and record their provenance. Simply put, keep good
backups. Incremental backups can be excellent mitigations because they’re
simple and efficient to put in place and provide a series of snapshots that
detail exactly what data changed, and when. However, the need for integrity
goes far beyond the protection of data, and often includes ensuring the
integrity of components, server logs, software source code and versions,
and other forensic information necessary to determine the original source
of tampering when problems occur. In addition to limited administrative
access controls, secure digests (similar to checksums) and digital signatures
are also strong integrity checks.</p>
<p>Bear in mind that tampering can happen in many different ways, not
necessarily by modifying data in storage. For instance, in a web application,
tampering might happen on the client side, on the wire between the client
and server, by tricking an authorized party into making a change, by modi-
fying a script on the page, or in many other ways.</p>
<h3>Availability</h3>
<p>Attacks on availability are a sad reality of the internet-connected world and
can be among the most difficult to defend against. In the simplest cases, the
attacker may just send an exceptionally heavy load of traffic to the server,
overwhelming it with what looks like valid uses of the service. This principle
implies that information is temporarily unavailable; while data that is perma-
nently lost is also unavailable, this is generally considered to be 
fundamentally a compromise of integrity.</p>
<p>Anonymous denial-of-service (DoS) attacks, often for ransom, threaten
any internet service, posing a difficult challenge. To best defend against such
attacks, host on large-scale services with infrastructure that stands up to
heavy loads, and maintain the flexibility to move infrastructure quickly in the
event of problems. Nobody knows how common or costly DoS attacks really
are, since many victims resolve these incidents privately. But without a doubt,
you should create detailed plans in advance to prepare for such incidents.</p>
<p>Many other kinds of availability threats are possible as well. For a web
server, a malformed request that triggers a bug, causing a crash or infinite 
loop, can devastate its service. Other attacks can also overload the
storage, computation, or communication capacity of an application, or perhaps 
use patterns that break the effectiveness of caching, all of which pose
serious issues. Unauthorized destruction of software, configuration, or data
(even with backup, delays can result) also can adversely impact availability.</p>
      <p>Copyright 2022 Loren Kohnfelder -- 
<a href="https://linkedin.com/in/kohnfelder">LinkedIn</a>
<a href="/blog.xml">RSS</a></p>
    </main>
  </body>
</html>
