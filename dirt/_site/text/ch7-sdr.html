<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/style/pico.min.css">
    <title>Security Design Review — Designing Secure Software</title>
  </head>
  <body>
    <main class="container">
      <h1><a href="/.">Designing Secure Software </a></h1>
<p><nav>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/page/about.html">About</a></li>
    <li><a href="/blog/">Blog</a></li>
    <li><a href="/page/text.html">Book text</a></li>
    <li><a href="/page/contents.html">Contents</a></li>
    <li><a href="/page/errata.html">Errata</a></li>
    <li><a href="/page/media.html">Media</a></li>
    <li><a href="/page/references.html">References</a></li>
    <li><a href="https%3A//nostarch.com/download/samples/Designing_Secure_Software_index.pdf">Index</a></li>
    <li><a href="/page/faq.html">FAQ</a></li>
  </ul>
</nav>
</p><a href="/text/ch4-trust-patterns.html">['Trust and Responsibility Patterns']</a>&nbsp;
      <p><strong>The following is an excerpt from the
book <em>Designing Secure Software: A Guide for Developers</em> by
Loren Kohnfelder, Copyright 2022,
<a href="https://nostarch.com/designing-secure-software">No Starch Press</a></strong></p>
<hr />
<blockquote>
<p>"A good, sympathetic review is always a wonderful surprise."
---Joyce Carol Oates</p>
</blockquote>
<p>One of the best ways to bake security into software is to separately
review designs with your "security hat" on. This chapter explains how to
apply the security and privacy design concepts discussed in the last
chapter in a <em>security design review</em> <em>(SDR)</em>. Think of this process as
akin to when an architect designs a building and an engineer then
reviews the design to ensure that it's safe and sound. Both the designer
and the reviewer need to understand structural engineering and building
codes, and by working together, they can achieve higher levels of
quality and trust.</p>
<p>Ideally, the security reviewer is someone not involved in the design
work, giving them distance and objectivity, and also someone familiar
with the systems and context within which the software runs and how it
will be used. However, these are not firm prerequisites; reviewers less
familiar with the design will tend to ask a lot more questions but can
also do a fine job.</p>
<p>Sharing these methods and encouraging more software professionals to
perform SDRs themselves was one of my core goals in writing this book.
You will almost certainly do a better SDR on the software systems that
you work with and know well than someone with more security experience
who is unfamiliar with those systems. This book provides guidance to
help you with this task, and it's my hope that in doing so it will
contribute in some small way to raising the bar for software security.</p>
<h2>The SDR Process</h2>
<p>The following explanation of the SDR process describes how I conducted
them at a large software company with a formal, mandatory review
process. That said, software design is practiced in countless different
ways, and you can adapt the same strategies and analysis to less formal
organizations.</p>
<p>Starting from a clear and complete design in written form, the SDR
consists of six stages:</p>
<ul>
<li>
<p><em>Study</em> the design and supporting documents to gain a basic
understanding of the project.</p>
</li>
<li>
<p><em>Inquire</em> about the design and its basic threats.</p>
</li>
<li>
<p><em>Identify</em> the most security critical parts of the design for closer
attention.</p>
</li>
<li>
<p><em>Collaborate</em> with the designer(s) to identify risks and discuss
mitigations.</p>
</li>
<li>
<p><em>Write</em> a summary report of findings and recommendations.</p>
</li>
<li>
<p><em>Follow</em> subsequent design changes to confirm resolution before signing
off.</p>
</li>
</ul>
<p>For small designs, you can often run through most of these in one
session; for larger designs, break up the work by stage, with some
stages possibly requiring multiple sessions to complete. Sessions
dedicated to meeting with the design team are ideal, but if necessary
the reviewer can work alone and then exchange notes and questions with
the design team via email or other means.</p>
<p>Everyone has a different style. Some reviewers like to dive in and do a
"marathon." I prefer (and recommend) working incrementally over several
days, affording myself an opportunity to "sleep on it," which is often
where my best thinking comes from.</p>
<p>The following walkthrough of the SDR process explains each stage, with
bullet points summarizing useful techniques. When you perform an SDR you
can refer to the bullets for each stage as you work through the process.</p>
<h3>1. Study</h3>
<p>Study the design and supporting documents to gain a basic understanding
of the software as preparation for the review. In addition to security
know-how, reviewers ideally bring domain-specific expertise. Lacking
that, try to pick up what you can, and stay curious to learn throughout
the process. Trade-offs are inherent in most security decisions, so a
single-minded push for more and more security is likely to end up
overdoing it, and risks ruining the design in the process. To understand
how too much security can be bad, think of a house designed solely to
reduce the risk of fire. Built entirely of concrete, with one thick
steel door and no windows, it would be costly as well as ugly, and
nobody would want to live in it.</p>
<p>In this preparatory stage:</p>
<ul>
<li>
<p>First, read the documentation to get a high-level understanding of the
design.</p>
</li>
<li>
<p>Next, put on your "security hat" and go through it again with a
threat-aware mindset.</p>
</li>
<li>
<p>Take notes, capturing your ideas and observations for future reference.</p>
</li>
<li>
<p>Flag potential issues for later, but at this stage it's premature to do
much security analysis.</p>
</li>
</ul>
<h3>2. Inquire</h3>
<p>Ask the designer clarifying questions to understand the basic threats to
the system. For simpler designs that are readily understood, or when the
designer has produced rock-solid documentation, you may be able to skip
this stage. Consider it an opportunity to confirm your understanding of
the design and to resolve any ambiguities or open questions before
proceeding further. Reviewers certainly don't need to know a design
inside and out to be effective---that's the designer's job---but you do
need a solid grasp of the broad outlines and how its major components
interact.</p>
<p>This stage is your opportunity to fill in gaps before digging in. Here
are some pointers:</p>
<ul>
<li>
<p>Ensure that the design document is clear and complete.</p>
</li>
<li>
<p>If there are omissions or corrections needed, help get them fixed in the
document.</p>
</li>
<li>
<p>Understand the design enough to be conversant, but not necessarily at an
expert level.</p>
</li>
<li>
<p>Ask members of the team what they worry about most; if they have no
security concerns, ask follow-up questions to learn why not.</p>
</li>
</ul>
<p>There's no need to limit the questions you ask as a security reviewer to
strictly what's in the design document. Understanding peer systems can
be extremely helpful for gauging their impact on the design's security.
Omitted details can be hardest to spot. For example, if the design
implicitly stores data without providing any details of how this is
handled, ask about the storage and its security.</p>
<h3>3. Identify</h3>
<p>Identify the security-critical parts of the design, and zero in on them
for close analysis. Work from basic principles to see through a security
lens: think in terms of C-I-A, the Gold Standard, assets, attack
surfaces, and trust boundaries. While these parts of the design deserve
special attention, keep the security review focused on the whole for
now, so as not to completely ignore the other parts. That said, it's
fine to skip over aspects of the design with little or no relevance to
security.</p>
<p>In this exploratory stage you should:</p>
<ul>
<li>
<p>Examine interfaces, storage, and communications---these will typically
be central points of focus.</p>
</li>
<li>
<p>Work inward from the most exposed attack surfaces toward the most
valuable assets, just as determined attackers would.</p>
</li>
<li>
<p>Evaluate to what degree the designer is aware of, and the design
addresses, security explicitly.</p>
</li>
<li>
<p>If needed, point out key protections, and get them called out in the
design as important features.</p>
</li>
</ul>
<h3>4. Collaborate</h3>
<p>Collaborate with the designer, conveying findings and discussing
alternatives. Ideally, the designer and reviewer meet for discussion and
go through the issues one by one. This is a learning process for
everyone: the designer gets a fresh perspective on the design while
learning about security, and the reviewer gains insights about the
design and the designer's intentions, deepening their understanding of
the security challenges and the best mitigation alternatives. The joint
goal is making the design better overall; security is the focus of the
review, but not the only consideration. There's no need make final
decisions on changes on the spot, but it is important to reach agreement
eventually about what design changes deserve consideration.</p>
<p>Here are some guidelines for effective collaboration:</p>
<ul>
<li>
<p>As a reviewer, provide a security perspective on risks and mitigations
where needed. This can be valuable even when the design is already
secure, reinforcing good security practice.</p>
</li>
<li>
<p>Consider sketching a scenario illustrating how a security change could
pay off down the line to help convince the designer of the need for
mitigations.</p>
</li>
<li>
<p>Offer more than a single solution to a problem when you can, and help
the designer see the strengths and weaknesses of these alternatives.</p>
</li>
<li>
<p>Accept that the designer gets the last word, because they are ultimately
responsible for the design.</p>
</li>
<li>
<p>Document the exchange of ideas, including what will or will not go into
the design.</p>
</li>
</ul>
<p>Expanding on "the last word": in practice, this balance will depend on
the organization and its culture, applicable industry standards,
possible regulatory requirements, and other factors. In large or highly
regimented organizations, the last word may involve sign-off by multiple
parties, including an architecture board, standards compliance officers,
usability assessors, and executive stakeholders. When multiple approvals
are required, designers must balance competing interests, so security
reviewers should be especially conscientious of this dynamic and be as
flexible as possible.</p>
<h3>5. Write</h3>
<p>Write an <em>assessment report</em> of the review findings and recommendations.
The findings are the security reviewer's assessment of the security of a
design. The report should focus on potential design changes to consider,
and an analysis of the security of the design as it stands. Any changes
the designer has already agreed to should be prominently identified as
such, and subject to later verification. Consider including priority
rankings for suggested changes, such as this simple three-level scheme:</p>
<ul>
<li>
<p><em>Must</em> is the strongest ranking, indicating there should be no choice,
and often implying urgency.</p>
</li>
<li>
<p><em>Ought</em> is intermediate: I use it to say that I, the reviewer, lean
"Must" but that it's debatable.</p>
</li>
<li>
<p><em>Should</em> is the weakest ranking for optional recommended changes.</p>
</li>
</ul>
<p>If you want more precise rankings, Chapter 13 includes guidance on ways
to systematically assign more fine-grained rankings for security bugs
that can be readily adapted for this purpose.</p>
<p>SDRs vary enough that I have never used a standardized template for the
assessment report, but instead write a narrative describing the
findings. I like to work from my own rough notes taken over the course
of the review, with the final form of the report evolving organically.
If you can hold all the details in your head reliably, then you may want
to write up the report after the review meeting.</p>
<p>The following tips can also be used as a framework for the write-up:</p>
<ul>
<li>
<p>Organize the report around specific design changes that address security
risks.</p>
</li>
<li>
<p>Spend most of your effort and ink on the highest-priority issues, and
proportionally less on lower priorities.</p>
</li>
<li>
<p>Suggest alternatives and strategies, without attempting to do the
designer's job for them.</p>
</li>
<li>
<p>Prioritize findings and recommendations, using priority rankings.</p>
</li>
<li>
<p>Focus on security, but feel free to offer separate remarks for the
designer's consideration as well. Be more deferential outside the scope
of the SDR, don't nit-pick, and avoid diluting the security message.</p>
</li>
</ul>
<p>Separating the designer and reviewer roles is important, but in practice
how this is done varies greatly depending on the responsibilities of
each and their ability to collaborate. In your assessment report, avoid
doing design work, while offering clear direction for needed changes so
the designer knows what to do. Offer to review and comment on any
significant redesign that results from the current review. As a rule of
thumb, a good reviewer helps the designer see security threats and the
potential consequences, as well as suggesting mitigation strategies,
without dictating actual design changes. Reviewers who are too demanding
often find that their advice is ineffective, even if it is correct, and
they risk forcing designers into making changes that they do not fully
understand or see the need for.</p>
<p>You can skimp on writing up the report if this level of rigor feels too
fussy, but the chances are good that you, or someone else working on the
software, will later wish that the details had been recorded for future
reference. At a bare minimum, I suggest taking the time to send an email
summary to the team for the record. 
If the design covered all the security bases, reference a few
of the most important design features that security depends on to
underscore their importance. In the case of a design where security is a
non-factor (for example, I once reviewed an informational website that
collected no private information), outline the reasoning behind that
conclusion. 
The style, length, and level of detail in these reports varies greatly
depending on the organizational culture, available time, number of
stakeholders, and many other factors. When the reviewer works closely
with the software designer, by collaborating you may be able to
incorporate needed provisions directly into the design document, rather
than enumerating issues in need of change in a report. Even for small,
informal projects, assigning separate designer and reviewer roles is
worthwhile so there are multiple sets of eyes on the work, and to ensure
that security is duly considered.</p>
<h3>6. Follow Up</h3>
<p>Follow up on agreed design changes resulting from a security review, to
confirm they were resolved correctly. When the collaboration has gone
well, I usually just check that documentation updates happened, without
looking at the implementation (and that approach has never backfired).
In other circumstances, and subject to your judgment, reviewers may need
to be more vigilant. Sign off on the review when it's complete,
including the verification of all necessary changes. Assigning the SDR
in the project bug tracker is a great way to track progress reliably.
Otherwise, use a more or less formal process if you prefer. Here are a
few pointers for this final stage:</p>
<p>For major security design changes, you might want to collaborate with
the designer to ensure that changes are made correctly.</p>
<p>Where opinions differ, the reviewer should include a statement of both
positions and the specific recommendations that weren't followed to flag
it as an open issue. (The book includes a section titled,
"Managing Disagreements" that talks about this topic in more detail.)</p>
<p>In the best case, the designer looks to the reviewer as a security
resource and will continue engaging as needed over time.</p>
      <p>Copyright 2022 Loren Kohnfelder -- 
<a href="https://linkedin.com/in/kohnfelder">LinkedIn</a>
<a href="/blog.xml">RSS</a></p>
    </main>
  </body>
</html>
