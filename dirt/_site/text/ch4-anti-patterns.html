<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/style/pico.min.css">
    <title>Anti-Patterns — Designing Secure Software</title>
  </head>
  <body>
    <main class="container">
      <h1><a href="/.">Designing Secure Software </a></h1>
<p><nav>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/page/about.html">About</a></li>
    <li><a href="/blog/">Blog</a></li>
    <li><a href="/page/text.html">Book text</a></li>
    <li><a href="/page/contents.html">Contents</a></li>
    <li><a href="/page/errata.html">Errata</a></li>
    <li><a href="/page/media.html">Media</a></li>
    <li><a href="/page/references.html">References</a></li>
    <li><a href="https%3A//nostarch.com/download/samples/Designing_Secure_Software_index.pdf">Index</a></li>
    <li><a href="/page/faq.html">FAQ</a></li>
  </ul>
</nav>
</p><a href="/text/ch2-threats.html">['Threats']</a>&nbsp;&nbsp;<a href="/text/ch4-design-patterns.html">['Design Patterns']</a>
      <p><strong>The following is an excerpt from the
book <em>Designing Secure Software: A Guide for Developers</em> by
Loren Kohnfelder, Copyright 2022,
<a href="https://nostarch.com/designing-secure-software">No Starch Press</a></strong></p>
<hr />
<blockquote>
<p>"Learn to see in another's calamity the ills which you should avoid."
---Publilius Syrus</p>
</blockquote>
<p>Some skills are best learned by observing how a master works, but
another important kind of learning comes from avoiding the past mistakes
of others. Beginning chemists learn to always dilute acid by adding the
acid to a container of water---never the reverse, because in the
presence of a large amount of acid, the first drop of water reacts
suddenly, producing a lot of heat that could instantly boil the water,
expelling water and acid explosively. Nobody wants to learn this lesson
by imitation, and in that spirit, I present here several anti-patterns
best avoided in the interests of security.</p>
<p>The following short sections list a few software security anti-patterns.
These patterns may generally carry security risks, so they are best
avoided, but they are not actual vulnerabilities. In contrast to the
named patterns covered in the previous sections, which are generally
recognizable terms, some of these don't have well-established names, so
I have chosen descriptive monikers here for convenience.</p>
<h3>Confused Deputy</h3>
<p>The <em>Confused Deputy</em> problem is a fundamental security challenge that
is at the core of many software vulnerabilities. One could say that this
is the mother of all anti-patterns. To explain the name and what it
means, a short story is a good starting point. Suppose a judge issues a
warrant, instructing their deputy to arrest Norman Bates. The deputy
looks up Norman's address, and arrests the man living there. He insists
there is a mistake, but the deputy has heard that excuse before. The
plot twist of our story (which has nothing to do with <em>Psycho</em>) is that
Norman anticipated getting caught and for years has used a false
address. The deputy, confused by this subterfuge, used their arrest
authority wrongly; you could say that Norman played them, managing to
direct the deputy's duly granted authority to his own malevolent
purposes. (The despicable crime of swatting---falsely reporting an
emergency to direct police forces against innocent victims---is a
perfect example of the Confused Deputy problem, but I didn't want to
vvvvvvtell one of those sad stories in detail.)</p>
<p>Common examples of this problem include the kernel when called by
userland code, or a web server when invoked from the internet. The
callee is a <em>deputy</em>, because the higher-privilege code is invoked to do
things on behalf of the lower-privilege caller. This risk derives
directly from the trust boundary crossing, which is why those are of
such acute interest in threat modeling. In later chapters, numerous ways
of confusing a deputy will be covered, including buffer overflows, poor
input validation, and cross-site request forgery (CSRF) attacks, just to
name a few. Unlike human deputies, who can rely on instinct, past
experience, and other cues (including common sense), software is
trivially tricked into doing things it wasn't intended to, unless it's
designed and implemented with all necessarily precautions fully
anticipated.</p>
<p>In summary, at trust boundaries, handle lower-trust data and
lower-privilege invocations with care so as not to become a Confused
Deputy. Keep the context associated with requests throughout the process
of performing the task so that authorization can be fully checked as
needed. Beware that side effects do not allow requesters to exceed their
authority.</p>
<h3>Backflow of Trust</h3>
<p>This anti-pattern is present whenever a lower-trust component controls a
higher-trust component. An example of this is when a system
administrator uses their personal computer to remotely administer an
enterprise system. While the person is duly authorized and trusted,
their home computer isn't within the enterprise regime and shouldn't be
hosting sessions using admin rights. In essence, you can think of this
as a structural Elevation of Privilege just waiting to happen.</p>
<p>While nobody in their right mind would fall into this anti-pattern in
real life, it's surprisingly easy to miss in an information system.
Remember that what counts here is not the trust you <em>give</em> components,
but how much trust the components <em>merit</em>. Threat modeling can surface
potential problems of this variety through an explicit look at trust
boundaries.</p>
<h3>Third-Party Hooks</h3>
<p>Another form of the Backflow of Trust anti-pattern is when hooks in a
component within your system provide a third party undue access.
Consider a critical business system that includes a proprietary
component performing some specialized process within the system. Perhaps
it uses advanced AI to predict future business trends, consuming
confidential sales metrics and updating forecasts daily. The AI
component is cutting-edge, and so the company that makes it must tend to
it daily. To make it work like a turnkey system, it needs a direct
tunnel through the firewall to access the administrative interface.
v
This also is a perverse trust relationship, because this third party has
direct access into the heart of the enterprise system, completely
outside the purview of the administrators. If the AI provider was
dishonest, or compromised, they could easily exfiltrate internal company
data, or worse, and there would be no way of knowing. Note that a
limited type of hook may not have this problem and would be acceptable.
For example, if the hook implements an auto-update mechanism and is only
capable of downloading and installing new versions of the software, it
may be fine, given a suitable level of trust.</p>
<h3>Unpatchable Components</h3>
<p>It's almost invariably a matter of when, not if, someone will discover a
vulnerability in any given popular component. Once such a vulnerability
becomes public knowledge, unless it is completely disconnected from any
attack surface, it needs patching promptly. Any component in a system
that you cannot patch will eventually become a permanent liability.</p>
<p>Hardware components with preinstalled software are often unpatchable,
but for all intents and purposes, so is any software whose publisher has
ceased supporting it or gone out of business. In practice, there are
many other categories of effectively unpatchable software: unsupported
software provided in binary form only; code built with an obsolete
compiler or other dependency; code retired by a management decision;
code that becomes embroiled in a lawsuit; code lost to ransomware
compromise; and, remarkably enough, code written in a language such as
COBOL that is so old that, these days, experienced programmers are in
short supply. Major operating system providers typically provide support
and upgrades for a certain time period, after which the software becomes
effectively unpatchable. Even software that is updatable may effectively
be no better if the maker fails to provide timely releases. Don't tempt
fate by using anything you are not confident you can update quickly when
needed.</p>
      <p>Copyright 2022 Loren Kohnfelder -- 
<a href="https://linkedin.com/in/kohnfelder">LinkedIn</a>
<a href="/blog.xml">RSS</a></p>
    </main>
  </body>
</html>
