<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/style/pico.min.css">
    <title>Security Testing — Designing Secure Software</title>
  </head>
  <body>
    <main class="container">
      <h1><a href="/.">Designing Secure Software </a></h1>
<p><nav>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="/page/about.html">About</a></li>
    <li><a href="/blog/">Blog</a></li>
    <li><a href="/page/text.html">Book text</a></li>
    <li><a href="/page/contents.html">Contents</a></li>
    <li><a href="/page/errata.html">Errata</a></li>
    <li><a href="/page/media.html">Media</a></li>
    <li><a href="/page/references.html">References</a></li>
    <li><a href="https%3A//nostarch.com/download/samples/Designing_Secure_Software_index.pdf">Index</a></li>
    <li><a href="/page/faq.html">FAQ</a></li>
  </ul>
</nav>
</p><a href="/text/ch10-injection.html">['Injection Attacks']</a>&nbsp;&nbsp;<a href="/text/ch2-stride.html">['Categorizing Threats with STRIDE']</a>
      <p><strong>The following is an excerpt from the
book <em>Designing Secure Software: A Guide for Developers</em> by
Loren Kohnfelder, Copyright 2022,
<a href="https://nostarch.com/designing-secure-software">No Starch Press</a></strong></p>
<hr />
<blockquote>
<p>"Testing leads to failure, and failure leads to understanding."---Burt Rutan</p>
</blockquote>
<p>To begin, it's important to define what I mean by <em>security testing</em>.
Most testing consists of exercising code to check that functionality
works as intended. Security testing simply flips this around, ensuring
that operations that should <em>not</em> be allowed aren't (an example with
code will shortly make this distinction clear).</p>
<p>Security testing is indispensable, because it ensures that mitigations
are working. Given that coders reasonably focus on getting the intended
functionality to work with normal use, attacks that do the unexpected
can be difficult to fully anticipate. The material covered in the
preceding chapters should immediately suggest numerous security testing
possibilities. Here are some basic kinds of security test cases
corresponding to the major classes of vulnerabilities 
(covered in detail in the book):</p>
<ul>
<li>
<p>Integer overflows: 
Establish permitted ranges of values and ensure that detection and
rejection of out-of-range values works.</p>
</li>
<li>
<p>Memory management problems:
Test that the code handles extremely large data values correctly, and
rejects them when they're too big.</p>
</li>
<li>
<p>Untrusted inputs:
Test various invalid inputs to ensure they are either rejected or
converted to a valid form that is safely processed.</p>
</li>
<li>
<p>Web security:
Ensure that HTTP downgrade attacks, invalid authentication and CSRF
tokens, and XSS attacks fail (see the previous chapter for details on
these).</p>
</li>
<li>
<p>Exception handling flaws:
Force the code through its various exception handling paths (using
dependency injection for rare ones) to check that it recovers
reasonably.</p>
</li>
</ul>
<p>What all of these tests have in common is that they are off the beaten
path of normal usage, which is why they are easily forgotten. And
since all these areas are ripe for attack, thorough testing makes a
big difference. Security testing makes code more secure by
anticipating such cases and confirming that the necessary protection
mechanisms always work. In addition, for security-critical code, I
recommend thorough code coverage to ensure the highest possible
quality, since bugs in those areas tend to be devastating.</p>
<p>Security testing is likely the best way you can start making real
improvements to application security, and it isn't difficult to do.
There are no public statistics for how much or how little security
testing is done in the software industry, but the preponderance of
recurrent vulnerabilities strongly suggests that it's an enormous
missed opportunity.</p>
<h3>The Limits of Security Tests</h3>
<p>Security testing aims to detect the potential major points of failure in
code, but it will never cover all of the countless ways for code to go
wrong. It's possible to introduce a vulnerability that the tests we just
wrote won't detect, but it's unlikely to happen inadvertently. Unless
test coverage is extremely thorough the possibility of crafting a bug
that slips through the tests remains; however, the major threat here is
inadvertent bugs, so a modest set of security test cases can be quite
effective.</p>
<p>Determining how thorough the security test cases need to be requires
judgment, but the rules of thumb are clear:</p>
<ul>
<li>
<p>Security testing is more important for code that is crucial to security.</p>
</li>
<li>
<p>The most important security tests often check for actions such as
denying access, rejecting input, or failing (rather than success).</p>
</li>
<li>
<p>Security test cases should ensure that each of the key steps (in our
example, the three hashes and the comparison of hashes) works correctly.</p>
</li>
</ul>
<p>Having closely examined a real security vulnerability with a simple
(if unexpected) cause, and how to security test for such
eventualities, let's consider the general case and see how we could
have anticipated this sort of problem and proactively averted it.</p>
<h3>Writing Security Test Cases</h3>
<blockquote>
<p>"A good test case is one that has a high probability of detecting an
as yet undiscovered error."---Glenford Myers</p>
</blockquote>
<p>A security test case confirms that a specific security failure does not
occur. These tests are motivated by the second of the Four Questions:
what can go wrong? This differs from <em>penetration testing</em>, where honest
people ethically pound on software to find vulnerabilities so they can
be fixed before bad actors find them, in that it does not attempt to
scope out all possible exploits. Security testing also differs from
penetration testing in providing protection against future
vulnerabilities being introduced.</p>
<p>A security test case checks that protective mechanisms work correctly,
which often involves the rejection or neutralization of invalid inputs
and disallowed operations. While nobody would have anticipated the
GotoFail bug specifically, it's easy to see that all of the if
statements in the VerifyServerKeyExchange function are critical to
security. In the general case, code like this calls for test coverage on
each condition that enforces a security check. With that level of
testing in place, when the extraneous goto creates a vulnerability, one
of those test cases will fail and call the problem to your attention.</p>
<p>You should create security test cases when you write other unit tests,
not as a reaction to finding vulnerabilities. Secure systems protect
valuable resources by blocking improper actions, rejecting malicious
inputs, denying access, and so forth. Create security test cases
wherever such security mechanisms exist to ensure that unauthorized
operations indeed fail.</p>
<p>General examples of commonplace security test cases include testing
that login attempts with the wrong password fail, that unauthorized
attempts to access kernel resources from user space fail, and that
digital certificates that are invalid or malformed in various ways are
always rejected. Reading the code is a great way to get ideas for good
security test cases.</p>
<h3>Security Regression Tests</h3>
<blockquote>
<p>"What regresses, never progresses."---Umar ibn al-Khattâb</p>
</blockquote>
<p>Once identified and fixed, security vulnerabilities are the last bugs
we want to come back and bite us again. Yet this does happen, more
often than it should, and when it does it's a clear indication of
insufficient security testing. When responding to a newly discovered
security vulnerability, an important best practice is to create a
<em>security regression test</em> that detects the underlying bug or bugs.
This serves as a handy repro (a test case that reproduces the bug or
bugs), as well as to confirm that the fix actually eliminates the
vulnerability.</p>
<p>That's the idea, anyway, but this practice seems to be less than
diligently followed, even by the largest and most sophisticated
software makers. For example, when Apple released iOS 12.4 in 2019, it
reintroduced a bug identical to one already found and fixed in iOS
12.3, immediately re-enabling a vulnerability after that door should
have been firmly closed. Had the original fix included a security
regression test case, this should never have happened.</p>
<p>It's notable that in some cases security regressions can be far worse
than new vulnerabilities. That iOS regression was particularly painful
because the bug was already familiar to the security research
community, so they quickly adapted the existing jailbreak tool built
for iOS 12.3 to work on iOS 12.4 (a jailbreak is an escalation of
privilege circumventing restrictions imposed by the maker limiting
what the user can do on their device).</p>
<p>I recommend writing the test case first, before tackling the actual
fix. In an emergency, you might prioritize the fix if it's clear-cut,
but unless you're working solo, having someone develop the regression
test in parallel is a good practice. In the process of developing an
effective regression test, you may learn more about the issue, and
even get clues about related potential vulnerabilities.</p>
<p>A good security regression test should try more than a single specific
test case that's identical to a known attack; it should be more
general. In addition to addressing the newly discovered vulnerability, it's
common that the investigation will suggest similar vulnerabilities
elsewhere in the system that might also be exploitable. Use your
superior knowledge of system internals and familiarity with the source
code to stay ahead of potential adversaries. If possible, probe for the
presence of similar bugs immediately, so you can fix them as part of the
update that closes the original vulnerability. This can be important,
since you can bet that attackers will also be thinking along these
lines, and releasing a fix will be a big clue about new ways they might
target your system. If there is no time to explore all the leads, file
away the details for investigation later, when time permits.</p>
      <p>Copyright 2022 Loren Kohnfelder -- 
<a href="https://linkedin.com/in/kohnfelder">LinkedIn</a>
<a href="/blog.xml">RSS</a></p>
    </main>
  </body>
</html>
