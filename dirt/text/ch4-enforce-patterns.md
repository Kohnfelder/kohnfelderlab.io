---
title: Strong Enforcement Patterns
weight: -440
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

These patterns concern how to ensure that code behaves by enforcing the
rules thoroughly. Loopholes are the bane of any laws and regulations, so
these patterns show how to avoid creating ways of gaming the system.
Rather than write code and reason that you don't think it will do
something, it's better to structurally design it so that forbidden
something cannot occur.

### Complete Mediation

Protect all access paths, enforcing the same access, without exception.

An obscure term for an obvious idea, Complete Mediation means securely
checking all accesses to a protected asset consistently. If there are
multiple access methods to a resource, they must all be subject to the
same authorization check, with no shortcuts that afford a free pass or
looser policy.

For example, suppose a financial investment firm's information system
policy declares that regular employees cannot look up the tax IDs of
customers without manager approval, so the system provides them with a
reduced view of customer records omitting that field. Managers can
access the full record, and in the rare instance that a non-manager has
a legitimate need, they can ask a manager to look it up. Employees help
customers in many ways, one of which is providing replacement tax
reporting documents if, for some reason, customers did not receive
theirs in the mail. After confirming the customer's identity, the
employee requests a duplicate form (a PDF), which they print out and
mail to the customer. The problem with this system is that the
customer's tax ID, which the employee should not have access to, appears
on the tax form: that's a failure of Complete Mediation. A dishonest
employee could request any customer's tax form, as if for a replacement,
just to learn their tax ID, defeating the policy preventing disclosure
to employees.

The best way to honor this pattern is, wherever possible, to have a
single point where a particular security decision occurs. This is often
known as a *guard* or, informally, a *bottleneck*. The idea is that all
accesses to a given asset must go through one gate. Alternatively, if
that is infeasible and multiple pathways need guards, then all checks
for the same access should be functionally equivalent, and ideally
implemented as identical code.


### Least Common Mechanism

Maintain isolation between independent processes by minimizing shared
mechanisms.

To best appreciate what this means and how it helps, let's consider an
example. The kernel of a multiuser operating system manages system
resources for processes running in different user contexts. The design
of the kernel fundamentally ensures the isolation of processes unless
they explicitly share a resource or a communication channel. Under the
covers, the kernel maintains various data structures necessary to
service requests from all user processes. This pattern points out that
the common mechanism of these structures could inadvertently bridge
processes, and therefore it's best to minimize such opportunities. For
example, if some functionality can be implemented in userland code,
where the process boundary necessarily isolates it to the process, the
functionality will be less likely to somehow bridge user processes.
Here, the term *bridge* specifically means either leaking information,
or allowing one process to influence another without authorization.

If that still feels abstract, consider this non-software analogy. You
visit your accountant to review your tax return the day before the
filing deadline. Piles of papers and folders cover the accountant's desk
like miniature skyscrapers. After shuffling through the chaotic mess,
they pull out your paperwork and start the meeting. While waiting, you
can see tax forms and bank statements with other people's names and tax
IDs in plain sight. Perhaps your accountant accidentally jots a quick
note about your taxes in someone else's file by mistake. This is exactly
the kind of bridge between independent parties, created because the
accountant shares the work desk as common mechanism, that the Least
Common Mechanism strives to avoid.

Next year, you hire a different accountant, and when you meet with them,
they pull your file out of a cabinet. They open it on their desk, which
is neat, with no other clients' paperwork in sight. That's how to do
Least Common Mechanism right, with minimal risk of mix-ups or nosy
clients seeing other documents.

In the realm of software, apply this pattern by designing services that
interface to independent processes, or different users. Instead of a
monolithic database with everyone's data in it, can you provide each
user with a separate database or otherwise scope access according to the
context? There may be good reasons to put all the data in one place, but
when you choose not to follow this pattern, be alert to the added risk,
and explicitly enforce the necessary separation. Web cookies are a great
example of using this pattern, because each client stores its own cookie
data independently.

