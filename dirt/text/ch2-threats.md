---
title: Threats
weight: -200
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

> "The threat is usually more terrifying than the thing itself."---Saul Alinsky

Threats are omnipresent, but we can live with them if we manage them.
Software is no different, except that we don't have the benefit of
millions of years of evolution to prepare us. That is why you need to
adopt a software security mindset, which requires you to flip from the
builder's perspective to that of the attackers. Understanding the
potential threats to a system is the essential starting point in order
to bake solid defenses and mitigations into your software designs. But
to perceive these threats in the first place, you'll have to stop
thinking about typical use cases and using the software as intended.
Instead, you must simply see it for what it is: a bunch of code and
components, with data flowing around and getting stored here and there.

For example, consider the paperclip: it's cleverly designed to hold
sheets of paper together, but if you bend a paperclip just right, it's
easily refashioned into a stiff wire. A security mindset discerns that
you could insert this wire into the keyhole of a lock to manipulate the
tumblers and open it without the key. It's worth emphasizing that
threats include all manner of ways that harm occurs. Adversarial attacks
conducted with intention are an important focus of the discussion, but
this does not mean that you should exclude other threats due to software
bugs, human error, accidents, hardware failures, and so on.

Threat modeling provides a perspective with which to guide any decisions
that impact security throughout the software development process. The
following treatment focuses on concepts and principles, rather than any
of the many specific methodologies for doing threat modeling. Early
threat modeling as first practiced at Microsoft in the early 2000s
proved effective, but it required extensive training, as well as a
considerable investment of effort. Fortunately, you can do threat
modeling in any number of ways, and once you understand the concepts,
it's easy to tailor your process to fit the time and effort available
while still producing meaningful results.

Setting out to enumerate *all* the threats and identify *all* the points
of vulnerability in a large software system is a daunting task. However,
smart security work targets incrementally raising the bar, not shooting
for perfection. Your first efforts may only find a fraction of all the
potential issues, and only mitigate some of those: even so, that's a
substantial improvement. Just possibly, such an effort may avert a major
security incident---a real accomplishment. Unfortunately, you almost
never know of the foiled attacks, and that absence of feedback can feel
disappointing. The more you flex your security mindset muscles, the
better you'll become at seeing threats.

## The Adversarial Perspective

> "Exploits are the closest thing to 'magic spells' we experience in the
real world: Construct the right incantation, gain remote control over
device."---Halvar Flake

Human perpetrators are the ultimate threat; security incidents don't
just happen by themselves. Any concerted analysis of software security
includes considering what hypothetical adversaries might try in order to
properly defend against potential attacks. Attackers are a motley group,
from script kiddies (criminals without tech skills using automated
malware) to sophisticated nation-state actors, and everything in
between. To the extent that you can think from an adversary's
perspective, that's great, but don't fool yourself into thinking you can
accurately predict their every move or spend too much time trying to get
inside their heads, like a master sleuth outsmarting a wily foe. It's
helpful to understand the attacker's mindset, but for our purposes of
building secure software, the details of actual techniques they might
use to probe, penetrate, and exfiltrate data are unimportant.

Consider what the obvious targets of a system might be (sometimes,
what's valuable to an adversary is less valuable to you, or vice versa)
and ensure that those assets are robustly secured, but don't waste time
attempting to read the minds of hypothetical attackers. Rather than
expend unnecessary effort, they'll often focus on the weakest link to
accomplish their goal (or they might be poking around aimlessly, which
can be very hard to defend against since their actions will seem
undirected and arbitrary). Bugs definitely attract attention because
they suggest weakness, and attackers who stumble onto an apparent bug
will try creative variations to see if they can really bust something.
Errors or side effects that disclose details of the insides of the
system (for example, detailed stack dumps) are prime fodder for
attackers to jump on and run with.

Once attackers find a weakness, they're likely to focus more effort on
it, because some small flaws have a way of expanding to produce larger
consequences under concerted attack (as we shall see in Chapter 8 in
detail). Often, it's possible to combine two tiny flaws that are of no
concern individually to produce a major attack, so it's wise to take all
vulnerabilities seriously. And attackers definitely know about threat
modeling, though they are working without inside information (at least
until they manage some degree of penetration).

Even though we can never really anticipate what our adversaries will
spend time on, it does make sense to consider the motivation of
hypothetical attackers as a measure of the likelihood of diligent
attacks. Basically, this amounts to a famous criminal's explanation of
why he robbed banks: "Because that's where the money is." The point is,
the greater the prospective gain from attacking a system, the higher the
level of skill and resources you can expect potential attackers to
apply. Speculative as this might be, the analysis is useful as a
relative guide: powerful corporations and government, military, and
financial institutions are big targets. Your cat photos are not.

In the end, with all kinds of violence, it's always far easier to attack
and cause harm than to defend. Attackers get to choose their point of
entry, and with determination they can try as many exploits as they
like, because they only need to succeed once. All of which amounts to
more reasons why it's important to prioritize security work: the
defenders need every advantage available.

