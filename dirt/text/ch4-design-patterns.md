---
title: Design Patterns
weight: -420
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

The first group of patterns describe at a high level what secure design
looks like: simple and transparent. These derive from the adages "keep
it simple" and "you should have nothing to hide." As basic and perhaps
obvious as these patterns may be, they can be applied widely and are
very powerful.

### Economy of Design

Designs should be as simple as possible.

Economy of Design raises the security bar because simpler designs likely
have fewer bugs, and thus fewer undetected vulnerabilities. Though
developers claim that "all software has bugs," we know that simple
programs certainly can be bug-free. Prefer the simplest of competing
designs for security mechanisms, and be wary of complicated designs that
perform critical security functions.

LEGO® bricks are a great example of this pattern. Once the design and
manufacture of the standard building element is perfected, it enables
building a countless array of creative designs. A similar system
comprised of a number of less universally useful pieces would be more
difficult to build with; any particular design would require a larger
inventory of parts and involve other technical challenges.

The Economy of Design pattern does not say that the simpler option is
unequivocally better, or that the more complex one is necessarily
problematic. In this example, \*nix ACLs are not inherently better, and
Windows ACLs are not necessarily buggy. However, Windows ACLs do
represent more of a learning curve for developers and users, and using
their more complicated features can easily confuse people as well as
invite unintended consequences. The key design choice here, which I will
not weigh in on, is to what extent the ACL designs best fit the needs of
users. Perhaps \*nix ACLs are too simplistic and fail to meet real
demands; on the other hand, perhaps Windows ACLs are overly
feature-bound and cumbersome in typical use patterns. These are
difficult questions we must each answer for our own purposes, but for
which this design pattern provides insight.

### Transparent Design

Strong protection should never rely on secrecy.

Perhaps the most famous example of a design that failed to follow the
pattern of Transparent Design is the Death Star in *Star Wars*, whose
thermal exhaust port afforded a straight shot at the heart of the battle
station. Had Darth Vader held his architects accountable to this
principle as severely as he did Admiral Motti, the story would have
turned out very differently. Revealing the design of a well-built system
should have the effect of dissuading attackers by showing its
invincibility. It shouldn't make the task easier for them. The
corresponding anti-pattern may be better known: we call it Security
by Obscurity.

This pattern specifically warns against a *reliance* on the secrecy of a
design. It doesn't mean that publicly disclosing designs is mandatory,
or that there is anything wrong with secret information. If full
transparency about a design weakens it, you should fix the design, not
rely on keeping it secret. This in no way applies to legitimately secret
information, such as cryptographic keys or user identities, which
actually would compromise security if leaked. That's why the name of the
pattern is Transparent *Design*, not Absolute Transparency. Full
disclosure of the design of an encryption method---the key size, message
format, cryptographic algorithms, and so forth---shouldn't weaken
security at all. The anti-pattern should be a big red flag: for
instance, distrust any self-anointed "experts" who claim to invent
amazing encryption algorithms that are so great that they cannot publish
the details. Without exception, these are bogus.

The problem with Security by Obscurity is that while it may help
forestall adversaries temporarily, it's extremely fragile. For example,
imagine that a design used an outdated cryptographic algorithm: if the
bad guys ever found out that the software was still using, say, DES (a
legacy symmetric encryption algorithm from the 1970s), they could easily
crack it within a day. Instead, do the work necessary to get to a solid
security footing so that there is nothing to hide, whether or not the
design details are public.

