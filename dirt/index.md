---
title: Book
image: "img/DSS-cover.png"
---

![Book cover art](/img/DSS-art.png)

*Designing Secure Software* consolidates more than twenty years of experience 
into a concise, elegant guide to improving the security of technology products. 
Written for a wide range of software professionals, it emphasizes building 
security into software design early and involving the entire team 
in the process.
 
The book begins with a discussion of core concepts, covering trust, threats, 
mitigation, secure design patterns, and cryptography. The second part, 
perhaps this book’s most unique and important contribution to the field, 
covers the process of designing and reviewing a software design with security 
considerations in mind. The final section details the most common coding flaws 
that create vulnerabilities, making copious use of code snippets written in 
C and Python to illustrate implementation vulnerabilities.
 
You’ll learn how to:

* Identify important assets, the attack surface, and the trust boundaries 
in a system
* Evaluate the effectiveness of various threat mitigation candidates
* Work with well-known mitigations and secure design patterns
* Understand and prevent vulnerabilities like XSS and CSRF, memory flaws, 
and more
* Use security testing to proactively identify vulnerabilities introduced 
into code
* Review a software design for security flaws effectively and without judgment

> “The writing in this book is very clear and easy reading, 
and the examples used are both captivating and easy to understand. 
Kohnfelder does a great job of making a point that is easy to understand, 
and most of the chapters could stand alone for developers just working 
in that one particular area.” 
> ([read the full review](https://systemoverlord.com/2021/11/24/book-review-designing-secure-software.html))
---

* [Frequently Asked Questions](/page/faq.html)

* The author's [blog](/blog) with updates and 
posts about writing and the publishing process.


