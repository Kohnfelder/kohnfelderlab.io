---
title: "Contents"
date: 2021-10-30
draft: false
---

# Overview of the book's content

The book consists of 13 chapters organized into three parts, covering concepts,
design, and implementation, plus a conclusion.

* View the [detailed table of contents](
https://nostarch.com/download/samples/Designing_Secure_Software_toc.pdf)


## Part I: Concepts
Chapters 1 through 5 provide a conceptual basis for the rest of book.

*Chapter 1: Foundations*, is an overview of information security and privacy
fundamentals. *Chapter 2: Threats*, introduces threat modeling, fleshing out
the core concepts of attack surfaces and trust boundaries in the context of
protecting assets. The next three chapters introduce valuable tools available
to readers for building secure software. *Chapter 3: Mitigations*, discusses
commonly used strategies for defensively mitigating identified threats.
*Chapter 4: Patterns*, presents a number of effective security design patterns
and flags some anti-patterns to avoid. *Chapter 5: Cryptography*, takes a 
toolbox approach to explaining how to use standard cryptographic libraries to
mitigate common risks, without going into the underlying math (which is
rarely needed in practice).

## Part II: Design

This part of the book represents perhaps its most unique and important 
contribution to prospective readers. *Chapter 6: Secure Design*, and
*Chapter 7: Security Design Reviews*, offer guidance on secure software
design and practical techniques for how to accomplish it, approaching the
subject from the designer’s and reviewer’s perspectives, respectively. In
the process, they explain why it’s important to bake security into software
design from the beginning.
These chapters draw on the ideas introduced in the first part of the
book, offering specific methodologies for how to incorporate them to build
a secure design. The review methodology is directly based on my industry
experience, including a step-by-step process you can adapt to how you work.
Consider browsing the sample design document in Appendix A while reading 
these chapters as an example of how to put these ideas into practice.

## Part III: Implementation

Chapters 8 through 13 cover security at the implementation stage and touch
on deployment, operations, and end-of-life. Once you have a secure design,
this part of the book explains how to develop software without introducing
additional vulnerabilities. These chapters include snippets of code, 
illustrating both how vulnerabilities creep into code and how to avoid them. 

*Chapter 8: Secure Programming*, 
introduces the security challenge that programmers
face and what real vulnerabilities actually look like in code. 
*Chapter 9: Low-Level Coding Flaws*, covers the foibles of 
computer arithmetic and how C-style
explicit management of dynamic memory allocation can undermine security.
*Chapter 10: Untrusted Input*, and *Chapter 11: Web Security*, cover many of
the commonplace bugs that have been well known for many years but just
don’t seem to go away (such as injection, path traversal, XSS, 
and CSRF vulnerabilities). 
*Chapter 12: Security Testing*, covers the greatly underutilized
practice of testing to ensure that your code is secure. *Chapter 13: Secure
Development Best Practices*, rounds out the secure implementation guidance, 
covering some general best practices and providing cautionary warnings 
about common pitfalls.

## Conclusion

The *Afterword* concludes the book and describes some ways that I hope it
will make a positive impact. Here I summarize the key points made in the
book, attempt to peer into the future, and offer speculative ideas that could
help ratchet software security upward, beginning with a vision for how this
book can contribute to more secure software going forward.

## Appendices

*Appendix A* is a sample design document that illustrates what security-
aware design looks like in practice.
*Appendix B* is a glossary of software security terms that appear
throughout the book.
*Appendix C* includes some open-ended exercises and questions that
ambitious readers might enjoy researching.
*Appendix D* consists of a collection of cheat sheets that summarize key
concepts and processes.

