title: "Errata"

This page is the errata for the book 
*Designing Secure Software: a guide for developers*. 

**Updated: 7 April 2022**

> "Errors of opinion may be tolerated where reason is left free to combat it."
> --- Thomas Jefferson

*(Nothing yet)*

## Note

Several readers have asked why the book doesn't cover a certain topic. 
The answer is that (after considerable thought) I don't think any book 
on a topic this complex and diverse can possibly be complete, so in the
end the choice of what to include or not is an arbitrary limitation.
For this book I decided to stick with what I have actual experience with
and know best. Since my experience all unfolds from doing a job, coverage
of the field of security is not going to be complete; however, given 
enough years I believe that I've been fortunate to work on many diverse
projects and that the basics are well covered.
