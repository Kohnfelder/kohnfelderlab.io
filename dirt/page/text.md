---
title: Book excerpts
---

__Selections from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

* [Preface](/text/ch0-preface.html)

* [Introduction](/text/ch0-intro.html)

* [Classic security principles](/text/ch1-classic.html): 
[C-I-A](/text/ch1-c-i-a.html) and 
the [Gold Standard](/text/ch1-gold.html) (Ch. 1)

* [Information privacy basics](/text/ch1-privacy.html) (Ch. 1)

* [Threats and security mindset](/text/ch2-threats.html) (Ch. 2)
    - [Categorizing Threats with STRIDE](/text/ch2-stride.html)

* [Security patterns](/text/ch4-patterns.html) (Ch. 4)
    - [Design Attributes](/text/ch4-design-patterns.html): 
	Economy of Design, Transparent Design
    - [Exposure Minimization](/text/ch4-minim-patterns.html): 
	Least Privilege, Least Information, Secure by Default, 
	Allowlists over Blocklists, Avoid Predictability, Fail Securely
    - [Strong Enforcement](/text/ch4-enforce-patterns.html): 
	Complete Mediation, Least Common Mechanism
    - [Redundancy](/text/ch4-redundancy-patterns.html): 
	Defense in Depth, Separation of Privilege
    - [Trust and Responsibility](/text/ch4-trust-patterns.html): 
	Reluctance to Trust, Accept Security Responsibility
	- [Anti-Patterns](/text/ch4-anti-patterns.html): 
	Confused Deputy, Backflow of Trust, Third-Party Hooks, Unpatchable Components

* [Security Design Review](/text/ch7-sdr.html): 
Overview and six step process (Ch. 7)

* [Injection Attacks](/text/ch10-injection.html) (Ch. 10)

* [Security Testing](/text/ch12-testing.html): 
Security tests and security regression tests (Ch. 12)
