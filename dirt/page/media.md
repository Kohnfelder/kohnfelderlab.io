title: "Media links"

# Podcast

Ted Harrington hosted for a great conversation about the proactive side 
of software security, how my book can help everyone move left, 
and why that's important.

https://tech-done-different.simplecast.com/episodes/starting-with-security-a-conversation-with-author-of-designing-secure-software-loren-kohnfelder-tech-done-different-with-ted-harrington-Ix6RSSPA

# Interviews

Q&A about the book *Designing Secure Software*:
https://nostarch.com/blog/redesigning-security-living-legend-loren-kohnfelder

*Rethink Security* interview,
speculative thoughts on future directions of security:
[*A Conversation With Loren Kohnfelder*](https://rethinksecurity.io/posts/a-conversation-with-the-father-of-pki).

# Other

You can find some of my other writing over a wide range of topics
on [Medium](https://medium.com/@lorenkohnfelder).

