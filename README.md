This project builds a static website as a companion to my book,
*Designing Secure Software* (No Starch Press).

This GitLab project is forked from the Hugo example
(the forking relationship is now disconnected).

## Useful details

* Install Hugo (start [here](https://gohugo.io/))

* Fork Hugo from [the GitLab sample project](https://gitlab.com/pages/hugo)

* Edit away and run `hugo server` to see a preview at http://localhost:1313/

* Website is hosted by GitLab (for free) at https://kohnfelder.gitlab.io/

* This GitLab project at https://gitlab.com/Kohnfelder/kohnfelder.gitlab.io

* Use the python utility ./supp/links.py to check for broken links
(a few URL it falsely finds problems with; best to compare to last scan)

    ./supp/links.py content/page/references.md

## References

* https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/

* https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_forked_sample_project.html

* https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/

* https://gitlab.com/pages/hugo
