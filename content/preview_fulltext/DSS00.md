---
title: 'Front matter'
date: 2024-09-17
summary: 'Foreword, Preface, Acknowledgements, Introduction'
weight: -1000
---

| *Designing Secure Software* by Loren Kohnfelder (all rights reserved) |
| --------------------------------------------------------------------- |
| [Home](../home/) [00](../dss00/) [01](../dss01/) [02](../dss02/) [03](../dss03/) [04](../dss04/) [05](../dss05/) [06](../dss06/) [07](../dss07/) [08](../dss08/) [09](../dss09/) [10](../dss10/) [11](../dss11/) [12](../dss12/) [13](../dss13/) [14](../dss14/) Appendix: [A](../dss_aa/) [B](../dss_ab/) [C](../dss_ac/) [D](../dss_ad/) |
| *Buy the book [here](https://nostarch.com/designing-secure-software).* |

&nbsp;

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*In memory of robin.*

&nbsp;

> Dedicated to all the software professionals who keep the digital world 
> afloat, working to improve security one day at a time. Their greatest
> successes are those rare boring days when nothing bad happens.

&nbsp;


# Foreward by Adam Shostack

In 2006, I joined Microsoft, and was handed responsibility for how we threat
modeled across all our products and services. The main approach we used was
based on Loren’s STRIDE work. STRIDE is a mnemonic to help us consider the
threats of Spoofing, Tampering, Repudiation, Information disclosure, Denial of
service, and Elevation of privilege. It has become a key building block for me.
(It’s so central that I regularly need to correct people who think I invented
STRIDE.) In fact, when I read this book, I was delighted to find that Loren
calls on my Four Questions Framework much the way I call on STRIDE. The
Framework is a way of approaching problems by asking what we are working on,
what can go wrong, what we are going to do about those things, and whether we
did a good job. Many of the lessons in this book suggest that Loren and I have
collaborated even though we never worked directly together.

Today, the world is changing. Security flaws have become front page news. Your
customers expect better security than ever before, and push those demands by
including security in their evaluation criteria, drafting contract clauses,
putting pressure on salespeople and executives, and pressing for new laws. Now
is a great time to bring better security design into your software, from
conception to coding. This book is about that difficult subject: how to design
software that is secure.

The subject is difficult because of two main challenges. The first challenge,
that security and trust are both natural and nuanced, is the subject of Chapter
1, so I won’t say more about it. The second is that software professionals
often hope that software won’t require design. Software seems infinitely
malleable, unlike the products of other engineering disciplines. In those other
disciplines, we build models and prototypes before we bend steel, pour
concrete, or photo-etch silicon. And in contrast, we build code, refine it, and
then release it to the world, rather than following the famous advice of Fred
Brooks: you’re going to throw away the first system you build, so you might as
well plan to treat it as a prototype. The stories we tell of the evolution of
software rarely linger on our fruitless meanderings. We like to dismiss the
many lightbulbs that didn’t work and talk instead about how the right design
just happened to come to us. Sometimes, we even believe it. Even in writing
this, I am aware of a risk that you will think me—or worse, Loren—to be an
advocate of design for its own sake. And that I bother to disclaim it brings me
to another challenge that this book ably takes on: offering practical advice
about the design of software.

This is a book for a group of people who are too rarely respectfully and
compassionately addressed: technical professionals new to security. Welcome to
this part of the profession. As you’ll discover in these paged, the choices you
make about the systems you work on can impact security. But you don’t need to
become a security expert to make better choices. This book will take you far.
Some of you will want to go further, and there’s plenty of material out there
for you to read. Others will do well simply by applying what you learn here.


> Adam Shostack  
> President, Shostack + Associates  
> Author: *Threat Modeling: Designing for Security* (Wiley, 2014)  
> Affiliate Professor, University of Washington Paul G. Allen School of Computer Science and Engineering

# Preface

> If you cannot—in the long run—tell everyone
> what you have been doing, your doing has been worthless.
> —Erwin Schrödinger

Join me on a hike through the software security landscape. 

My favorite hike begins in a rainforest, near the top of the island of Kaua’i,
which is often shrouded in misty rain. The trail climbs moderately at first,
then descends along the contour of the sloping terrain, in places steep and
treacherously slippery after frequent rains. Further down, passing through
valleys choked with invasive ginger or overgrown by thorny lantana bushes, it
gets seriously muddy, and the less dedicated turn and head back. A couple of
miles out, the trees thin out as the environment gradually warms, becoming arid
with the lower elevation. Further on, the first long views of the surrounding
Pacific begin to open up, as reminders of the promise the trail offers.

In my experience, many software professionals find security daunting at first:
shrouded in mist, even vaguely treacherous. This is not without good reason. If
the act of programming corresponded to a physical environment, this would be
it.

The last mile of the trail runs through terrain made perilous by the loose
volcanic rock that, due to the island’s geologically tender age of five million
years, hasn’t had time to turn into soil. Code is as hard and unforgiving as
rock, yet so fragile that one small flaw can lead to a disaster, just as one
misstep on the trail could here. Fortunately, the hiking trail’s path along the
ridge has been well chosen, with natural handholds on the steepest section:
sturdy basalt outcroppings, or the exposed, solid roots of ohia trees.

Approaching the end of the trail, you’ll find yourself walking along the rim of
a deep gorge, the loose ground underfoot almost like ball bearings. To your
right, a precipice drops over 2,000 feet. In places, the trail is shoulder
width. I’ve seen acrophobic hikers turn around at this point, unable to summon
the confidence to proceed. Yet most people are comfortable here, because the
trail is *slightly inclined away from the dangerous side*. To the left, the
risk is minimal; you face the same challenging footing, but on a gentle slope,
so at worst you might slide a few feet. I thought about this trail often as I
wrote this book and have endeavored to provide just such a path, using stories
and analogies like this one to tackle the toughest subjects in a way that I
hope will help you get to the good stuff.

Security is challenging for a number of reasons: it’s abstract, the subject is
vast, and software today is both fragile and extremely complex. How can one
explain the intricacies of security in enough depth to connect with readers,
without overwhelming them with too much information? This book confronts those
challenges in the spirit of hikers on that trail at the rim of the gorge: by
leaning away from the danger of trying to cover everything. In the interest of
not losing readers, I err on the side of simplification, leaving out some of
the smaller details. By doing so, I hope to avoid readers metaphorically
falling into the gorge—that is, getting so confused or frustrated that you give
up. The book should instead serve as a springboard, sparking your interest in
continued exploration of software security practices. 

As you approach the end of the trail, the ridge widens out and becomes flat,
easy walking. Rounding the last curve, you’re treated to a stunning 300-degree
view of the fabled Na Pali coast. To the right is a verdant hanging valley,
steeply carved from the mountain. A waterfall feeds the meandering river
visible almost directly below. The intricate coastline extends into the
distance, flanked by neighboring islands on the horizon to the west. The
rewards of visiting this place never get old. After drinking in the experience,
a good workout awaits as you start the climb back up.

══════════════════════════════

Just as I’ll never get to see every inch of this island, I won’t learn
everything there is to know about software security, and of course, no book
will ever cover this broad topic completely, either. What I do have, as my
guide, is my own experience. Each of us charts our own unique path through this
topic, and I’ve been fortunate to have been doing this work for a long time.
I’ve witnessed firsthand some key developments and followed the evolution of
both the technologies and the culture of software development since its early
days. 

The purpose of this book is to show you the lay of the security land, with some
words of warning about some of the hazards of the trail so you can begin
confidently exploring further on your own. When it comes to security,
cut-and-dried guidance that works in all circumstances is rare. Instead, my aim
is to show you some simple examples from the landscape to kick-start your
interest and deepen your understanding of the core concepts. For every topic
this book covers, there is always much more to say. Solving real-world security
challenges always requires more context in order to better assess possible
solutions; the best decisions are grounded in a solid understanding of the
specifics of the design, implementation details, and more. As you grasp the
underlying ideas and begin working with them, with practice it becomes
intuitive. And fortunately, even small improvements over time make the effort
worthwhile. 

When I look back on my work with the security teams at major software
companies, a lost opportunity always strikes me. Working at a large and
profitable corporation has many benefits: along with on-site massage and
sumptuous cafes come on-tap security specialists (like myself) and a design
review process. Yet few other software development efforts enjoy the benefits
of this level of security expertise and a process that integrates security from
the design phase. This book seeks to empower the software community to make
this standard practice.

With myriad concerns to balance, designers have their hands full. The good ones
are certainly aware of security considerations, but they rarely get a security
design review. (And none of my industry acquaintances have even heard of the
service being offered by consultants.) Developers also have varying degrees of
security knowledge, and unless they pursue it as a specialty, their knowledge
is often at best piecemeal. Some companies do care enough about security to
hire expert consultants, but this invariably happens late in the process, so
they’re working after the fact to shore up security ahead of release. Bolting
on security at the end has become the industry’s standard strategy—the opposite
of baking in security.

Over the years, I have tried to gently spread the word about security among my
colleagues. Invariably, one quickly sees that certain people get it; others,
not so much. Why people respond so differently is a mystery, possibly more
psychological than technological, but it does raise an interesting question.
What does it mean to “get” security, and how do you teach it? I don’t mean
world-class knowledge, or even mastery, but a sufficient grasp of the basics to
be aware of the challenges and how to make incremental improvements. From that
point, software professionals can continue their research to fill in any gaps.
That’s the objective that this book endeavors to deliver. 

Throughout the process of writing this book, my understanding of the challenge
this work entailed has grown considerably. At first, I was surprised that a
book like this didn’t already exist; now I think I know why. Security concepts
are frequently counterintuitive; attacks are often devious and nonobvious, and
software design itself is already highly abstract. Software today is so rich
and diverse that securing it represents a daunting challenge. Software security
remains an unsolved problem, but we do understand large parts of it, and we’re
getting better at it—if only it weren’t such a fast-moving target! I certainly
don’t have perfect answers for everything. All of the easy answers to security
challenges are already built into our software platforms, so it’s the hard
problems that remain. This book strategically emphasizes concepts and the
development of a security mindset. It invites more people to contribute to
security, to bring a greater diversity of fresh perspectives and more
consistent security focus. 

I hope you will join me on this personal tour of my favorite paths through the
security landscape, in which I share with you the most interesting insights and
effective methodologies that I have to offer. If this book convinces you of the
value of baking security into software from the design phase, of considering
security throughout the process, and of going beyond what I can offer here,
then it will have succeeded.


# Acknowledgements

> Knowledge is in the end based on acknowledgement.
> —Ludwig Wittgenstein

I wrote this book with appreciation of the many colleagues in academia and

industry from whom I have learned so much. Security work can be remark-
ably thankless—successes are often invisible, while failures get intense scru-
tiny—and it’s extremely heartening that so many great people devote their

considerable talents and effort to the cause.
Publishing with No Starch Press was my best choice to make this book
the best it can be. Without exception, everyone was great to work with and
infinitely patient handling my endless questions and suggestions.
I would like to thank the early readers of the manuscript for their
valuable feedback: Adam Shostack, Elisa Heymann, Joel Scambray, John

Camilleri, John Goben, Jonathan Lundell, and Tony Cargile. Adam’s sup-
port has been above and beyond, leading to a wide range of other discus-
sions, putting in the good word for me with No Starch Press, and capped

off by his generous contribution of the foreword.
It would have been interesting to record all the errors corrected in
the process of writing this book, and it certainly has been a great lesson in
humility. I thank everyone for their sharp eyes, and take responsibility for
what errors may have made it through. Please refer to the online errata at
https://www.nostarch.com/designing-secure-software/ for the latest corrections.

I have benefited from great support from others outside the tech sphere
as well, and a few deserve special mention with my appreciation: Rosemary
Brisco, for marketing advice; Lisa Steres, PhD, for unwavering enthusiasm
and enduring interest in this project.

Finally, arigatou to my wife, Keiko, for her boundless support through-
out this project.

# Introduction


Two central themes run through this book: encouraging software professionals to
focus on security early in the software construction process, and involving the
entire team in the process of—as well as the responsibility for—security. There
is certainly plenty of room for improvement in both of these areas, and this
book shows how to realize these goals. 

I have had the unique opportunity of working on the front lines of software
security over the course of my career, and now I would like to share my
learnings as broadly as possible. Over 20 years ago, I was part of the team at
Microsoft that first applied threat modeling at scale across a large software
company. Years later, at Google, I participated in an evolution of the same
fundamental practice, and experienced a whole new way of approaching the
challenge. Part 2 of this book is informed by my having performed well over a
hundred design reviews. Looking back on how far we have come provides me with a
great perspective with which to explain it all anew.

Designing, building, and operating software systems is an inherently risky
undertaking. Every choice, every step of the way, nudges the risk of
introducing a security vulnerability either up or down. This book covers what I
know best, learned from personal experience. I convey the security mindset from
first principles and show how to bake in security throughout the development
process. Along the way I provide examples of design and code, largely
independent of specific technologies so as to be as broadly applicable as
possible. The text is peppered with numerous stories, analogies, and examples
to add spice and communicate abstract ideas as effectively as possible. 

The security mindset comes more easily to some people than others, so I have
focused on building that intuition, to help you think in new ways that will
facilitate a software security perspective in your work. And I should add that
in my own experience, even for those of us to whom it comes easily, there are
always more insights to gain.

This is a concise book that covers a lot of ground, and in writing it, I have
come to see this as essential to what success it may achieve. Software security
is a field of intimidating breadth and depth, so keeping the book shorter will,
I hope, make it more broadly approachable. My aim is to get you thinking about
security in new ways, and to make it easy for you to apply this new perspective
in your own work.


## Who Should Read This Book?

This book is for anyone already proficient in some facet of software design and
development, including architects, UX/UI designers, program managers, software
engineers, programmers, testers, and management. Tech professionals should have
no trouble following the conceptual material so long as they understand the
basics of how software works and how it’s constructed. Software is used so
pervasively and is of such great diversity that I won’t say that all of it
needs security; however, most of it likely does, and certainly any that
connects to the internet or interfaces significantly with people.

In writing the book, I found it useful to consider three classes of prospective
readers, and would like to offer a few words here to each of these camps.

*Security newbies*, especially those intimidated by security, are the primary
audience I am writing for, because it’s important that everyone working in
software understand security so they can contribute to improving it. To make
more secure software in the future we need everyone involved, and I hope this
book will help those just starting to learn about security to quickly get up to
speed.

*Security-aware* readers are those with interest in but limited knowledge of
security, who are seeking to round out and deepen their understanding and also
learn more practical ways of applying these skills to their work. I wrote this
book to fill in the gaps, and provide plenty of ways you can immediately put
what you learn here into practice.

*Security experts* (you know who you are) round out the field. They may be
familiar with much of the material, but I believe this book provides some new
perspectives and still has much to offer them. Namely, the book includes
discussions of important relevant topics, such as secure design, security
reviews, and “soft skills” that are rarely written about. 


> NOTE: The third part of this book, which covers implementation vulnerabilities
and mitigations, includes short excerpts of code written in either C or Python.
Some examples assume familiarity with the concept of memory allocation, as well
as an understanding of integer and floating-point types, including binary
arithmetic. In a few places I use mathematical formulae, but nothing more
than modulo and exponential arithmetic. Readers who find the code or math
too technical or irrelevant should feel free to skip over these sections
without fear of losing the thread of the overall narrative. 
References such as man(1) are \*nix (Unix family of operating systems)
commands `(1)` and functions `(3)`.

## What Topics Does the Book Cover?

The book consists of 14 chapters organized into three parts, covering concepts,
design, and implementation, plus a conclusion.


### Part 1: Concepts

Chapters 1 through 5 provide a conceptual basis for the rest of book. Chapter
1, *Foundations*, is an overview of information security and privacy
fundamentals. Chapter 2, *Threats*, introduces threat modeling, fleshing out
the core concepts of attack surfaces and trust boundaries in the context of
protecting assets. The next three chapters introduce valuable tools available
to readers for building secure software. Chapter 3, *Mitigations*, discusses
commonly used strategies for defensively mitigating identified threats. Chapter
4, *Patterns*, presents a number of effective security design patterns, and
flags some anti-patterns to avoid. Chapter 5, *Crypto*graphy, takes a toolbox
approach to explaining how to use standard cryptographic libraries to mitigate
common risks, without going into the underlying math (which is rarely needed in
practice).


### Part 2: Design

This part of the book represents perhaps its most unique and important
contribution to prospective readers. Chapter 6, *Secure Design*, and Chapter 7,
*Security Design Review*s, offer guidance on secure software design and
practical techniques for how to accomplish it, approaching the subject from the
designer’s and reviewer’s perspectives, respectively. In the process, they
explain why it’s important to bake security into software design from the
beginning. These chapters draw on the ideas introduced in the first part of the
book, offering specific methodologies for how to incorporate them to build a
secure design. The review methodology is directly based on my industry
experience, including a step-by-step process you can adapt to how you work.
Consider browsing the sample design document in Appendix A while reading these
chapters as an example of how to put these ideas into practice.


### Part 3: Implementation

Chapters 8 through 13 cover security at the implementation stage and touch on
deployment, operations, and end-of-life. Once you have a secure design, this
part of the book explains how to develop software without introducing
additional vulnerabilities. These chapters include snippets of code,
illustrating both how vulnerabilities creep into code and how to avoid them.
Chapter 8, *Secure Programming*, introduces the security challenge that
programmers face, and what real vulnerabilities actually look like in code.
Chapter 9, *Low-Level* Coding *Flaws*, covers the foibles of computer
arithmetic and how C-style explicit management of dynamic memory allocation can
undermine security. Chapter 10, *Untrusted Input*, and Chapter 11, *Web
Security*, cover many of the commonplace bugs that have been well known for
many years but just don’t seem to go away (such as injection, path traversal,
XSS, and CSRF vulnerabilities). Chapter 12, *Security Testing*, covers the
greatly underutilized practice of testing to ensure that your code is secure.
Chapter 13, *Secure Development Best Practices*, rounds out the secure
implementation guidance, covering some general best practices and providing
cautionary warnings about common pitfalls.

The excerpts of code in this part of the book generally demonstrate
vulnerabilities to be avoided, followed by patched versions that show how to
make the code secure (labeled “vulnerable code” and “fixed code,”
respectively). As such, the code herein is not intended to be copied for use in
production software. Even the fixed code could have vulnerabilities in another
context due to other issues, so you should not consider any code presented in
this book to be guaranteed secure for any application.


### Conclusion

The final chapter—Chapter 14*Looking Ahead*—is brief, because my crystal ball
is cloudy. Here I summarize the key points made in the book, attempt to peer
into the future, and offer speculative ideas that could help ratchet software
security upward, beginning with a vision for how this book can contribute to
more secure software going forward. 


### Appendices

Appendix A is a sample design document that illustrates what security-aware
design looks like in practice.

Appendix B is a glossary of software security terms that appear throughout the
book.

Appendix C includes some open-ended exercises and questions that ambitious
readers might enjoy researching.

In addition, a compilation of references to sources mentioned in the book can
be found on the web, 
linked from https://designingsecuresoftware.com/page/references/.


## Good, Safe Fun

Before we get started, I’d like to add some important words of warning about
being responsible with the security knowledge this book presents. In order to
explain how to make software safe, I have had to describe how various
vulnerabilities work, and how attackers potentially exploit them.
Experimentation is a great way to hone skills from both the attack and defense
perspectives, but it’s important to use this knowledge carefully.

Never play around by investigating security on production systems. When you
read about cross-site scripting (XSS), for instance, you may be tempted to try
browsing your favorite website with tricky URLs to see what happens. Please
don’t. Even when done with the best of intentions, these explorations may look
like real attacks to site administrators. It’s important to respect the
possibility that others will interpret your actions as a threat—and, of course,
you may be skirting the law in some countries. Use your common sense, including
considering how your actions might be interpreted and the possibility of
mistakes and unintended consequences, and err on the side of refraining.
Instead, if you’d like to experiment with XSS, put up your own web server using
fake data; you can then play around with this to your heart’s content.

Furthermore, while this book presents the best general advice I can offer based
on many years of experience working on software security, no guidance is
perfect or applicable in every conceivable context. Solutions mentioned herein
are never “silver bullets”: they are suggestions, or examples of common
approaches worth knowing about. Rely on your best judgment when assessing
security decisions. No book can make these choices for you, but this book can
help you get them right.

&#10042; &#10042; &#10042; &#10042; &#10042; &#10042; &#10042; &#10042;

