+++
title = "[TEST] Free Online Edition"
date = 2024-09-17
draft = false
+++

This is the online full text version of 
*Designing Secure Software* by Loren Kohnfelder
(all rights reserved).

Providing this free online version is the best way to share the ideas
in the book as widely as possible. Please understand that this version
is hand built and includes the author's rough diagrams rather than the
versions in the book done by a graphic artist,precedes final proofreading
edits, and generally lacks the many other details that gives
the published version additional quality and finish. 

Please consider 
[buying the book](https://nostarch.com/designing-secure-software) 
from the publisher, where you can get the e-book *without DRM*
at no additional cost when you purchase the print edition
(or just buy the e-book).

## Contents

* [Frontmatter](../dss00/)
  * [Foreward by Adam Shostack](../dss00/#foreward-by-adam-shostack)
  * [Preface](../dss00/#preface)
  * [Acknowledgements](../dss00/#acknowledgements)
  * [Introduction](../dss00/#introduction)
<!--
: Foreward, [Preface](../dss00/#preface), Acknowledgements, Introduction
-->

### PART I

* [Foundations](../dss01/)
* [Threats](../dss02/)
* [Mitigation](../dss03/)
* [Patterns](../dss04/)

