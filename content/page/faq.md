---
title: "FAQ"
date: 2021-10-30
draft: false
---

# When will the book be published?

The book is available for pre-order from No Starch Press [here](https://nostarch.com/designing-secure-software) and they expect to start shipping in early November, with the e-book already available there. 

December 7, 2021, is the general availability on-sale data for other booksellers.

[Updated: October 2021]

# Code from the book

## Where can I find code from the book online?

It isn't online at this time. I haven't made the code in the book
public because it’s intended to show vulnerabilities and is not
intended to be copy/pasted. The fixes I demonstrate are, so far as I
can verify, secure; however, they are kept simple for explanatory
purposes, and are not necessarily bullet-proof for all possible
applications.

The last thing I want is for someone to grab some code with blind
faith and then it turns out to have a vulnerability under certain conditions.
(See the next question for more on how that could easily happen.)

## Why doesn't the book provide code that is 100% vulnerability free?

There are several reasons I don't guarantee any code in the book
unconditionally: (a) the requirements are informal and may not apply
in every possible case of implementations; (b) they are not thoroughly
tested (in part this is because the requirements are vague); \(c\) some
API may vary between platforms (e.g. details of pathnames differ
between *nix and Windows platforms in complicated ways).

If we knew how to write code that perfectly handled all known common
vulnerabilities that would basically solve our security problem.
Bug bounty money is good money, but if you had a solution that would
end the need for bug bounties entirely, that would sell for serious money.

## How can I get a copy of the code in the book for security research?

It is conceivable that this code would be useful for security
research. Most code snippets in the book are only a handful of lines,
but if anyone has a special need please 
[contact me](https://linkedin.com/in/kohnfelder) to discuss.

