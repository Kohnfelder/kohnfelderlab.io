---
title: Epigrams
---

> Epigrams from the book *Designing Secure Software: a guide for developers*

“If you cannot—in the long run—tell everyone what you have been doing, your doing has been worthless.” —Erwin Schrödinger

“Honesty is a foundation, and it’s usually a solid foundation. Even if I do get in trouble for what I said, it’s something that I can stand on.”—Charlamagne tha God

“The threat is usually more terrifying than the thing itself.”—Saul Alinsky

“Exploits are the closest thing to ‘magic spells’ we experience in the real world: Construct the right incantation, gain remote control over device.” —Halvar Flake

“Everything is possible to mitigate through art and diligence.”—Gaius Plinius Caecilius Secundus (Pliny the Younger)

“Art is pattern informed by sensibility.”—Herbert Read

“Learn to see in another’s calamity the ills which you should avoid.”—Publilius Syrus

“Cryptography is typically bypassed, not penetrated.”—Adi Shamir

“Overload, clutter, and confusion are not attributes of information, they are failures of design.”—Edward Tufte

“I will contend that conceptual integrity is the most important consideration in system design.”—Fred Brooks (from The Mythical Man-Month)

“Simplicity is the ultimate sophistication.”—Leonardo da Vinci

“A good, sympathetic review is always a wonderful surprise.”—Joyce Carol Oates

“Whatever you do in life, surround yourself with smart people who’ll argue with you.” —John Wooden

“The first principle is that you must not fool yourself, and you are the easiest person to fool.”—Richard P. Feynman

“If debugging is the process of removing bugs, then programming must be the process of putting them in.”—Edsger Dijkstra

“All happy families are alike; each unhappy family is unhappy in its own way.”—Leo Tolstoy

The greatest trick the devil ever pulled was convincing the world he didn’t exist.—Charles Baudelaire

“Low-level programming is good for the programmer’s soul.”—John Carmack

“I like engineering, but I love the creative input.”—John Dykstra

“Before you look for validation in others, try and find it in yourself.”—Greg Behrendt

“If you are a programmer working in 2006 and you don’t know the basics of characters, character sets, encodings, and Unicode, and I catch you, I’m going to punish you by making you peel onions for six months in a submarine.”—Joel Spolsky

“If you ever injected truth into politics you would have no politics.”—Will Rogers

“When the words appeared, everyone said they were a miracle. But nobody pointed out that the web itself is a miracle.”—E. B. White (from Charlotte’s Web)

“Use design as a framework to bring order out of chaos.”—Nita Leland

“I'm kind of glad the web is sort of totally anarchic. That’s fine with me.”—Roger Ebert

“Anyone who considers protocol unimportant has never dealt with a cat.”—Robert A. Heinlein

“If what is communicated is false, it can hardly be called communication.”—Benjamin Mays

“Doubt is the origin of wisdom.”—Rene Descartes

“When the going gets tough, the tough make cookies.”—Erma Bombeck

“Websites should look good from the inside and out.”—Paul Cookson

“I don’t let myself ‘surf’ on the Web, or I would probably drown.”—Aubrey Plaza

“One cannot separate the spider web’s form from the way in which it originated.”—Neri Oxman

“The only way you can know where the line is, is if you cross it.”—Dave Chappelle

“Testing leads to failure, and failure leads to understanding.”—Burt Rutan

“What a testing of character adversity is.”—Harry Emerson Fosdick

“A good test case is one that has a high probability of detecting an as yet undiscovered error.”—Glenford Myers

“What regresses, never progresses.”—Umar ibn al-Khattâb

“Worry about being unavailable; worry about being absent or fraudulent.”—Anne Lamott

“They say that nobody is perfect. Then they tell you practice makes perfect. I wish they'd make up their minds.”—Winston Churchill

“Quality is always in style.”—Robert Genn

“Dependence leads to subservience.”—Thomas Jefferson

“The term ‘triage’ normally means deciding who gets attention first.”—Bill Dedman

“The secret of landscapes isn’t creation. . . It’s maintenance.”—Michael Dolan

“Exploration is the engine that drives innovation.” —Edith Widder

“For me, what makes life enjoyable is having a shared culture and shared references.” —Michael Sheen


