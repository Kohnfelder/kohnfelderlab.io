---
title: Book excerpts
---

__Selections from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

* [Preface](/text/ch0-preface)

* [Introduction](/text/ch0-intro)

* [Classic security principles](/text/ch1-classic): 
[C-I-A](/text/ch1-c-i-a) and the [Gold Standard](/text/ch1-gold) (Ch. 1)

* [Information privacy basics](/text/ch1-privacy) (Ch. 1)

* [Threats and security mindset](/text/ch2-threats) (Ch. 2)
    - [Categorizing Threats with STRIDE](/text/ch2-stride)

* [Security patterns](/text/ch4-patterns) (Ch. 4)
    - [Design Attributes](/text/ch4-design-patterns): 
	Economy of Design, Transparent Design
    - [Exposure Minimization](/text/ch4-minim-patterns): 
	Least Privilege, Least Information, Secure by Default, 
	Allowlists over Blocklists, Avoid Predictability, Fail Securely
    - [Strong Enforcement](/text/ch4-enforce-patterns): 
	Complete Mediation, Least Common Mechanism
    - [Redundancy](/text/ch4-redundancy-patterns): 
	Defense in Depth, Separation of Privilege
    - [Trust and Responsibility](/text/ch4-trust-patterns): 
	Reluctance to Trust, Accept Security Responsibility
	- [Anti-Patterns](/text/ch4-anti-patterns): 
	Confused Deputy, Backflow of Trust, Third-Party Hooks, Unpatchable Components

* [Security Design Review](/text/ch7-sdr): Overview and six step process (Ch. 7)

* Untrusted Input selections (Ch. 10)
    - [Injection Attacks](/text/ch10-injection)
    - [Path Traversal](/text/ch10-path-traversal)

* [Security Testing](/text/ch12-testing): 
Security tests and security regression tests (Ch. 12)
