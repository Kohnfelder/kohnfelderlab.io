+++
title = "Errata"
+++

This page is the errata for the book 
*Designing Secure Software: a guide for developers*. 

Japanese translation:
本書の日本語版の正誤情報につきましては以下のリンクを参照下さい。
https://www.shuwasystem.co.jp/support/7980html/6975.html

**Updated: November 14, 2023**

> "Errors of opinion may be tolerated where reason is left free to combat it."
> --- Thomas Jefferson

### Afterword (page 243)

The best advice remains to ***only*** use your phone for anything that you
wouldn’t greatly mind possibly leaking if you lose it.

### Appendix C: Exercises

Two links printed in the book's text (both on page 271)
have broken since publication.

https://bugs.chromium.org/p/chromium/issues/list
(remove the final slash)

https://web.archive.org/web/20221205164132/https://underhandedcrypto.com/


### Note

Several readers have asked why the book doesn't cover a certain topic. 
The answer is that (after considerable thought) I don't think any book 
on a topic this complex and diverse can possibly be complete, so in the
end the choice of what to include or not is an arbitrary limitation.
For this book I decided to stick with what I have actual experience with
and know best. Since my experience all unfolds from doing a job, coverage
of the field of security is not going to be complete; however, given 
enough years I believe that I've been fortunate to work on many diverse
projects and that the basics are well covered.
