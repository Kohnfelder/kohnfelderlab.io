---
title: About the author
comments: false
---

I began programming over fifty years ago,
and my path has crossed into security a few times.

As a student at MIT, my thesis, *Towards a Practical Public Key Cryptosystem*;
(S.B. Massachusetts Institute of Technology 1978), first described 
digital certificates and the foundations of public key infrastructure (PKI).

My software career spans a wide variety of programming jobs,
from punched cards, writing disk controller drivers, a linking loader,
video games, two stints in Japan, 
to equipment control software in a semiconductor research lab.

At Microsoft, I returned to security work on the Internet Explorer team,
and later, the .NET platform security team,
contributing to the industry’s first proactive security process methodology.

Most recently, at Google, I worked as a software engineer on the
security team and later as a founding member of the privacy team,
performing well over one hundred security design reviews of
large-scale commercial systems.

You can reach me at [infosec.exchange@lmk](https://infosec.exchange/@lmk).

***
