+++
title = "Media links"
draft = false
+++

# Podcasts

Podcast interview all about software security, touching on:
artificial intelligence, cryptography, PKI, development,
trust in AI, automation, triaging, transparency and knowledge sharing,
and future speculation. Thanks to host James McQuiggan & Roger Grimes!

https://securitymasterminds.buzzsprout.com/1892704/15401073-50-years-of-evolution-of-cybersecurity-and-securing-software-from-punch-cards-and-pki-to-chatgpt-with-special-guest-loren-kohnfelder

Ted Harrington hosted for a great conversation about the proactive side 
of software security, how my book can help everyone move left, 
and why that's important.

https://tech-done-different.simplecast.com/episodes/starting-with-security-a-conversation-with-author-of-designing-secure-software-loren-kohnfelder-tech-done-different-with-ted-harrington-Ix6RSSPA

On Chris Romeo's Application Security Podcast we talked about the book,
(security design reviews in particular), and the impossible question of
what I would do as the one thing to improve software security. 
We also reminisce about creating STRIDE and digital certificates.

https://podcasts.apple.com/us/podcast/loren-kohnfelder-designing-secure-software/id1154351685?i=1000544209193

# Interviews

Q&A about the book *Designing Secure Software*:
https://nostarch.com/blog/redesigning-security-living-legend-loren-kohnfelder

*Rethink Security* interview,
speculative thoughts on future directions of security:
[*A Conversation With Loren Kohnfelder*](https://rethinksecurity.io/posts/a-conversation-with-the-father-of-pki).

# Other

You can find some of my other writing over a wide range of topics
on [Medium](https://medium.com/@lorenkohnfelder).

