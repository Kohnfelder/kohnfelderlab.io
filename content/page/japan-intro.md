+++
title = "Introduction to the Japanese translation"
+++

The Japanese translation of this book is very special to me because
of my connection to the country and its people. So I'm especially
pleased to be able to share what I've learned back to Japan through
this book, which I hope will inspire readers to embrace security and
spread the word.

I moved to Japan in the latter years of the Showa era with a vision
of helping bootstrap the fledgling software industry to a position of
global leadership. Japanese electronics technology was world class
with the Sony Walkman and passport-sizes videocam demonstrating
unparalleled reliability and ease of use. I reasoned that Japan had
every advantage suited to creating great software: the intensely
competitive education system would train plenty of highly skilled
software engineers; a culture of attention to the smallest detail and
utmost quality assurance; and the intimate bond between government
and industry would surely nurture a favorable environment.

My work went well and coworkers kindly accommodated my peculiarities
and lack of experience with the different work culture, but I left
Japan during the heyday of Windows 95 when its epic success made
it clear that Microsoft had a commanding lead, landing a job on the
fledgling Internet Explorer team just as the World Wide Web was taking
off, eventually leading to an even more epic software transformation
still going strong.

Fast forward to the Reiwa era, and my belief that Japan is uniquely
positioned to become a world leader in software across all categories
remains alive. The same cultural and structural advantages are in
place, and by some estimates today there are over one million software
professionals. Over a quarter century later, I will add three new
factors where I believe Japan has deep experience and tradition:
simplicity, transparency, and security.

When I first visited Japan many years ago I was struck by the austere
beauty of cultural landmarks including Nijo Castle or the imperial
estates. In stark contrast to the gaudy ornateness of European royalty
such as Versailles, the Japanese aesthetic of beauty seems from another
world entirely — a much more advanced civilization at that. Be it the
elegant lines of the kimono, or the meticulously manicured "perfected
nature" of the gardens, Japan has long enjoyed a uniquely sophisticated
yet minimalist sense of artistry. This tradition embodies clean design,
focusing on the essential, with meticulous attention to detail that
looks effortless. And if that isn't a perfect recipe for just what
our software ecosystem desperately needs then I don't know what is.

While I know well that today most people in Japan do not live in
traditional style buildings with tatami mats, these Japanese values do
survive and still form an important part of the cultural core. With
the right leadership I have no doubt that this cultural heritage
can be leveraged to build great software of the highest quality —
including reducing vulnerabilities to a minimum. Those principles
of simplicity, transparency, and security all interrelate and are
essential qualities of the finest quality software.

Software has advanced by leaps and bounds — now generative AI
is exploding onto the scene promising to open a new period of
technological advances — and one unfortunate part of that growth
is that our systems seem to be increasingly vulnerable to attack. At
the same time the world is also increasingly dependent on digital
infrastructure for just about every facet of modern life. Modern
software stacks are enormously complex, and now we are just beginning
to understand the challenges of dependency management and how
vulnerable our infrastructure is to supply chain attacks.

Writing in 2023, as the tsunami of generative AI technology is
beginning to crest, two things are clear: it's going to have a
profound impact on many aspects of society, and nobody has any idea
exactly how it will play out. AI will surely have a growing role in
our software development process, but to the extent we use it there
will be a proportional open question: how do we trust it?

It's too early to say how much generative AI will accelerate software
development, and if that will potentially mean higher or lower
quality. As with any powerful new technology the result is going
to depend on how we choose to apply it. In the case of AI the one
thing we do know with certainty is that malicious threat actors are
not going away, so we can be certain that they are going to leverage
it in every way that they can — offensively attacking, stealthily
infiltrating, finding weaknesses in our policies and configurations,
infecting trusted code and models, and much more. So as the pace and
scale of technology growth accelerate we are going to need smart and
diligent humans overseeing and reviewing the process, because that's
a job we cannot blindly entrust to the AI.

After college and a couple of years working in Silicon Valley,
I was working too many hours and also found myself in need of a
little adventure. With the patient support of my good friend there
(my girlfriend's roommate's boyfriend's acquaintance), I enjoyed
annual vacations traveling all around Japan, and on the third trip
interviewed and landed a job in Tokyo. Beyond simply enjoying the
people, culture, and amazing cuisine, at the time I specifically
wanted to get outside of my comfort zone, and Japan was challenging
as a profoundly different country and language, yet with thoroughly
modern infrastructure and quite safe and friendly.

By sheer good fortune, I moved to Tokyo in the spring of 1982, living
in a clean, modern, but tiny furnished apartment (provided by my
employer by agreement) and immersed in the workplace with a wonderful
translator to assist me as needed. Starting from full translation
support, over the next six months I weaned myself off her support
as I learned to understand the gist of conversation, then to speak
well enough to be understood more or less. The concentration required
was exhausting, but in time I adapted and began reading simple text
on my own, and eventually writing clumsily but well enough to be
understood. It was an ideal living laboratory for language learning
that I'm forever grateful for.

Japan and the US are at once utterly different societies yet there
is an ineffable common spirit one sees in personal relationships and
the close ties of the national governments. Whether it's writing
horizontally versus (old style Japanese) vertically, eating with
utensils versus chopsticks, or driving on the right or left side of
the road, it seems that if there was possibly any way the two countries
could be different they in fact were. Working in Japan for a total of
about ten years, I had many opportunities to see this superposition
of similarity and dissimilarity. I won't claim to deeply understand
Japan, but I did come to see it clearly enough to trust it, which is
more important.

One minor detail that I remember noticing in Japan that really struck
is worth mentioning here because it's an example of threat modeling. I
loved all the greengrocers that would often be located near train
stations as a wonderful alternative to the supermarket. Japanese
fruit and vegetables are world class, but it wasn't the JPY 10,000
cantaloupes that surprised me: it was the money handling at these
shops. Customers would select what they wanted to buy and a shop
attendant would appear to take the money and give change. The detail
that caught my attention was that there was no cash register and
no waiting in line: instead, they kept the till in a few baskets
suspended by strings around the shop at strategic locations so the
attendant could just reach in.

Now that cashless payment is more common this practice may be
dwindling, but at the time I was shocked because obviously anybody
could grab a handful of money — or easily yank the whole basket down
— and run off. Of course, this virtually never happened in practice,
but to my US native sensibilities the threat was obvious. Probably most
shopkeepers were oblivious to such admittedly shameful perception,
and rightfully so. What I couldn't fathom was how an entire society
achieves such a level of honesty and integrity. While I didn't
actually think that most of my fellow Americans were thieves,
I was raised that there were inevitably "a few bad apples" and
that responsible shopkeepers should secure their cash or they were
"asking for it". Yet here in Japan the dynamic was utterly different,
so maybe what I considered to be "human nature" was not that at all,
it was the result of cultural milieus.

Having long passed *Kanreki*, I have been incredibly fortunate to have
enjoyed an unusual software working career, and my work in Japan was
an important part of that journey.  My purpose in writing the book was
to share back to the software community what I've learned along the
way because if there is one thing we know for certain about software
security it's that not enough people understand it well enough.

Over a quarter of a century later my hopes for the future of Japanese
software remain strong. I recall the culture shock of moving back
from Japan to work at Microsoft where I learned that the work culture
rewarded not asking permission, but acting independently and asking
forgiveness later — an early precursor to "move fast and break
things" which I've never thought was wise. The problem is that it's
easy for the developer to move on, but too often customers are caught
unawares and the collateral damage can be enormous (for example,
consider the repercussions of Cambridge Analytica and the many social
media privacy violations).

I believe that difference in fundamental values remains today as
a clear contrast between cultures, and that the best software only
results from careful planning and cooperation followed by meticulous
implementation and scrutiny to deliver the highest quality. This
represents a great opportunity since software is a major component of
just about every economic activity today. Japan is ideally positioned
to make a difference and I can't wait to see what happens next.
