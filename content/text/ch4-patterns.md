---
title: Patterns
weight: -400
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

> "Art is pattern informed by sensibility."
> ---Herbert Read

Architects have long used design patterns to envision new buildings,
an approach just as useful for guiding software design. This chapter excerpt
introduces many of the most useful patterns promoting secure design.
Several of these patterns derive from ancient wisdom; the trick is
knowing how to apply them to software and how they enhance security.

These patterns either mitigate or avoid various security
vulnerabilities, forming an important toolbox to address potential
threats. Many are simple, but others are harder to understand and best
explained by example. Don't underestimate the simpler ones, as they can
be widely applicable and are among the most effective. Still other
concepts may be easier to grasp as anti-patterns describing what *not* to do. 
I present these patterns in groups based on shared
characteristics that you can think of as sections of the toolbox.

When and where to apply these patterns requires judgment. Let necessity
and simplicity guide your design decisions. As powerful as these
patterns are, don't overdo it; just as you don't need seven deadbolts
and chains on your doors, you don't need to apply every possible design
pattern to fix a problem. Where several patterns are applicable, choose
the best one or two, or maybe more for critical security demands.
Overuse can be counterproductive, because the diminishing returns of
increased complexity and overhead quickly outweigh additional security
gains.

* [Design Attributes](../ch4-design-patterns)
   - Economy of Design
   - Transparent Design
* [Exposure Minimization](../ch4-minim-patterns)
   - Least Privilege
   - Least Information
   - Secure by Default
   - Allowlists over Blocklists
   - Avoid Predictability
   - Fail Securely
* [Strong Enforcement](../ch4-enforce-patterns)
   - Complete Mediation
   - Least Common Mechanism
* [Redundancy](../ch4-redundancy-patterns)
   - Defense in Depth
   - Separation of Privilege
* [Trust and Responsibility](../ch4-trust-patterns)
   - Reluctance to Trust
   - Accept Security Responsibility
* [Anti-Patterns](../ch4-anti-patterns)
   - Confused Deputy
   - Backflow of Trust
   - Third-Party Hooks
   - Unpatchable Components


