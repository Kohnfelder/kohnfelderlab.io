---
title: The Gold Standard
weight: -140
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

If C-I-A is the goal of secure systems, the Gold Standard describes
the means to that end. *Aurum* is Latin for gold, hence the chemical
symbol "Au," and it just so happens that the three important
principles of security enforcement start with those same two letters:

**Authentication**: High-assurance determination of the identity of a principal

**Authorization**: Reliably only allowing an action by 
an authenticated principal

**Auditing**: Maintaining a reliable record of actions by principals for 
inspection

> *Jargon alert: because the words are so long and similar,
you may encounter the handy abbreviations **authN** (for authentication)
and **authZ** (for authorization) as short forms that plainly
distinguish them.*

A *principal* is any reliably authenticated entity: a person, business
or organization, government entity, application, service, device, or any
other agent with the power to act.

*Authentication* is the process of reliably establishing the validity of
the credentials of a principal. Systems commonly allow registered users
to authenticate by proving that they know the password associated with
their user account, but authentication can be much broader. Credentials
may be something the principal knows (a password) or possesses (a smart
card), or something they are (biometric data); we'll talk more about
them in the next section.

Data access for authenticated principals is subject to **authorization**
decisions, either allowing or denying their actions according to
prescribed rules. For example, filesystems with access control settings
may make certain files read-only for specific users. In a banking
system, clerks may record transactions up to a certain amount, but might
require a manager to approve larger transactions.

If a service keeps a secure log that accurately records what principals
do, including any failed attempts at performing some action, the
administrators can perform a subsequent **audit** to inspect how the
system performed and ensure that all actions are proper. Accurate audit
logs are an important component of strong security, because they provide
a reliable report of actual events. Detailed logs provide a record of
what happened, shedding light on exactly what transpired when an unusual
or suspicious event takes place. For example, if you discover that an
important file is gone, the log should ideally provide details of who
deleted it and when, providing a starting point for further
investigation.

The Gold Standard acts as the enforcement mechanism that protects C-I-A.
We defined confidentiality and integrity as protection against
*unauthorized* disclosure or tampering, and availability is also subject
to control by an authorized administrator. The only way to truly enforce
authorization decisions is if the principals using the system are
properly authenticated. Auditing completes the picture by providing a
reliable log of who did what and when, subject to regular review for
irregularities, while holding the acting parties responsible.

Secure designs should always cleanly separate authentication from
authorization, because combining them leads to confusion, and audit
trails are clearer when these stages are cleanly divided. These two
real-world examples illustrate why the separation is important:

* "Why did you let that guy into the vault?" "I have no idea, but he
looked legit!"

* "Why did you let that guy into the vault?" "His ID was valid for 'Sam
Smith' and he had a written note from Joan."

The second response is much more complete than the first, which is of no
help at all, other than proving that the guard is a nitwit. If the vault
was compromised, the second response would give clear details to
investigate: did Joan have authority to grant vault access and write the
note? If the guard retained a copy of the ID, then that information
helps identify and find Sam Smith. By contrast, if Joan's note had just
said, "let the bearer into the vault"---authorization without
authentication---after security was breached, investigators would have
had little idea what happened or who the intruder was.

### Authentication

An authentication process tests a principal's claims of identity based
on credentials that demonstrate they really are who they claim to be. Or
the service might use a stronger form of credentials, such as a digital
signature of a challenge, which proves that the principal possesses a
private key associated with the identity, which is how browsers
authenticate web servers using HTTPS. The digital signature is stronger
authentication because the principal can prove they know the secret
without divulging it.

Evidence suitable for authentication falls into the following
categories:

* **Something you know**, like a password

* **Something you have**, like a secure token, or in the analog world some
kind of certificate, passport, or signed document that is unforgeable

* **Something you are**---that is, biometrics (fingerprint, iris pattern,
and so forth)

* **Somewhere you are**---your verified location, such as a connection to
a private network in a secure facility

Many of these methods are quite fallible. Something you know can be
revealed, something you have can be stolen or copied, your location can
be manipulated in various ways, and even something you are can
potentially be faked (and if it's compromised, you can't later change
what you are). On top of those concerns, in today's networked world
authentication almost always happens across a network, making the task
more difficult than in-person authentication. On the web, for instance,
the browser serves as a trust intermediary, locally authenticating and
only if successful then passing along cryptographic credentials to the
server. Systems commonly use multiple authentication factors to mitigate
these concerns, and auditing these frequently is another important
backstop. Two weak authentication factors are better than one (but not a
lot better).

Whenever feasible, rely on existing trustworthy authentication services,
and do not reinvent the wheel unnecessarily. Even simple password
authentication is quite difficult to do securely, and dealing securely
with forgotten passwords is even harder. Generally speaking, the
authentication process should examine credentials and provide either a
pass or fail response. Avoid indicating partial success, since this
could aid an attacker zeroing in on the credentials by trial and error.
To mitigate the threat of brute force guessing, a common strategy is to
make authentication inherently computationally heavyweight, or to
introduce increasing delay into the process.

After authenticating the user, the system must find a way to securely
bind the identity to the principal. Typically, an authentication module
issues a token to the principal that they can use in lieu of full
authentication for subsequent requests. The idea is that the principal,
via an agent such as a web browser, presents the authentication token as
shorthand assurance of who they claim to be, creating a "secure context"
for future requests. This context binds the stored token for
presentation with future requests on behalf of the authenticated
principal. Websites often do this with a secure cookie associated with
the browsing session, but there are many different techniques for other
kinds of principals and interfaces.

The secure binding of an authenticated
identity can be compromised in two fundamentally different ways. The
obvious one is where an attacker usurps the victim's identity.
Alternatively, the authenticated principal may collude and try to give
away their identity or even foist it off on someone else. An example of
the latter case is the sharing of a paid streaming subscription. The web
does not afford very good ways of defending against this because the
binding is loose and depends on the cooperation of the principal.

### Authorization

A decision to allow or deny critical actions should be based on the
identity of the principal as established by authentication. Systems
implement authorization in business logic, an access control list, or
some other formal access policy.

Anonymous authorization (that is, authorization without authentication)
can be useful in rare circumstances; a real-world example might be
possession of the key to a public locker in a busy station. Access
restrictions based on time (for example, database access restricted to
business hours) are another common example.

A single guard should enforce authorization on a given resource.
Authorization code scattered throughout a codebase is a nightmare to
maintain and audit. Instead, authorization should rely on a common
framework that grants access uniformly. A clean design structure can
help the developers get it right. Use one of the many standard
authorization models rather than confusing ad hoc logic wherever
possible.

*Role-based access control (RBAC)* bridges the connection between
authentication and authorization. RBAC grants access based on roles,
with roles assigned to authenticated principals, simplifying access
control with a uniform framework. For example, roles in a bank might
include these: clerk, manager, loan officer, security guard, financial
auditor, and IT admin. Instead of choosing access privileges for
each person individually, the system designates one or more roles based
on each person's identity to automatically and uniformly assign them
associated privileges. In more advanced models, one person might have
multiple roles and explicitly select which role they chose to apply for
a given access.

Authorization mechanisms can be much more granular than the simple
read/write access control that operating systems traditionally provide.
By designing more robust authorization mechanisms, you can strengthen
your security by limiting access without losing useful functionality.
These more advanced authorization models include *attribute-based access
control (ABAC)* and *policy-based access control (*P*BAC)*, and there
are many more.

Consider a simple bank teller example to see how fine-grained
authorization might tighten up policy:

**Rate-limited**: Tellers may do up to 20 transactions per hour, but more 
would be considered suspicious.

**Time of day**: Teller transactions must occur during business hours, 
when they are at work.

**No self-service**: Tellers are forbidden to do transactions with 
their own personal accounts.

**Multiple principals**: Teller transactions over \$10,000 require
special manager approval
(eliminating the risk of one bad actor moving a lot of money at once).

Finally, even read-only access may be too high a level for certain data,
like passwords. Systems usually check for login passwords by comparing
hashes, which avoids any possibility of leaking the actual plaintext
password. The username and password go to a frontend server that hashes
the password and passes it to an authentication service, quickly
destroying any trace of the plaintext password. The authentication
service cannot read the plaintext password from the credentials
database, but it can read the hash, which it compares to what the
frontend server provided. In this way, it checks the credentials, but
the authentication service never has access to any passwords, so even if
compromised, the service cannot leak them. Unless
interface design affords these alternatives, they will miss these
opportunities to mitigate the possibility of data leakage.

### Auditing

In order for an organization to audit system activity, the system must
produce a reliable log of all events that are critical to maintaining
security. These include authentication and authorization events, system
startup and shutdown, software updates, administrative accesses, and so
forth. Audit logs must also be tamper-resistant, and ideally even
difficult for administrators to meddle with, to be considered fully
reliable records. Auditing is a critical leg of the Gold Standard,
because incidents do happen, and authentication and authorization
policies can be flawed. Auditing can also serve as mitigation for inside
jobs in which trusted principals cause harm, providing necessary
oversight.

If done properly, audit logs are essential for routine monitoring, to
measure system activity level, to detect errors and suspicious activity,
and, after an incident, to determine when and how an attack actually
happened and gauge the extent of the damage. Remember that completely
protecting a digital system is not simply a matter of correctly
enforcing policies, it's about being a responsible steward of
information assets. Auditing ensures that trusted principals acted
properly within the broad range of their authority.

In May 2018, Twitter disclosed an embarrassing bug: they had discovered
that a code change had inadvertently caused raw login passwords to
appear in internal logs. It's unlikely that this resulted in any abuse,
but it certainly hurt customer confidence and should never have
happened. Logs should record operational details but not store any
actual private information so as to minimize the risk of disclosure,
since many members of the technical staff may routinely view the logs.
For a detailed treatment of this requirement, see the sample design
document in Appendix A detailing a logging tool that addresses just this
problem.

The system must also prevent anyone from tampering with the logs to
conceal bad acts. If the attacker can modify logs, they'll just clean
out all traces of their activity. For especially sensitive logs at high
risk, an independent system under different administrative and
operational control should manage audit logs in order to prevent the
perpetrators of inside jobs from covering their own tracks. This is
difficult to do completely, but often the mere presence of independent
oversight serves as a powerful disincentive to any funny
business, just as a modest fence and conspicuous video
surveillance camera can be an effective deterrent to trespassing.

Furthermore, any attempt to circumvent the system would seem highly
suspicious, and any false move would result in serious repercussions for
the offender. Once caught, they would have a hard time repudiating their
guilt.

Non-repudiability is an important property of audit logs; if the log
shows that a named administrator ran a certain command at a certain time
and the system crashed immediately, it's hard to point fingers at
others. By contrast, if an organization allowed multiple administrators
to share the same account (a terrible idea), it would have no way of
definitively knowing who actually did anything, providing plausible
deniability to all.

Ultimately, audit logs are useful only if you monitor them, analyze
unusual events carefully, and follow up, taking appropriate actions when
necessary. To this end, it's important to log the right amount of detail
by following the Goldilocks principle. Too much logging bloats the
volume of data to oversee, and excessively noisy or disorganized logs
make it difficult to glean useful information. On the other hand, sparse
logging with insufficient detail might omit critical information, so
finding the right balance is an ongoing challenge.


