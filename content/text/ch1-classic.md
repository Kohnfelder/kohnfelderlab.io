---
title: Classic security principles
weight: -120
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

The guiding principles of information security originated in the early days
of computing, when computers were emerging from special locked, 
air-conditioned, raised-floor rooms and starting to be connected in networks.
These traditional models are the “Newtonian physics” of modern informa-
tion security: a good and simple guide for many applications, but not the
be-all and end-all. For example, information privacy is one of the more
nuanced considerations for modern data protection and stewardship that
traditional information security principles do not cover.

The foundational principles group nicely into two sets of three. The first
three principles, which I will call [*C-I-A*](../ch1-c-i-a), 
define data access requirements;
the other three, in turn, concern how access is controlled and monitored.
We call these the [*Gold Standard*](../ch1-gold). 
The two sets of principles are interdependent,
and only as a whole do they protect data assets.

Beyond the prevention of unauthorized data access lies the question of
who or what components and systems should be entrusted with access. This
is a harder question of trust, and ultimately beyond the scope of 
information security, even though confronting it is unavoidable 
in order to secure any digital system.

