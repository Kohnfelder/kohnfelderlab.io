---
title: Preface
weight: -40
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

> "If you cannot---in the long run---tell everyone what you have been
> doing, your doing has been worthless." --- Erwin Schrödinger

  Join me on a hike through the software security landscape.

  My favorite hike begins in a rainforest, near the top of the island of
  Kaua’i, which is often shrouded in misty rain. The trail climbs
  moderately at first, then descends along the contour of the sloping
  terrain, in places steep and treacherously slippery after frequent
  rains. Further down, passing through valleys choked with invasive
  ginger or overgrown by thorny lantana bushes, it gets seriously muddy,
  and the less dedicated turn and head back. A couple of miles out, the
  trees thin out as the environment gradually warms, becoming arid with
  the lower elevation. Further on, the first long views of the
  surrounding Pacific begin to open up, as reminders of the promise the
  trail offers.

  In my experience, many software professionals find security daunting
  at first: shrouded in mist, even vaguely treacherous. This is not
  without good reason. If the act of programming corresponded to a
  physical environment, this would be it.

  The last mile of the trail runs through terrain made perilous by the
  loose volcanic rock that, due to the island’s geologically tender age
  of five million years, hasn’t had time to turn into soil. Code is as
  hard and unforgiving as rock, yet so fragile that one small flaw can
  lead to a disaster, just as one misstep on the trail could here.
  Fortunately, the hiking trail’s path along the ridge has been well
  chosen, with natural handholds on the steepest section: sturdy basalt
  outcroppings, or the exposed, solid roots of ohia trees.

  Approaching the end of the trail, you’ll find yourself walking along
  the rim of a deep gorge, the loose ground underfoot almost like ball
  bearings. To your right, a precipice drops over 2,000 feet. In places,
  the trail is shoulder width. I’ve seen acrophobic hikers turn around
  at this point, unable to summon the confidence to proceed. Yet most
  people are comfortable here, because the trail is slightly inclined
  away from the dangerous side. To the left, the risk is minimal; you
  face the same challenging footing, but on a gentle slope, so at worst
  you might slide a few feet. I thought about this trail often as I
  wrote this book and have endeavored to provide just such a path, using
  stories and analogies like this one to tackle the toughest subjects in
  a way that I hope will help you get to the good stuff.

  Security is challenging for a number of reasons: it’s abstract, the
  subject is vast, and software today is both fragile and extremely
  complex. How can one explain the intricacies of security in enough
  depth to connect with readers, without overwhelming them with too much
  information? This book confronts those challenges in the spirit of
  hikers on that trail at the rim of the gorge: by leaning away from the
  danger of trying to cover everything. In the interest of not losing
  readers, I err on the side of simplification, leaving out some of the
  smaller details. By doing so, I hope to avoid readers metaphorically
  falling into the gorge—that is, getting so confused or frustrated that
  you give up. The book should instead serve as a springboard, sparking
  your interest in continued exploration of software security practices.

  As you approach the end of the trail, the ridge widens out and becomes
  flat, easy walking. Rounding the last curve, you’re treated to a
  stunning 300-degree view of the fabled Na Pali coast. To the right is
  a verdant hanging valley, steeply carved from the mountain. A
  waterfall feeds the meandering river visible almost directly below.
  The intricate coastline extends into the distance, flanked by
  neighboring islands on the horizon to the west. The rewards of
  visiting this place never get old. After drinking in the experience, a
  good workout awaits as you start the climb back up.

***************************

Just as I’ll never get to see every inch of this island, I won’t learn
everything there is to know about software security, and of course, no
book will ever cover this broad topic completely, either. What I do
have, as my guide, is my own experience. Each of us charts our own
unique path through this topic, and I’ve been fortunate to have been
doing this work for a long time. I’ve witnessed firsthand some key
developments and followed the evolution of both the technologies and the
culture of software development since its early days.

  The purpose of this book is to show you the lay of the security land,
  with some words of warning about some of the hazards of the trail so
  you can begin confidently exploring further on your own. When it comes
  to security, cut-and-dried guidance that works in all circumstances is
  rare. Instead, my aim is to show you some simple examples from the
  landscape to kick-start your interest and deepen your understanding of
  the core concepts. For every topic this book covers, there is always
  much more to say. Solving real-world security challenges always
  requires more context in order to better assess possible solutions;
  the best decisions are grounded in a solid understanding of the
  specifics of the design, implementation details, and more. As you
  grasp the underlying ideas and begin working with them, with practice
  it becomes intuitive. And fortunately, even small improvements over
  time make the effort worthwhile.

When I look back on my work with the security teams at major software
companies, a lost opportunity always strikes me. Working at a large and
profitable corporation has many benefits: along with on-site massage and
sumptuous cafes come on-tap security specialists (like myself) and a
design review process. Yet few other software development efforts enjoy
the benefits of this level of security expertise and a process that
integrates security from the design phase. This book seeks to empower
the software community to make this standard practice.

  With myriad concerns to balance, designers have their hands full. The
  good ones are certainly aware of security considerations, but they
  rarely get a security design review. (And none of my industry
  acquaintances have even heard of the service being offered by
  consultants.) Developers also have varying degrees of security
  knowledge, and unless they pursue it as a specialty, their knowledge
  is often at best piecemeal. Some companies do care enough about
  security to hire expert consultants, but this invariably happens late
  in the process, so they’re working after the fact to shore up security
  ahead of release. Bolting on security at the end has become the
  industry’s standard strategy—the opposite of baking in security.

  Over the years, I have tried to gently spread the word about security
  among my colleagues. Invariably, one quickly sees that certain people
  get it; others, not so much. Why people respond so differently is a
  mystery, possibly more psychological than technological, but it does
  raise an interesting question. What does it mean to “get” security,
  and how do you teach it? I don’t mean world-class knowledge, or even
  mastery, but a sufficient grasp of the basics to be aware of the
  challenges and how to make incremental improvements. From that point,
  software professionals can continue their research to fill in any
  gaps. That’s the objective that this book endeavors to deliver.

  Throughout the process of writing this book, my understanding of the
  challenge this work entailed has grown considerably. At first, I was
  surprised that a book like this didn’t already exist; now I think I
  know why. Security concepts are frequently counterintuitive; attacks
  are often devious and nonobvious, and software design itself is
  already highly abstract. Software today is so rich and diverse that
  securing it represents a daunting challenge. Software security remains
  an unsolved problem, but we do understand large parts of it, and we’re
  getting better at it—if only it weren’t such a fast-moving target! I
  certainly don’t have perfect answers for everything. All of the easy
  answers to security challenges are already built into our software
  platforms, so it’s the hard problems that remain. This book
  strategically emphasizes concepts and the development of a security
  mindset. It invites more people to contribute to security, to bring a
  greater diversity of fresh perspectives and more consistent security
  focus.

  I hope you will join me on this personal tour of my favorite paths
  through the security landscape, in which I share with you the most
  interesting insights and effective methodologies that I have to offer.
  If this book convinces you of the value of baking security into
  software from the design phase, of considering security throughout the
  process, and of going beyond what I can offer here, then it will have
  succeeded.
