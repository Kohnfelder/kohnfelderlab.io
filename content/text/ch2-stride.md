---
title: Categorizing Threats with STRIDE
weight: -240
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

In the late 1990s, Microsoft Windows dominated the personal computing
landscape. As PCs became essential tools for both businesses and homes,
many believed the company's sales would grow endlessly. But Microsoft
had only begun to figure out how networking should work. The Internet
(back then still usually spelled with a capital I) and this new thing
called the World Wide Web were rapidly gaining popularity, and
Microsoft's Internet Explorer web browser had aggressively gained market
share from the pioneering Netscape Navigator. Now the company faced this
new problem with security: who knew what can of worms connecting all the
world's computers might open up?

While a team of Microsoft testers worked creatively to find security
flaws, the rest of the world appeared to be finding these flaws much
faster. After a couple of years of reactive behavior, issuing patches
for vulnerabilities that exposed customers over the network, the company
formed a task force to get ahead of the curve. As part of this effort, I
co-authored a paper with Praerit Garg that described a simple
methodology to help developers see security flaws in their own products.
Threat modeling based on the *STRIDE* *threat taxonomy* drove a massive
education effort across all the company's product groups. More than 20
years later, researchers across the industry continue to use STRIDE, and
many independent derivatives, to enumerate threats.

STRIDE focuses the process of identifying threats by giving you a
checklist of specific kinds of threats to consider: What can be *spoofed
(S)*, *tampered (T)* with, or *repudiated (R)*? What *information (I)*
can be disclosed? How could a *denial of service (D)* or *elevation of
privilege (E)* happen? These categories are specific enough to focus
your analysis, yet general enough that you can mentally flesh out
details relevant to a particular design and dig in from there.

Though members of the security community often refer to STRIDE as a
threat modeling methodology, this is a misuse of the term (to my mind,
at least, as the one who concocted the acronym). STRIDE is a simply a
taxonomy of threats to software. The acronym provides an easy and
memorable mnemonic to ensure that you haven't overlooked any category of
threat. It's not a complete threat modeling methodology, which would
have to include the many other components we've already explored in this
chapter.

To see how STRIDE works, let's start with spoofing. Looking through the
model, component by component, consider how secure operation depends on
the identity of the user (or machine, or digital signature on code, and
so on). What advantages might an attacker gain if they could spoof
identity here? This thinking should give you lots of possible threads to
pull on. By approaching each component in the context of the model from
a threat perspective, you can more easily set aside thoughts of how it
should work, and instead begin to perceive how it might be abused.

Here's a great technique I've used successfully many times: start your
threat modeling session by writing the six threat names on a whiteboard.
To get rolling, brainstorm a few of these abstract threats before
digging into the details. The term "brainstorm" can mean different
things, but the idea here is to move quickly, covering a lot of area,
without overthinking it too much or judging ideas yet (you can skip the
duds later on). This warm-up routine primes you for what to look out
for, and also helps you switch into the necessary mindset. Even if
you're familiar with these categories of threat, it's worth going
through them all, and a couple that are less familiar and more technical
bear careful explanation.

The STRIDE threat categories correspond to six security goals seen in
Chapter 1 as classic principles. The security goal and
threat category are two sides of the same coin, and sometimes it's
easier to work from one or the other---on the defense (the goal) or the
offense (the threat).

Here's how the match up with a few examples for each:

* **Spoofing** is Authenticity: Phishing, stolen password, 
impersonation, message replay, BGP hijacking

* **Tampering** is Integrity: Unauthorized data modification and deletion,
Superfish ad injection

* **Repudiation** is why we Audit: Plausible deniability, insufficient,
logging, destruction of logs

* **Information disclosure** is Confidentiality: Leaks, side channel, 
weak encryption, data left behind in a cache, Spectre/Meltdown

* **Denial of Service** is Availability: Simultaneous requests swamp 
a web server, ransomware, MemCrashed

* **Elevation of Privilege** is Authorization: SQL injection,
xkcd's "Little Bobby Tables"

Half of the STRIDE menagerie are direct threats to the information
security fundamentals you learned about in Chapter 1: information
disclosure is the enemy of confidentiality, tampering is the enemy of
integrity, and denial of service compromises availability. The other
half of STRIDE targets the Gold Standard. Spoofing subverts authenticity
by assuming a false identity. Elevation of privilege subverts proper
authorization. That leaves repudiation as the threat to auditing, which
may not be immediately obvious and so is worth a closer look.

According to the Gold Standard, we should maintain accurate records of
critical actions taken within the system and then audit those actions.
Repudiation occurs when someone credibly denies that they took some
action. In my years working in software security, I have never seen
anyone directly repudiate anything (nobody has ever yelled "Did so!" and
"Did not!" at each other in front of me). But what does happen is, say,
a database suddenly disappears, and nobody knows why, because nothing
was logged, and the lost data is gone without a trace. The organization
might suspect that an intrusion occurred. Or it could have been a rogue
insider, or possibly a regrettable blunder by an administrator. But
absent any evidence, nobody knows. That's a big problem, because if you
cannot explain what happened after an incident, it's very hard to
prevent it from happening again. In the physical world, such perfect
crimes are rare because activities such as robbing a bank involve
physical presence, which inherently leaves all kinds of traces. Software
is different; unless you provide a means to reliably collect evidence
and log events, no fingerprints or muddy boot tracks remain as evidence.

Typically, we mitigate the threat of repudiation by running systems in
which administrators and users understand they are responsible for their
actions, because they know an accurate audit trail exists. This is also
one more good reason to avoid having admin passwords written on a sticky
note that everyone shares. If you do that, when trouble happens,
everyone can credibly claim someone else must have done it. This applies
even if you fully trust everyone, because accidents happen, and the more
evidence you have available when trouble arises, the easier it is to
recover and remediate.

## STRIDE at the Movies

Just for fun (and to solidify these concepts), consider the STRIDE
threats applied to the plot of the film *Ocean's Eleven*. This classic
heist story nicely demonstrates threat modeling concepts, including the
full complement of STRIDE categories, from the perspectives of both
attacker and defender. Apologies for the simplification of the plot,
which I've done for brevity and focus, as well as for spoilers.

Danny Ocean violates parole (an *elevation of privilege*), flies out to
meet his old partner in crime, and heads for Vegas. He pitches an
audacious heist to a wealthy casino insider, who fills him in on the
casino's operational details (*information disclosure*), then gathers
his gang of ex-cons. They plan their operation using a full-scale
replica vault built for practice. On the fateful night, Danny appears at
the casino and is predictably apprehended by security, creating the
perfect alibi (*repudiation* of guilt). Soon he slips away through an
air duct, and through various intrigues he and his accomplices extract
half the money from the vault (*tampering* with its integrity),
exfiltrating their haul with a remote-control van.

Threatening to blow up the remaining millions in the vault (a very
expensive *denial of service*), the gang negotiates to keep the money in
the van. The casino owner refuses and calls in the SWAT team, and in the
ensuing chaos the gang destroys the vault's contents and gets away.
After the smoke clears, the casino owner checks the vault, lamenting his
total loss, then notices a minor detail that seems amiss. The owner
confronts Danny---who is back in lockup, as if he had never left---and
we learn that the SWAT team was, in fact, the gang (*spoofing* by
impersonating the police), who walked out with the money hidden in their
tactical equipment bags after the fake battle. The practice vault
mock-up had provided video to make it only appear (*spoofing* of the
location) that the real vault had been compromised, which didn't
actually happen until the casino granted full access to the fake SWAT
team (an *elevation of privilege* for the gang). Danny gets the girl,
and they all get away clean with the money---a happy ending for the
perpetrators that might have turned out quite differently had the casino
hired a threat modeling consultant!

