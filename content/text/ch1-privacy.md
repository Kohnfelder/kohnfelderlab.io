---
title: Information Privacy
weight: -150
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

In addition to the foundations of information security---C-I-A and the
Gold Standard---another fundamental topic I want to introduce is the
related field of information privacy. The boundaries between security
and privacy are difficult to clearly define, and they are at once
closely related and quite different. In this book I would like to focus
on the common points of intersection, not to attempt to unify them, but
to incorporate both security and privacy into the process of building
software.

To respect people's digital information privacy, we must extend the
principle of confidentiality by taking into account additional human
factors, including:

* Customer expectations regarding information collection and use

* Clear policies regarding appropriate information use and disclosure

* Legal and regulatory issues relating to handling various classes of
information

* Political, cultural, and psychological aspects of processing personal
information

As software becomes more pervasive in modern life, people use it in more
intimate ways and include it sensitive areas of their lives, resulting
in many complex issues. Past accidents and abuses have raised the
visibility of the risks, and as society grapples with the new challenges
through political and legal means, handling private information properly
has become challenging.

In the context of software security, this means:

* Considering the customer and stakeholder consequences of all data
collection and sharing

* Flagging all potential issues, and getting expert advice where necessary

* Establishing and following clear policies and guidelines regarding
private information use

* Translating policy and guidance into software-enforced checks and
balances

* Maintaining accurate records of data acquisition, use, sharing, and
deletion

* Auditing data access authorizations and extraordinary access for
compliance

Privacy work tends to be less well defined than the relatively
cut-and-dried security work of maintaining proper control of systems and
providing appropriate access. Also, we're still working out privacy
expectations and norms as society ventures deeper into a future with
more data collection. Given these challenges, you would be wise to
consider maximal transparency about data use, including keeping your
policies simple enough to be understood by all, and to collect minimal
data, especially personally identifiable information.

Collect information for a specific purpose only, and retain it only as
long as it's useful. Unless the design envisions an authorized use,
avoid collection in the first place. Frivolously collecting data for use
"someday" is risky, and almost never a good idea. When the last
authorized use of some data becomes unnecessary, the best protection is
secure deletion. For especially sensitive data, or for maximal privacy
protection, make that even stronger: delete data when the potential risk
of disclosure exceeds the potential value of retaining it. Retaining
many years' worth of emails might occasionally be handy for something,
but probably not for any clear business need. Yet internal emails could
represent a liability if leaked or otherwise disclosed, such as by power of
subpoena. Rather than hang onto all that data indefinitely, "just in
case," the best policy is usually to delete it.

A complete treatment of information privacy is outside the scope of this
book, but privacy and security are tightly bound facets of the design of
any system that collects data about people---and people interact with
almost all digital systems, in one way or another. Strong privacy
protection is only possible when security is solid, so these words are
an appeal for awareness to consider and incorporate privacy
considerations into software by design.

For all its complexity, one best practice for privacy is well known:
the necessity of clearly communicating privacy expectations. In
contrast to security, a privacy policy potentially affords a lot of
leeway as to how much an information service does or does not want to
leverage the use of customer data. "We will reuse and sell your data"
is one extreme of the privacy spectrum, but "Some days we may not
protect your data" is not a viable stance on security. Privacy
failures arise when user expectations are out of joint with actual
privacy policy, or when there's a clear policy and it is somehow
violated. The former problem stems from not proactively explaining
data handling to the user. The latter happens when the policy is
unclear, or ignored by responsible staff, or subverted in a security
breakdown.
