---
title: Exposure Minimization Patterns
weight: -430
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

The largest group of patterns call for caution: think "err on the safe
side." These are expressions of basic risk/reward strategies where you
play it safe unless there is an important reason to do otherwise.

### Least Privilege

It's always safest to use just enough privilege for the job.

Handle only unloaded guns. Unplug power saws when changing blades. These
commonplace safety practices are examples of the Least Privilege
pattern, which aims to reduce the risk of making mistakes when
performing a task. This pattern is the reason that administrators of
important systems should not be randomly browsing the internet while
logged in at work; if they visit a malicious website and get
compromised, the attack could easily do serious harm.

The \*nix sudo command performs exactly this purpose. User accounts with
high privilege (known as *sudoers*) need to be careful not to
inadvertently use their extraordinary power by accident or if
compromised. To provide this protection, the user must prefix superuser
commands with sudo, which may prompt the user for a password, in order
to run them. Under this system, most commands (those that do not require
sudo) will affect only the user's own account, and cannot impact the
entire system. This is akin to the "IN CASE OF EMERGENCY BREAK GLASS"
cover on a fire alarm switch to prevent accidental activation, in that
this forces an explicit step (corresponding to the sudo prefix) before
activating the switch. With the glass cover, nobody can
claim to have accidentally pulled the fire alarm, just as a
competent administrator would never type sudo and a command that breaks
the system all by accident.

This pattern is important for the simple reason that when
vulnerabilities are exploited, it's better for the attacker to have
minimal privileges to use as leverage. Use all-powerful authorizations
such as superuser privileges only when strictly necessary, and for the
minimum possible duration. Even Superman practiced Least Privilege by
only wearing his uniform when there was a job to do, and then, after
saving the world, immediately changing back into his Clark Kent persona.

In practice, it does take more effort to selectively and sparingly use
minimal elevated privileges. Just as unplugging power tools to work on
them requires more effort, discretion when using permissions requires
discipline, but doing it right is always safer. In the case of an
exploit, it means the difference between a minor incursion and total
system compromise. Practicing Least Privilege can also mitigate damage
done by bugs and human error.

Like all rules of thumb, use this pattern with a sense of balance to
avoid overcomplication. Least Privilege does not mean the system should
always grant literally the minimum level of authorization (for instance,
creating code that, in order to write file X, is given write access to
only that one file). You may wonder, why not always apply this excellent
pattern to the max? In addition to maintaining a general sense of
balance and recognizing diminishing returns for any mitigation, a big
factor here is the granularity of the mechanism that controls
authorization, and the cost incurred while adjusting privileges up and
down. For instance, in a \*nix process, permissions are conferred based
on user and group ID access control lists. Beyond the flexibility of
changing between effective and real IDs (which is what sudo does), there
is no easy way to temporarily drop unneeded privileges without forking a
process. Code should operate with lower ambient privileges where it can,
using higher privileges in the necessary sections and transitioning at
natural decision points.

### Least Information

It's always safest to collect and access the minimum amount of private
information needed for the job.

The Least Information pattern, the data privacy analog of Least
Privilege, helps to minimize unintended disclosure risks. Avoid
providing more private information than necessary when calling a
subroutine, requesting a service, or responding to a request, and at
every opportunity curtail unnecessary information flow. Implementing
this pattern can be challenging in practice because software tends to
pass data around in standard containers not optimized for purpose, so
extra data often is included that isn't really needed. In fact, you're
unlikely to find this pattern mentioned anywhere else.

All too often, software fails this pattern because the design of
interfaces evolves over time to serve a number of purposes, and it's
convenient to reuse the same parameters or data structure for
consistency. As a result, data that isn't strictly necessary gets sent
along as extra baggage that seems harmless enough. The problem arises,
of course, when this needless data flowing through the system creates
additional opportunities for attack.

It's particularly important to apply this pattern at design time, as it
can be extremely difficult to implement later on because both sides of
the interface need to change together. If you design independent
components suited to specific tasks that require different sets of data,
you're more likely to get this right. 

Considering the Secure by Default pattern as well, the default for the
items parameter should be a minimal set of fields, provided that callers
can request exactly what they need to minimize information flow.

### Secure by Default

Software should always be secure "out of the box."

Design your software to be Secure by Default, including in its initial
state, so that inaction by the operator does not represent a risk. This
applies to the overall system configuration, as well as configuration
options for components and API parameters. Databases or routers with
default passwords notoriously violate this pattern, and to this day,
this design flaw remains surprisingly widespread.

If you are serious about security, never configure an insecure state
with the intention of making it secure later, because this creates an
interval of vulnerability and is too often forgotten. If you must use
equipment with a default password, for example, first configure it
safely on a private network behind a firewall before deploying it in the
network. A pioneer in this area, the state of California has mandated
this pattern by law; its Senate Bill No. 327 (2018) outlaws default
passwords on connected devices.

Secure by Default applies to any setting or configuration that could
have a detrimental security impact, not just to default passwords.
Permissions should default to more restrictive settings; users should
have to explicitly change them to less restrictive ones if needed, and
only if it's safe to do so. Disable all potentially dangerous options by
default. Conversely, enable features that provide security protection by
default so they are functioning from the start. And of course, keeping
the software fully up-to-date is important; don't start out with an old
version (possibly one with known vulnerabilities) and hope that, at some
point, it gets updated.

Ideally, you shouldn't ever need to have insecure options. Carefully
consider proposed configurable options, because it may be simple to
provide an insecure option that will become a booby trap for others
thereafter. Also remember that each new option increases the number of
possible combinations, and the task of ensuring that all of those
combinations of settings are actually useful and safe becomes more
difficult as the number of options increases. Whenever you must provide
unsafe configurations, make a point of proactively explaining the risk
to the administrator.

Secure by Default applies much more broadly than to configuration
options, though. Defaults for unspecified API parameters should be
secure choices. A browser accepting a URL entered into the address bar
without any protocol specified should assume the site uses HTTPS, and
fall back to HTTP only if the former fails to connect. Two peers
negotiating a new HTTPS connection should default to accepting the
more secure cipher suite choices first.

### Allowlists over Blocklists

Prefer allowlists over blocklists when designing a security mechanism.
Allowlists are enumerations of what's safe, so they are inherently
finite. By contrast, blocklists attempt to enumerate all that isn't
safe, and in doing so implicitly allow an infinite set of things you
**hope** are safe. It's clear which approach is riskier.

First, a non-software example to make sure you understand what the
allowlist versus blocklist alternative means, and why allowlists are
always the way to go. During the early months of the COVID-19
stay-at-home emergency order, the governor of my state ordered the
beaches closed with the following provisos, presented here in simplified
form:

> "No person shall sit, stand, lie down, lounge, sunbathe, or loiter on
> any beach . . ."
>
> . . . except when "running, jogging, or walking on the beach, so long as
> social distancing requirements are maintained" (crossing the beach to
> surf is also allowed).

The first clause is a blocklist, because it lists what activities are
not allowed, and the second exception clause is an allowlist, because it
grants permission to the activities listed. Due to legal issues, there
may well be good reasons for this language, but from a strictly logical
perspective, I think it leaves much to be desired.

Let's consider the blocklist: I'm confident that there are other
risky activities people could do at the beach that the first clause
fails to prohibit. If the intention of the order was to keep people
moving, it omitted many---kneeling, for example, as well as yoga and
living statue performances. The problem with blocklists is that any
omissions become flaws, so unless you can completely enumerate every
possible bad case, it's an insecure system.

Now consider the allowlist of allowable beach activities. While it, too,
is incomplete---who would contest that skipping is also fine?---this
won't cause a big security problem. Perhaps a fraction of a percent of
beach skippers will be unfairly punished, but the harm is minor, and
more importantly, an incomplete enumeration doesn't open up a hole that
allows a risky activity. Additional safe items initially omitted can
easily be added to the allowlist as needed.

More generally, think of a continuum, ranging from disallowed on the
left, then shading to allowed on the right. Somewhere in the middle is a
dividing line. The goal is to allow the good stuff on the right of the
line while disallowing the bad on the left. Allowlists draw the line
from the right side, then gradually move it to the left, including more
parts of the spectrum as the "allow" list grows. If you omit something
good from the allowlist, you're still on the safe side of the elusive
line that's the true divide. You may never get to the precise point that
allows all safe actions, at which point any addition to the list would
be too much, but using this technique it's easy to stay on the safe
side. Contrast that to the blocklist approach: unless you enumerate
everything to the left of the true divide, you're allowing something you
shouldn't. The safest blocklist will be one that includes just about
everything, and that's likely to be overly restrictive, so it doesn't
work well either way.

### Avoid Predictability

Any data (or behavior) that is predictable cannot be kept private, since
attackers can learn it by guessing.

Predictability of data in software design can lead to serious flaws,
because it can result in the leakage of information. For instance,
consider the simple example of assigning new customer account IDs. When
a new customer signs up on a website, the system needs a unique ID to
designate the account. One obvious and easy way to do this is to name
the first account 1, the second account 2, and so on. This works, but
from the point of view of an attacker, what does it give away?

New account IDs now provide an attacker an easy way of learning the
number of user accounts created so far. For example, if the attacker
periodically creates a new, throwaway account, they have an accurate
metric for how many customer accounts the website has at a given
time---information that most businesses would be loathe to disclose to a
competitor. Many other pitfalls are possible, depending on the specifics
of the system. Another consequence of this poor design is that attackers
can easily guess the account ID assigned to the next new account
created, and armed with this knowledge, they might be able to interfere
with the new account setup by claiming to be the new account and
confusing the registration system.

The problem of predictability takes many guises, and different types of
leakage can occur with different designs. For example, an account ID
that includes several letters of the account holder's name or ZIP code
would needlessly leak clues about the account owner's identity. Of
course, this same problem applies to IDs for web pages, events, and
more. The simplest mitigation against these issues is that if the
purpose of an ID is to be a unique handle, you should make it just
that---never a count of users, the email of the user, or based on other
identifying information.

The easy way to avoid these problems is to use *securely random *IDs.
Truly random values cannot be guessed, so they do not leak information.
(Strictly speaking, the length of IDs leaks the maximum number of
possible IDs, but this usually isn't sensitive information.) A standard
system facility, random number generators come in two flavors:
pseudorandom number generators, and secure random number generators. You
should use the secure option, which is slower, unless you're certain
that predictability is harmless. See Chapter 5 for more about secure
random number generators.

### Fail Securely

If a problem occurs, be sure to end up in a secure state.

In the physical world, this pattern is common sense itself. An
old-fashioned electric fuse is a great example: if too much current
flows through it, the heat melts the metal, opening the circuit. The
laws of physics make it impossible to fail in a way that maintains
excessive current flow. []{#anchor-6}This pattern perhaps may seem like
the most obvious one, but software being what it is (we don't have the
laws of physics on our side), it's easily disregarded.

Many software coding tasks that at first seem almost trivial often grow
in complexity due to error handling. The normal program flow can be
simple, but when a connection disconnects, memory allocation fails,
inputs are invalid, or any number of other potential problems arise, the
code needs to proceed if possible, or back out gracefully if not. When
writing code, you might feel as though you spend more time dealing with
all these distractions than with the task at hand, and it's easy to
quickly dismiss error-handling code as unimportant, making this a common
source of vulnerabilities. Attackers will intentionally trigger these
error cases if they can, in hopes that there is a vulnerability they can
exploit. The pitfalls are legion, but a number of common traps are worth
mentioning.

Error cases are often tedious to test thoroughly, especially when
combinations of multiple errors can compound into new code paths, so
this can be fertile ground for attack. Ensure that each error is either
safely handled, or leads to full rejection of the request. For example,
when someone uploads an image to a photo sharing service, immediately
check that it is well formed (because malformed images are often used
maliciously), and if not, then promptly remove the data from storage to
prevent its further use.

