---
title: Trust and Responsibility Patterns
weight: -460
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

Trust and responsibility are the glue that makes cooperation work.
Software systems are increasingly interconnected and interdependent, so
these patterns are important guideposts.

### Reluctance to Trust

Trust should be always be an explicit choice, informed by solid
evidence.

This pattern acknowledges that trust is precious, and so urges
skepticism. Before there was software, criminals exploited people's
natural inclination to trust others, dressing up as workmen to gain
access, selling snake oil, or perpetrating an endless variety of other
scams. Reluctance to Trust tells us not to assume that a person in a
uniform is necessarily legit, and to consider that the caller who says
they're with the FBI may be a trickster. In software, this pattern
applies to checking the authenticity of code before installing it, and
requiring strong authentication before authorization.

The use of HTTP cookies is a great example of this pattern, as Chapter
11 explains in detail. Web servers set cookies in their response to the
client, expecting clients to send back those cookies with future
requests. But since clients are under no actual obligation to comply,
servers should always take cookies with a grain of salt, and it's a huge
risk to absolutely trust that clients will always faithfully perform
this task.

Reluctance to Trust is important even in the absence of malice. For
example, in a critical system, it's vital to ensure that all components
are up to the same high standards of quality and security so as not to
compromise the whole. Poor trust decisions, such using code from an
anonymous developer (which might contain malware, or simply be buggy)
for a critical function quickly undermines security. This pattern is
straightforward and rational, yet can be challenging in practice because
people are naturally trusting and it can feel paranoid to withhold
trust.

### Accept Security Responsibility

All software professionals have a clear duty to take responsibility for
security; they should reflect that attitude in the software they
produce.

For example, a designer should include security requirements when
vetting external components to incorporate into the system. And at the
interface between two systems, both sides should explicitly take on
certain responsibilities they will honor, as well as confirming any
guarantees they depend on the caller to uphold.

The anti-pattern that you don't want is to someday encounter a problem
and have two developers say to each other, "I thought you were handling
security, so I didn't have to." In a large system, both sides can easily
find themselves pointing the finger at the other. Consider a situation
where component A accepts untrusted input (for example, a web frontend
server receiving an anonymous internet request) and passes it through,
possibly with some processing or reformatting, to business logic in
component B. Component A could take no security responsibility at all
and blindly pass through all inputs, assuming B will handle the
untrusted input safely with suitable validation and error checking. From
component B's perspective, it's easy to assume that the frontend
validates all requests and only passes safe requests on to B, so there
is no need for B to worry about security at all. The right way to handle
this situation is by explicit agreement; decide who validates requests
and what guarantees to provide downstream, if any. For maximum safety,
consider Defense in Depth, where both components independently validate
the input.
