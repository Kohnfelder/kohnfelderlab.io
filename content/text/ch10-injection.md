---
title: Injection Attacks
weight: -1050
---

__The following is an excerpt from the
book *Designing Secure Software: A Guide for Developers* by
Loren Kohnfelder, Copyright 2022,
[No Starch Press](https://nostarch.com/designing-secure-software)__

---

> "If you ever injected truth into politics you would have no politics."
---Will Rogers

Unsolicited credit card offers comprise a major chunk of the countless
tons of junk mail that clog up the postal system, but one clever
recipient managed to turn the tables on the bank. Instead of tossing out
a promotional offer to sign up for a card with terms he did not like,
Dmitry Agarkov scanned the attached contract and carefully modified the
text to specify terms extremely favorable to him, including 0% interest,
unlimited credit, and a generous payment that he would receive should
the bank cancel the card. He signed the modified contract and returned
it to the bank, and soon received his new credit card. Dmitry enjoyed
the generous terms of his uniquely advantageous contract for a while,
but things got ugly when the bank finally caught on. After a protracted
legal battle that included a favorable judgment upholding the validity
of the modified contract, he eventually settled out of court.

This is a real-world example of an *injection attack*: contracts are not
the same as code, but they do compel the signatories to perform
prescribed actions in much the same way as a program behaves. By
altering the terms of the contract, Dmitry was able to force the bank to
act against its will, almost as if he had modified the software that
manages credit card accounts in his favor. Software is also susceptible
to this sort of attack: untrusted inputs can fool it into doing
unexpected things, and this is actually a fairly common vulnerability.

There is a common software technique that works by constructing a string
or data structure that encodes an operation to be performed, and then
executing that to accomplish the specified task. (This is analogous to
the bank writing a contract that defines how its credit card service
operates, expecting the terms to be accepted unchanged.) When data from
an untrusted source is involved, it may be able to influence what
happens upon execution. If the attacker can change the intended effect
of the operation, that influence may cross a trust boundary and get
executed by software at a higher privilege. This is the idea of
injection attacks in the abstract.

Before explaining the specifics of some common injection attacks, let's
consider a simple example of how the influence of untrusted data can be
deceptive. According to an apocryphal story, just this kind of confusion
was exploited successfully by an intramural softball team that craftily
chose the name "No Game Scheduled." Several times opposing teams saw
this name on the schedule, assumed it meant that there was no game that
day, and lost by forfeit as no-shows. This is an example of an injection
attack because the team name is an input to the scheduling system, but
"No Game Scheduled" was misinterpreted as being a message from the
scheduling system.

The same injection attack principles apply to many different
technologies (that is, forms of constructed strings that represent an
operation), including but not limited to:

* SQL statements

* File path traversals

* Regular expressions (as a denial-of-service threat)

* XML data (specifically, XXE declarations)

* Shell commands

* Interpreting strings as code (for example, JavaScript's eval function)

* HTML and HTTP headers (covered in Chapter 11)
