---
title: Running the “Reflections on Trusting Trust” Compiler 
date: 2023-10-25
tags: ["security"]
draft: false
---

In the Afterword of my book I mention Ken Thompson's classic
"Trusting Trust" paper showing how to embed a backdoor into
an OS by compiling the compiler such that the modification
is invisible in source code.

Recently the author revealed that he had done the experiment and
still had the code, so someone shared it with annotations.
Now for the first time you can see exactly how it works.

https://research.swtch.com/nih

Wonderful that we now can learn from that code written long ago.

