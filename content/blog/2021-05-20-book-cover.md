---
title: Cover design
date: 2021-05-20
tags: ["publishing"]
draft: false
---

Once the title was chosen by the publisher (in consultation, of course) 
the cover design becomes the next topic of discussion. 
Cover design is up to the publisher, and I'm quite happy with that having
zero experience. My input was that I personally prefer the dark background
style of their security book series, while being open to anything the
publisher might propose.

It's difficult to map from an abstract topic like secure software design to
something in the physical world that can be portrayed on a book over. Some
covers are just title text, and the "Matrix"-style spew of digital fixed
width characters like a text dump is overdone.

My ideas for cover themes are admittedly not great, but I wanted to focus
on secure by design as a central topic. (listed in no particular order) 
* A turtle with a nice shell beside a hermit crab with an shabby, ill-fitting
shell 
* Someone building a house on bedrock 
* The construction of a Roman arch bridge (which was very clever for its time)

*Update:* From a subsequent cover discussion, a new idea of an armored robot
came up that the publisher thought promising. The graphic artist evolved this
concept into the final cover art that has generally received 
a very positive reaction.
