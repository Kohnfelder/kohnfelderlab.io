---
title: Sales categories adjustment
date: 2021-07-15
tags: ["publishing"]
draft: false
---

When the book appeared in the [Amazon marketplace](https://www.amazon.com/dp/1718501927) I set up an author's page and claimed the title. I expected some sort of verification, but they just granted me the book I asked for. This allows me to publish my bio and also see reports on sales ranking and so forth.

I don't know how the book gets assigned to categories, but it was listed in "Programming Languages" -- while the book includes code and uses programming languages, it's hardly about them. If anything, the book attempts to span many languages and operating systems, only using specific instances as examples. Additionally, the book was not in a security category though it clearly should be.

> Books > Computers & Technology > Security & Encryption

This change must be made via the publisher (I learned) and I've requested that.

Still in pre-order and ahead of any public-facing marketing, the current ranking is what one would expect.

> #609,529 in Books - Down 87,307 spots since yesterday

This is a great starting point with the only direction to go being up.
