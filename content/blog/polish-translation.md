---
title: Polish translation is on sale
date: 2023-05-05
tags: ["publishing"]
draft: false
---

Wspaniale jest widzieć, że ukazało się polskie tłumaczenie mojej książki o
bezpieczeństwie oprogramowania.
(It's great to see that the Polish translation of my book on
software security is out. )

https://helion.pl/ksiazki/po-pierwsze-bezpieczenstwo-przewodnik-dla-tworcow-oprogramowania-loren-kohnfelder,popbez.htm#format/d

![Book cover](https://static01.helion.com.pl/global/okladki/326x466/popbez.png)

**Dziękuję!**

I know of three additional language translations in the works.
