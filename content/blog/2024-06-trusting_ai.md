---
title: Trusting AI
date: 2024-06-03
tags: ["security"]
draft: false
---

Whenever considering applications that rely on generative AI,
I believe we always need to ask if we can trust it.
And given the technology's track record it's hard to imagine
how we are going to honestly be able to say "100%" any time soon.
That's why, for the time being, I think the following guideline
will be very important.

> Only use generative AI when there exists only minor downsides if it's wrong.

Read the full article:
[Trusting AI](/writings/trusting_ai)

\#
