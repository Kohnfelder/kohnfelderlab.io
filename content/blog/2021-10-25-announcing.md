---
title: Announcement
date: 2021-10-25
tags: ["promo"]
draft: false
---

I’m proud to announce my new software security book, *Designing Secure Software: A Guide for Developers*. I wanted to create something a little different: broadly readable rather than expert targeted, general approaches over specific details, all based on direct personal experience. 

https://designingsecuresoftware.com/

Two central themes run through the book: encouraging software professionals to focus on security early in the software construction process, and involving the entire team in the process of—as well as the responsibility for—security. There is certainly plenty of room for improvement in both of these areas, and this book shows how to realize these goals. 

I convey the security mindset from first principles and show how to bake in security throughout the development process. Along the way I provide examples of design and code, largely independent of specific technologies so as to be as broadly applicable as possible. The text is peppered with numerous stories, analogies, and examples to add spice and communicate abstract ideas as effectively as possible. 

Available soon (e-book on sale now at No Starch Press) [here](https://nostarch.com/designing-secure-software).

&nbsp;
