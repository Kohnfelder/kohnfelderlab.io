---
title: "Scope of the book"
date: 2021-11-03
tags: ["security"]
---

One big learning for me from writing a book on software security is
realizing the importance of context to security. There was a constant
challenge of discovering the right scope — what needs adding, and what
can be cut to keep it concise. Each chapter of the book could well have
been an entire book itself, but nobody is going to read what would consume
a foot of shelf space. Software security can go wrong in so many ways
that there is always more to say, different approaches to take, various
pros and cons of different mitigations, further interesting details to
consider. And of course new vulnerabilities keep popping up, offering more
examples to learn from, and suggesting various new mitigation techniques
that might have prevented the problem.

After having chosen the right content, the writer must anticipate what
the target readership already knows. When you start writing the question
of what the readership already does or doesn’t know, and therefore what
you need to explain arises and reveals how imprecise one’s notion of
this is. Balancing between losing some readers by providing too little
background, against boring the more knowledgeable with too much is one
of those judgments with no clear best answer even with perfect future
information.

There are other formidable challenges to determining the scope and content
of the book as well. When writing about material that one knows best,
it’s exceptionally difficult to recall how you learned it, and what
the beginner’s mind needs in order to learn.

And then there is the question of what topics to cover, and in how much
depth. I decided early on that a complete compendium of every known
software vulnerability and mitigation would be so voluminous that, even
if I cataloged it, likely nobody would ever read it all anyway.

In the end, I chose to fall back on my personal experience which spans
enough years to give a broad perspective. My target reader is an amalgam
of people I have interacted with professionally: fellow programmers,
but also managers and various specialists who don’t write code but are
deeply involved in the software process. It’s admittedly an arbitrary
criterion, but to the extent that it works, it solves these problems
nicely and provides clear guidance that led me through the writing.
