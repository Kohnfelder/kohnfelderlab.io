---
title: Epigrams in the book
date: 2022-01-02
tags: ["publishing"]
draft: false
---

I like clever sayings and wise proverbs, the shorter the better,
so I wanted to work a number of them into the book. 
There are [45 of them in there](/page/epigrams),
and for the most part I think they are at least relevant if not insightful.
However, I admit that a few were just so good I bent the rules to include them.
I hope the add a little spice to the book, provide useful perspective,
perhaps educate or even provide a chuckle to readers.

When I felt stuck in the writing, it proved to be a nice technique to
would spend a little time poking around the web for good quotes to go with 
the topic at hand, or sometimes to fill a gap elsewhere 
(from a list of places I wanted to find quotes). 
Usually, a few minutes searching was boring enough that I could 
return to writing afresh thanks to that slight diversion.

Also along the way I kept a file of good quotes that I had no spot for, 
and sometimes would find gold there later in a completely different context. 
I was very unsure if these would make it past the editors, 
but throughout the entire book there never was a single remark 
about any of the quotations. 

My personal preference is for short quotations, and I wrote 
a simple script to sort a web page by word count (of <P> paragraphs) 
so I could skim the shorter ones quickly. A friend with a law degree 
advised me to strictly avoid song lyrics as those are particularly well 
defended. The other filter I applied was to avoid quoting people whom 
history has not been kind to, because we can move forward without their 
contributions.
