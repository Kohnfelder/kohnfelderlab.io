---
title: Copy edit begins
date: 2021-03-01
tags: ["publishing"]
draft: false
---

Copy edit phase starts this week with two chapters (Ch 1 and 4).
The chapters that resulted from developmental editing have been
converted to a new style regime, and are now ODT instead of DOCX format.
Each chapter arrives with edits and comments from the copy editor.

At her suggestion, in this process I review tracked changes but don't
accept or reject them because this would lose her edits. Instead,
I comment or provide my own tracked changes for her review, with any
edits left alone implicitly to be accepted.

For comments I reply to each and every one. Often comments will describe
a change that I can agree to or counter with other ideas.
The idea of using a comment to describe a change is it allows the copy editor
to explain why for context, and this is quite valuable.

Since I'm conscientious about reviewing and responding this system immediately
works well. Ch 4 takes three roundtrips of copy edit to finalize;
Ch 1 takes two. We agree to proceed at two chapter per week pace.
