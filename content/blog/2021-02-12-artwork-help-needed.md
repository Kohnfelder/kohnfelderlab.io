---
title: Artwork help needed
date: 2021-02-12
tags: ["publishing"]
draft: false
---

Perhaps it was due to poor grades in Art class back in my school days,
but I quickly realized that the sketches I did for graphics in the book
we not going to be up to par. Having never worked with a pro, I didn't
invest a lot of time as the drawings were not getting better it seemed,
and when production began the publisher sent them to a pro to fix up.

I did provide notes for each drawing listing the key points (I could
describe in words better than render in art what I wanted to say) that
I hoped would bridge the gap.  The first batch was pretty good,
considering that Richard (I later learned the artist's name) surely
had no idea what the symbols were meant to depict.

I was able to add three more graphics that came late in the process.
During editing of the chapters a few places it because clear that
what I wanted to say was difficult to convey, and a graphic depiction
suggested itself. (Sometimes getting the words right is just hard.)

Katrina accepted my notes and the files containing my crude drawings,
and was off finding somebody to make it all ship-shape.
