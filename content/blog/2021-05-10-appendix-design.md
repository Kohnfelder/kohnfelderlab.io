---
title: Layout design for Appendix A
date: 2021-05-10
tags: ["publishing"]
draft: false
---

Writing about software design is difficult because there is little to no standard methodology or practice -- everyone does it differently. So there is a challenge as to how to show secure software design.

The idea that I came up with was to write a software design, and then show via highlighting the parts of the design that address security. My original vision was that this would be an appendix in the book that would look like a real working software document that was included as an example.

Once again I was pleased that the No Starch folks immediately embraced the idea and helped me work out exactly how to do that. Here is the rough concept that I pitched.

* Most important this is to have a different look and feel from the main text of the book to signal that it's a separate document that's attached. 
* I suggest using sans serif font as a primary indication that it's different.
* Design documents, like the link above, are very structured, have minimal styling, with emphasis on being functional (and hence a little ugly).
* Perhaps an additional header as for the design document (in addition to the book's footer showing page number, Appendix A).

The production editor worked with the compositor, and that's basically how it turned out: sans serif, a different look and feel more like a design doc.
