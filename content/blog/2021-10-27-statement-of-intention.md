---
title: Statement of Intention
date: 2021-10-27
tags: ["security"]
draft: false
---

I believe that we can do so much better at delivering more secure software, and my book explains how we could do that. While there are a few new ideas in there, it’s mainly about covering well established methodology with focus on showing how to put it into practice. The book takes a different approach to the topic of software security to reach as broad an audience of software professionals as possible because I think there is often an over-reliance on "experts". 

The book challenges some industry practices where I see clear opportunities for improving. 
> “Bridges, roads, buildings, factories, ships, dams, harbors, and rockets are all designed and meticulously reviewed to ensure quality and safety, and only then built. In any other engineering field, it’s acknowledged that refining a design on paper is better than retrofitting security measures after the fact. Yet most software is built first and then secured later.”

While I wouldn't say that every programmer needs to know this stuff, I would say that it's the exceptional case where software security can be ignored (and it's an interesting question to determine exactly under what conditions such an exception applies). So this book offers all software professionals a change to level up, or fill in some weak gaps in their understanding. And for security experts, I think it provides new ways of thinking and communicating to clients.

&nbsp;
