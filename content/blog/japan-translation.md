---
title: Japanese translation is on sale
date: 2023-08-15
tags: ["publishing"]
draft: false
---

8月18日、新刊『セキュアなソフトウェアの設計と開発』を発刊

The Japanese translation of my book goes on sale this week.
This is particularly rewarding for me because I lived and worked
there for about ten years and speak the language well enough to
read the text (but nowhere near ability write at a publishable
level of quality). I'm quoted in the
[press release](https://prtimes.jp/main/html/rd/p/000000121.000049716.html),
wrote a special preface ([English version](/page/japan-intro)),
and there's an interview with the translators in the book
(all in Japanese, obviously).

![Book cover](https://m.media-amazon.com/images/I/51wREyeJR5L._SX385_BO1,204,203,200_.jpg)

On sale at [Amazon Japan](https://www.amazon.co.jp/dp/4798069752/)
and other bookstores.

I know of two additional language translations in the works.

