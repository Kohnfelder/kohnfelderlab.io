---
title: Better security discussions
date: 2024-05-26
tags: ["security"]
draft: false
---

I've been a fan of threat modeling for many years, but only recently seeing
that it isn't just behind-the-scenes work for software professionals to do.
Threats and mitigations need to be part of any discussion about security.
That is, news articles that urgently warn of the latest zero-day or when
privacy advocates decry the latest outrage from the big platforms, to frame
a good discussion requires outlining the threat model you are talking about.

Of course some of this is mentioned some of the time, but I would say it needs
to be explicitly stated to be effective. Only when both sides ("nothing to see
here" versus "the sky is falling") lay out threats and mitigations can we
decide which position is more reasonable. Is one side ignoring a real threat,
or the other imagining one that isn't such a problem? Once we can see a list
of potential threats then we can assess the mitigations against them.

Read the full article:
[Better Security Discussions](/writings/better)

\#
