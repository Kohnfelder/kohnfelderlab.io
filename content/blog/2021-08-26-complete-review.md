---
title: Complete book proof review
date: 2021-08-26
tags: ["publishing"]
draft: false
---

This week I reviewed the complete book PDF, checking that previous changes
were successfully made by the compositor. 
With luck that should be make it final.

The review consisted of comparing (with the handy DiffPDF tool) the previous
proofs to the latest complete PDF, and also checking each annotation from the
last review pass to see how it ended up. (Since the previous proof reviews
were done on individual chapter PDF files, I used pdfunite command to make
a full book PDF and then compared that. This worked exactly as I hoped.)

Most changes landed successfully, but there were a number of issues, mostly
small. Copy editing happens through redlines (tracked changes) such that one
can see the effect of all changes, or even make the redlines go away
temporarily to confirm the end result. Working in PDF, however, one must
describe the change and it isn't actually visible until later when the
compositor interprets the directions -- and sometimes the result of what I
asked for doesn't work when it comes back in black and white. 

At this point the index with page number references is in the proofs, so
nothing can cause text flow that would cause page numbering to shift at all.
Fortunately, all changes are tiny, and so long as paragraphs don't change their
length in number of lines everything is quite stable.

In addition to fine tuning the last set of changes, the inspection of diffs
identified one new error that appeared (two characters " d" were inserted
near one of the changes), possibly a cat walking on the compositor's keyboard?
