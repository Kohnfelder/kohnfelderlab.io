---
title: Designing Secure Software Blog
---

The book *Designing Secure Software: a guide for developers* took nearly two years from finding a publisher to publication. 
These posts tell part of my experience developing the book and writing it.
Writing a book is the ideal project for riding out a pandemic.

