---
title: "Complete Mediation in Sci-Fi"
date: 2021-08-30T17:59:03-10:00
tags: ["security"]
---

In the book [*Project Hail Mary* by Andy Weir](https://www.andyweirauthor.com/books/project-hail-mary-hc/project-hail-mary-el), 
there is a short scene (on page 339)
that features a very clear example of failure to implement Complete Mediation
(one of the secure design patterns described in Chapter 4 of my book).

The scene opens with the protagonist recently injured and struggling 
in great pain while working hard to save humanity.
When he asks the robotic medic for painkillers, it says that he must wait
a few more hours to comply with the required interval between doses.

Unable to override the restriction, instead, he asks the current time,
and then directs the computer to set the current time ahead by a few hours.
Now simply by repeating his request the medicine is promptly provided.

The book has the protagonist think how "stupid" the system is, that 
astronauts "trusted to save the world" should be able to self-prescribe.
That may be, (and I am not an MD) but even for the most trustworthy of
astronauts, there is a valid argument for enforcing
limits on medicines that are potentially addictive.

From a security perspective, this is a lapse of strong enforcement.
If the computer is going to enforce any time-based restrictions on the
crew it cannot allow them to change the current time setting -- 
a clean example of the Complete Mediation pattern.

As an aside, I note that on an interstellar spaceship implementing time
is particularly challenging. It's unclear that knowing the time according
to an Earth time zone seems quaint and utterly useless, especially since it's
far enough that even radio communication is impossible. 
On top of that, this spaceship travels at relativistic speeds so it's an
open question as to what reference frame such a time would even be.
