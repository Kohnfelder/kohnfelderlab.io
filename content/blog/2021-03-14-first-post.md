---
title: Going live at kohnfelder.gitlab.io
date: 2021-03-14
tags: ["website"]
---

This is the obligatory post that accompanies the skeleton first draft of the website.
Setting up Hugo at GitLab was easy:
fork Hugo from https://gitlab.com/pages/hugo, follow the simple instructions,
and start editing files in the `content/` directory.

Working from the sample website makes it easy to intuit
the magic in the `config.toml` file.
Running the `hugo` command you can watch the localhost hosted webpage
(`http://localhost:1313/`) auto-update with every save.
Commit the edits and a GitLab does the rest through more CI/CD magic.

One minor glitch with Hugo is that if you delete content
(for example, from the sample website),
you need to `rm -r` the `public/` directory to regenerate it cleanly.
I have no idea if Hugo is lazy and only does partial updates,
there is some optimization skipping unchanged files that doesn't account for deletions,
or if this is an attempt at keeping old URLs still responding to reduce 404s.
