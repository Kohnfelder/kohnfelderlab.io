---
title: References links checking
date: 2021-11-09
tags: ["publishing"]
draft: false
---

While writing this book I curated a list of web references for sources 
of material or details beyond what made sense to incorporate in the text.
The publisher's house style did not allow for footnotes, so the References
appendix became a list of URLs organized by chapter. 

Several times while exchanging drafts during the editing during the editing
process, links that had worked when the first draft went out were broken
by the time the editor responded. Link rot on the web was much faster and
more common than I expected.

Rather than put a lot of URLs on paper that were liable to change we decided
to move the [References](/page/references) appendix online where it can be 
updated. From a brief search I did not find a good existing tool so I wrote 
some code (Python) to check, but it simple and ad hoc and doesn't work well 
on a few sites (giving false negatives). 
I manually run it from time to time and it works well enough.
(There's a long discussion here: something about software that makes it
easy to write a quick and dirty tool that works pretty well, but from there
making it solid and generally useful and efficient and adding the obvious
cool features quick becomes a major undertaking ... and we quickly give up.
The result is that there must be thousands (?) of one-off tools like this,
but they aren't good enough to share and grow into really slick tools.)

Since completing the book (late August 2021), here is a list of updates:

1. OpenSSL moved the web home of their git repository.

1. One [link](https://www.ida.org/-/media/feature/publications/i/in/initial-analysis-of-underhanded-source-code/d-13166.ashx) 
currently is temporarily broken (for over a week) as the company moves location.
```
The good news: We’re moving our headquarters to Potomac Yard in Alexandria, Virginia!
The bad news: This site is down for maintenance as we prepare to move our servers.
Thanks for your patience.
We expect to be up and running Tuesday, November 16, 2021.
```

---

