---
title: Demand more
date: 2024-09-20
tags: ["security"]
draft: false
---

### Demand more

I applaud CISA leadership speaking out aggressively at the 
[mWISE Conference 2024](https://mwise.mandiant.com/conf24/keynotes) 
about the dismal state of software security 
(based on [reporting in The Register](https://www.theregister.com/2024/09/20/cisa_sloppy_vendors_cybercrime_villains/), 
but it would be nice for 
[www.cisa.gov](http://www.cisa.gov) to publish transcripts
in order to ensure we are interpreting remarks with full context,
given that the videos are paywalled). 

While agreeing with the aspirations and good intentions,
behind the words I worry about how CISA views the problem.
It's very bold, yet I would say it stops short of being actionable.

What follows are a few quotes from the article on the speech with
my observations:

*"Why does software require so many urgent patches?"* 

* We require many patches because software is fragile 
  and corporations prioritize new feature development 
  over meticulous testing and bug fixing. This is a well-known,
  industry-wide, pervasive strategy driven by profit-making motives.

* Also it should be noted that software engineers (almost without 
  exception) favor new development over debugging and maintenance.

*"The truth is: We need to demand more of technology vendors."*

* How in the world do we demand more when everyone is already locked
  into a platform?  
* Is there any viable commercial operating system that CISA deems acceptable?
  If so, they should identify it and explain why; if not,
  then it seems nobody knows how to do better.  
* What exactly is the demand? Zero defects seems unachievable,
  so what is the ask?

*"Despite a multi-billion-dollar cyber security industry,
we still have a multi-trillion-dollar software quality 
issue leading to a multi-trillion-dollar global cyber crime issue."*

* Naturally, because most of that "multi-billion-dollar cyber
  security industry" is spent on after-market products attempting
  to shore up fundamentally insecure systems.  
* Security-first operating systems (e.g. OpenBSD, Qubes OS) are
  rarely used because enterprises favor standard platforms,
  convenience, rich 3rd party markets, etc.

"*We don't need more security products – we need more secure products.*"

* True if security were top priority but (judging by actions)
  few enterprises seem to agree.  
* The dominant operating systems are millions of lines of
  code developed over decades with strict legacy compatibility requirements, 
  with accumulated technical debt on a scale with the US national debt. 
  Retroactively raising the security bar significantly would be a massive
  multi-year undertaking with little or no promised economic return.

In conclusion, I completely agree that it would be awesome if the 
changes CISA seeks ever came to pass, but I don't see how we get 
from here to there. To put the situation in blunt economic terms, 
today the costs of security risk are an externality for 
software makers that customers are forced to accept. 
Externalities (meaning, in industrial terms, 
dumping waste into the river instead of designing a zero-emissions factory) 
are always grossly inefficient in overall cost, but do serve to 
save costs at the source, hence the aforementioned 
"multi-trillion-dollar global cyber crime issue".

Operating system companies own the platforms so they call the shots, 
and doing what CISA wants would mean incurring massive red ink with 
only a vision of a better world in return (CISA isn't suggesting 
software should cost more if we ever got better security). 
The rest of the industry is downstream of the platforms and 
it's very questionable that they can make much of a dent in 
the problem unless the operating systems change first 
(because you can't build secure apps on top of an operating system 
that isn't designed with that as a priority, 
just as a stock car is never going to win a Formula 1 race). 

The only alternative would be an equally massive effort to 
design and build a competitive operating system from scratch 
with security as top priority, and the prospects of a lucrative 
market for such a hypothetical product are at best speculative 
(especially compared to the very profitable and stable status quo).

So how do we make progress on security? I see three broad approaches:

* Ask nicely? (not likely to work)  
* Make *specific achievable security demands* of the software industry, 
starting with the platforms, coupled with broad support from 
customers to pay for such improvement.  
* Build *new secure-by-design platforms* incorporating 
all we've learned and transition there. This would be 
a "moonshot" scale endeavor, but if realized would be 
a powerful new technology base to build a secure digital future.

Which road ahead should we take, any other ideas,
or just stick with the status quo?

&there4;&nbsp;&there4;&nbsp;&there4;&nbsp;&there4;

&nbsp;
