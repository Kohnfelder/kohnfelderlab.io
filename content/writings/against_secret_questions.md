---
title: "Secret Questions for password reset"
date: 2024-07-11
tags: ["security"]
draft: false
---


Secret questions as credentials for online account authentication are
simply a bad idea in my view: I have never seen them done well, often
seem them done atrociously, and my most generous assessment would be
that they are extremely hard to do well. But keeping an open mind,
here\'s a brief reasoning why these are problematic, and I invite anyone
interested to do a brilliant design and prove me wrong.

There is no standard practice for secret questions to analyze (which is
problematic) but to start here\'s what I see:

-   Users register their answers to a few questions that they must match
    later to prove it\'s them.
-   Questions should be designed to have answers unique to the user they
    won\'t forget, not known to attackers.
-   Password reset requires answering the questions with the same
    answers in order to regain access.

Different implementations widely differ in all details so let me stick
to fundamentals at the expense of abstraction.

-   Questions about personal details potentially incur privacy harms on
    people. 
-   If you let the person write their own question, naive people will
    write poor questions with weak security. 
-   Answers change over time (I have to remember my favorite movie as of
    whenever I answered that long ago).
-   Multiple choice answers are easily faked by trial and error.
-   Questions with seemingly one obvious answer don\'t for some
    (adoptees may have more than two parents and not even know the
    identity of their birth parents, city of birth for someone born at
    sea, etc.).
-   Questions shouldn\'t have very common answers (millions of people
    were born in any large city).
-   Free form answers are hard to compare (\"New York, NY\" and \"NYC\"
    are the same, or even \"Bronx\").
-   If humans interpret answer matching it means storing plain text
    (leak risk) and introducing unreliable and adds subjectivity that
    makes results variable depending on many factors (some people may
    freak out thinking they could lose their account, and that makes
    them nervous, forgetful, even suspicious sounding).
-   It should go without saying that generative AI isn\'t ready for this
    where false negatives are disastrous.
-   Seeming private personal details may leak out in social media posts,
    photos, replies, and so forth.
-   Social engineering can easily extract personal details (take this
    survey to win a prize).
-   Important details like employer, previous addresses, birthday,
    mother\'s maiden name are shared elsewhere.
-   Consider that people in abusive relationships may be attacked by
    others who know them intimately.
-   Unless all websites ask different secret questions, there is risk of
    leaks across sites (the shared password risk).

People have all kinds of life experiences, memory is fickle, some Q&A
that seem harmless might be difficult to answer for folks with unusual
life circumstances, be taken as a privacy invasion, and more. What
questions must every single person online be fully expected to
unambiguously know the answer to, that is easy to remember accurately
(ruling out identification numbers), that doesn\'t vary over time, that
is unknown by others except people they can fully trust, and is not
already recorded in many databases and already used for authentication?
(And, in order to avoid cross-site leaks, how is every website going to
have a unique set of such qualified questions?)

How exactly are Secret Questions used in the password reset process? Do
people get multiple chances to give the right answer or not, do you tell
them if the answer is right or not, and are timeouts enforced to prevent
brute force attacks? How are answers provided compared to those
registered? Is (e.g. 2 out of 3 right) a pass? How do you prevent people
being phished (including MitM) for answers if they aren\'t
authenticated? Are Secret Questions a single gate to password reset, or
one factor in a more comprehensive evidence evaluation?

My best way of dealing with secret questions is to type
gibberish rather than disclose my mother\'s maiden name. If I\'m being
conscientious I will record these to provide later --- it just becomes
more password entropy, and unless I use a password manager or encrypted
storage it\'s exposed. Alternatively, if challenged by phone support
staff (I think I actually got away with it once) I say that it must be a
software bug since that cannot possibly be the answer.

It would be nice if this technique worked without any of the downsides
mentioned above (and more if this analysis is incomplete).
Fundamentally there's something very unfriendly about requiring people
to answer questions you insist they must, and as mentioned already
it isn't always secure to let them choose for themselves either.
I don't see how but would be very interested to see the best of the best
design and be proven wrong.
