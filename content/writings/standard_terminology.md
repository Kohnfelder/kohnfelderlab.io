---
title: 'CSRC NIST glossary'
date: 2024-07-25
---


> In search of standard terminology to talk about software security 

Someone referred to Crowdstrike as not being a "security incident" to which
someone else responded that according to NIST it is. I'd like to think that the
US National Institute of Standards and Technology (NIST) would provide standard
definitions of technical terms for the software community which is awash in
vague nomenclature that leads to much confusion, however, I see that the
situation is unexpectedly complicated. According
[https://csrc.nist.gov/glossary/term/security\_incident](https://csrc.nist.gov/glossary/term/security\_incident)
there are eight different definitions of "security incident". This strikes me
as fundamentally unhelpful: if we are having a discussion and referencing the
NIST definition, we can disagree about the meaning and both be completely
accurate. If there is some rule to determine which of the definitions to apply
in different contexts the webpage appears to be silent on what that is.



Let's look at the top definition (what determines the order?): "An occurrence
that actually or potentially jeopardizes the confidentiality, integrity, or
availability of an information system or the information the system processes,
stores, or transmits or that constitutes a violation or imminent threat of
violation of security policies, security procedures, or acceptable use
policies." It's hard to think of any significant change in the production
environment  that does *not* actually or potentially jeopardize the
confidentiality, integrity, or availability: a routine update, restoring a
database from backup, almost any sudo command\! The hypothetical standard of
"potentially jeopardizes" seems distinctly unlike my sense of an "incident" (an
occurrence) and only excludes operations that are 100% safe and secure, which
anyone with much software experience knows almost nothing qualifies as. And I'd
say not only does this not jive with most people's common sense of the meaning,
it's subjective because it's couched as "potentially" it's almost an ongoing
condition happening so frequently there's little point to the term.



It appears the glossary is produced by automation extracting definitions from
applicable NIST publications, so unless you are very familiar with those
publications citing these for most purposes may be outside the intended use.
However, as an industry we could use a well crafted stable and useful set of
clear definitions — if that is outside NIST's purview, should we build such a
thing?




