---
title: Incoming message mess
date: 2024-07-30
tags: ["security"]
draft: false
---

> July 30, 2024 — When will we address the unacceptable status quo of scam phone calls, SMS text, and email?

Today I got a cold call from someone purporting to represent my health insurer 
(I don't actually doubt they do, but it would be gullible to take the word of 
a stranger) and they started the conversation demanding personal information 
"for security purposes". I didn't watch Black Mirror but at times I feel like 
I'm living in an episode. This is a large corporation failing to see what's 
wrong with that — for starters, 
[the FCC tells consumers](https://www.fcc.gov/consumers/guides/deep-fake-audio-and-video-links-make-robocalls-and-scam-texts-harder-spot), 
"Don't answer calls from unknown numbers. 
Never give out personal information in response to unexpected calls…" 
The caller ID number can be 
[spoofed by scammers, the FCC also tells us](https://www.fcc.gov/spoofing). 

Sadder still, that I'm likely an outlier in making a fuss and most people comply so countless of these calls work day in and day out so nothing changes. I get how from their perspective they are the good guys doing this, but it isn't that hard to step into the customer's shoes and consider how these calls are received.

Corporations who do this, or outsource it, and doing society a huge disservice in normalizing poor security practice. They demand personal information for *their* "security purposes" but don't care one whit about the customer's security. Our communication systems have been so thoroughly manipulated and abused for dishonest monetization that incoming messages of any form — phone calls, email, SMS texts, you name it — are all essentially unsafe to respond to. "Don't call us, we'll call you" is our last resort. The FCC spoofing advisory even says that pressing a number on the keypad can indicate you are a potential "mark" for future calls, and that recording your voice saying "Yes" and "No" can be used to fraudulently create evidence you agreed to something over the phone.

Instead of attempting to train all citizens to be suspicious of all incoming calls, I suggest we fix the system that enables countless opportunities for scams. The communications industry has shirked their responsibility to steward a secure system, and no doubt due to intense lobbying our lawmakers have failed to regulate it properly so we can trust it. Securing our systems does incur a cost, just as reaching out to customers securely would for corporations (compared to cold calling), but investing in better infrastructure yields all kinds of long-term benefits lifting all boats.

Dumping the burden on the poor consumer to fend off all scammers is so much easier. Insecure systems always give advantages to the rich (who can afford to protect themselves with assistants) and the unscrupulous willing to bend the rules for their own profit, with the honest trusting (even gullible) citizen at a disadvantage every time. 

Reading that FCC guidance (first link above) is downright depressing. Maybe this is the best advice they can offer given the hostile reality of our phone and internet infrastructure and the levels of sophisticated scamming out there, but every point they suggest screams for better answers — and I don't think any of this is technically difficult to solve if we only committed to it.

* Don't answer calls from unknown numbers. *— If we are serious about this, our phones should block calls from numbers not in our contacts already, but I don't think people want to add new contacts before they can talk to anyone new.*  
* Never give out personal information… *— The use of personal information for authentication is so commonly abused we need to stop the practice. How many copies of our name/address etc. are out there that can all be reused by scammers if any leak? They say don't reuse passwords, but if these are used for authentication that's total reuse: why is this OK?*  
* Always proceed with caution when a caller is pressuring you for information or money immediately. *— I don't even know what this means, when is pressuring like this ever OK?*  
* If the caller claims to be a family member or friend in distress. Reach out to that person directly to confirm they need help before sending any money. *— This is one sentence oddly broken into two (typo?). If the caller claims to be traveling and lost their phone, how exactly do you "reach out" directly?*  
* If the caller claims to be celebrity or political figure …  do research online … *— Suggesting "research online" is laughable because the web is renowned for untrustworthy information. More typos (it should be "a celebrity", twice).*  
* If you get an inquiry from someone who says they represent a company or a government agency, hang up and call the phone number on your account statement, in the phone book, or on the company's or government agency's website to verify the authenticity … *— This not only places the burden on the victim, it's not very secure: web search could easily bring up a false phone number, fake statements are not hard to make with any color printer, and even phone books are printed by unlicensed publishers with no particular liability or security protocols. Furthermore, most corporations have numerous phone numbers and finding the right department and the extension of the person who called is like a needle in a haystack.*  
* Do not respond to any questions, especially those that can be answered with "Yes." *— This is crazy, the notion that saying a certain word and being recorded can be used against us. With generative AI even a short recording of our voice will be enough to fake how we sound saying "Yes' by now. Instead, we need to legislate that phone voice recordings are not acceptable as proof of agreement (instead send a document to be reviewed, signed, and returned).*  
* You may not be able to tell right away if an incoming call is spoofed. *— True, and many folks will be completely fooled. Since up top they said don't answer calls from unknown numbers, I assume this means we must be wary of spoofed calls faking the person caller ID tells us it is.*  
* Talk to your phone company about call blocking tools they may have and check into apps that you can download to your mobile device to block unwanted calls. *— Why aren't blocking tools the default, and is added protection sold at a cost further disadvantaging the less affluent? Mobile app stores constantly struggle with scam apps so how do we know this isn't inviting further damage? A secure should be the baseline for everyone, not something requiring the consumer do extra research and effort.*  
* Check out the FCC consumer guide on Call Blocking Tools and Resources, … *— The FCC won't even stand behind [their own recommendations](https://www.fcc.gov/call-blocking); their list of add-ons is footnoted, "\*The resources listed are provided for informational purposes. The FCC does not endorse any products or services listed, and is not responsible for the content, accuracy, completeness, or timeliness of non-FCC websites."*  
* To block telemarketing calls, register your number on the Do Not Call List. Legitimate telemarketers consult the list to avoid calling both landline and wireless phone numbers on the list. *— Why don't we get to be on the list by default and then opt in to get telemarketing? Obviously because nobody wants it, and it's well known that most people are too lazy, or ignorant of how to protect themselves. And this only protects us from the legitimate ones, big help\! Plus many kinds of unwanted calls (political campaigns, charities, debt collections, surveys) are still permitted by FCC rules. (There are [reports](https://thehill.com/homenews/nexstar\_media\_wire/4507529-why-being-on-the-do-not-call-list-doesnt-actually-stop-spam-calls-texts/) that unethical scammers use the Do Not Call list as rich targets.)*

We could have a "blue check mark" (the classic meaning from the heyday of Twitter, not X that just charges for it) caller ID that was reliable. We could have a national register of trustworthy companies and enterprises, with stated specific purposes they do place phone calls, all integrated into our phones as standard. It's nice that our phones allow us to report spam phone calls but we don't get to see what happens from there: status of our complaint, how many others reported that number, if any action was taken, statistics for our state and the country. And this would just be a start, surely many innovative mitigations are possible. 

I have no doubt most Americans are already annoyed and fed up, so it looks like a political failure. I don't know how to fix that; all I can think of is a strike, let's all just stop answering the phone unless we know who's calling and expect their call (I always welcome and expect calls from family and friends). Some legit calls will get missed, but they can send us a letter. It's a small sacrifice, but any protest always is, and I cannot remember ever getting an urgent unexpected call from a stranger that was very helpful. I hope this informal analysis makes clear that it isn't a technical challenge and we don't have to live with this mess any longer than we choose to by inaction.

