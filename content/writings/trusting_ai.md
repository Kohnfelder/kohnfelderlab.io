---
title: Trusting AI
date: 2024-06-03
tags: ["security"]
draft: false
---

* *(220 words)* June 2024 -- Loren Kohnfelder

Whether or not
[this unscientific test](https://boingboing.net/2024/06/03/google-ai-just-might-kill-you-it-misidentified-a-destroying-angel-mushroom-as-an-ediblebutton-mushroom.html)
is reliable, asking generative AI if a mushroom is safe to eat —
it misclassified a highly toxic variety that looks like a common edible one —
is a terrible idea if you are prepared to eat according to what it says.
This illustrates my rule of thumb:

> Only use generative AI when there exists only minor downsides if it's wrong.

Here's a good example of the application of this rule. Many online training
courses confirm that the student has learned the material by quizzing them.
Generative AI can select questions and then use their answers to select the
next question, skipping once mastery is demonstrated or asking more questions
where the student needs most help.
__NOTE__: This assumes human written
multiple-choice questions, checked by expert humans for correctness,
so there is no chance the AI misinterprets answers or asks screwy questions.
The worst cast is that AI asks too many or too few questions,
but the answers asked will be solid.

Some say it helps to ask different AIs, but how much more confidence
does that really give? Also how diverse are different LLM?
Same basic algorithm and they rarely disclose training data particulars
for comparison. Not to mention the danger of
[Model collapse](https://en.wikipedia.org/wiki/Model_collapse) as LLMs
unwittingly get trained on each other's output. Unless someone figures out
how to avoid model collapse, the latest models might be getting progressively
worse over time.

\#
