+++
title = "Collected Writings — Loren Kohnfelder"
date = 2024-05-26
draft = false
+++

Collected writings that may be of interest to some of the technically inclined.

I'm posting essays and opinion pieces about current events in the
software landscape here, occasionally linking from Mastodon to my
zero (rounded to the nearest hundred) follows there.

Usually these are lightly edited by myself,
so I welcome feedback via posting or private message me at 
[infosec.exchange@lmk](https://infosec.exchange/@lmk).
