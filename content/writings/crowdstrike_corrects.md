---
title: Crowdstrike further revelations
date: 2024-08-09
tags: ["security"]
draft: false
---


In a debunking blog post, Crowdstrike finally starts to describe that content files are digitally signed for deployment. The initial report oddly referenced file timestamps instead of hashes to designate the bad and good versions of the infamous Channel File 291, but now we know these were signed.

https://www.crowdstrike.com/blog/tech-analysis-addressing-claims-about-falcon-sensor-vulnerability/

We learn that content files are signed by a pinned certificate as a check of data authenticity. However, now that they have signed a bad Channel File 291 instance to fully close the door on a regression (which could be leveraged as an attack) they need to block the bad file being deployed again in a replay attack. Once they fix the bug in the kernel extension this may not be strictly necessary.

All of this is intended to correct the record about various speculative claims they dispute. While baseless claims piling on to all the trouble are not responsible, Crowdstrike has left a considerable information vacuum that inquisitive people will naturally fill as they can, "reading between the lines". Increased transparency from the start and more complete disclosure about the entire process surrounding the failure would have easily spared a lot of this.
