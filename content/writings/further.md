---
title: Further security discussions
date: 2024-05-30
tags: ["security"]
draft: false
---

* *(500 words)* May 2024 -- Loren Kohnfelder

> This article is a continuation of [Better Security Discussions](../better).

> This analysis can be extended by considering potential mitigations for additional threats.


Possibly some of these threats are already mitigated to an extent, if so we can de-emphasize further consideration accordingly. Some may have good mitigations that are easy to do: this suggests lobbying the software maker to implement these. Threats that appear hard to do anything about, or in cases inherent such that protection would reduce the value of the feature in the first place are most important and deserve the greatest attention. With thorough analysis if the threat is significant and mitigation looks hopeless, these seriously call into question the wisdom of the feature.

Let's consider the first threat listed above, making exploitation most effective should malware gain access. The Recall database of observations is reported to be accessible at user privilege level (system level access is not required), so any malware in a user-installed app or arbitrary-code-execution flaw in the browser can use it. What about mitigation? Locking down the database with system level access could be one idea, but it would add overhead and more importantly mean that using the Recall feature would require elevated privileges which makes it awkward for the intended user. Asymmetric cryptography might help (write data public key encrypted) but then you need to protect the private key somehow and it adds complexity and overhead. There seems to be a powerful wish for this feature to be easy for "good guys" to use but somehow off limits to "bad guys" but of course we don't have such protections.

Or consider the requirement for configuring all sensitive apps and websites. Users need to be more diligent and work harder is the obvious mitigation, but of course people don't like being blamed and spending more time keeping the software in line. If Recall is on by default surely some people won't notice or won't understand the implications and completely ignore it, incurring great potential harm. Even assuming the most assiduous user, before installing any new app must they make the effort to mentally consider its privacy impact and whether it needs exclusion from Recall — or the same before visiting any website for the first time? Seriously? Such security mechanisms are called block lists and it's well known that thoroughly excluding everything deserving is nearly impossible when the set of candidates is infinite. The secure way to do this is with an allow list, where safe items are enumerated: that way, an omission isn't harmful. It would be more secure for Recall to opt-in apps and websites suitable for recording, but this is probably a non-starter since it would be a lot of work just to get started. This isn't a suggestion but rather suggests that to make the feature work places an obligation to configure on the user while also incurring risk to the extent they don't do their job precisely.

Whether you agree with these takes, the details give us something to discuss productively. Interested readers can try their hand at mitigating these additional threats or find new ones.

-----
