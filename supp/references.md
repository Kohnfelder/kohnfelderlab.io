+++
title = "References"
draft = true
+++

# References

References and related material from the book, by chapter.

## Introduction

Read the original Microsoft technical article on threat modeling: The
threats to our products (April 1, 1999).
[https://adam.shostack.org/microsoft/The-Threats-To-Our-Products.docx](https://adam.shostack.org/microsoft/The-Threats-To-Our-Products.docx)

Learn all about the generally very useful skill of using thinking hats
from the book, *Six Thinking Hats* by Edward de Bono.
[http://www.debonogroup.com/six\_thinking\_hats.php](http://www.debonogroup.com/six_thinking_hats.php)

Explore the ancient computing history of the IBM System/360 and coding
in Basic Assembly Language.

* http://www.righto.com/2019/04/iconic-consoles-of-ibm-system360.html?m=1

* http://bitsavers.trailing-edge.com/pdf/ibm/360/asm/SC20-1646-6_int360asm_Aug70.pdf

[Over forty years later, I cannot sincerely recommend reading it, but
there is an archival copy of the author's
thesis:](http://bitsavers.trailing-edge.com/pdf/ibm/360/asm/SC20-1646-6_int360asm_Aug70.pdf)
*Towards a Practical Public Key Cryptosystem*; Massachusetts Institute
of Technology, 1978.
[https://dspace.mit.edu/handle/1721.1/15993]{.underline}

Learn about the "Princeton Word Macro Virus Loophole": *The History of
Internet Explorer* by Scott Schnoll.
[https://www2.cs.duke.edu/courses/spring01/cps049s/class/html/iehistory.htm](https://www2.cs.duke.edu/courses/spring01/cps049s/class/html/iehistory.htm)

Revisit the bad old days of rampant browser bugs: Internet Explorer
security issue summary (1996-2002).
[https://web.archive.org/web/20110714214614/http://www.nwnetworks.com/96-02iesecurity.htm](https://web.archive.org/web/20110714214614/http://www.nwnetworks.com/96-02iesecurity.htm)

Further thoughts on the role of the security expert: *Information
security and privacy "experts".*
[https://medium.com/\@lorenkohnfelder/information-security-and-privacy-experts-33368a9a1365]{.underline}

## Foundations

Read the newspaper report of the auto test drive that went bad: *Car
thief to pay for damages* by Caleb Loehrer (The Garden Island: Sunday,
April 7, 2019)
[https://www.thegardenisland.com/2019/04/07/hawaii-news/car-thief-to-pay-for-damages/](https://www.thegardenisland.com/2019/04/07/hawaii-news/car-thief-to-pay-for-damages/)

Learn about IMDB de-anonymization.
[https://www.cs.utexas.edu/\~shmat/shmat\_oak08netflix.pdf](https://www.cs.utexas.edu/~shmat/shmat_oak08netflix.pdf)

Read about Twitter security incident involving the logging of raw
passwords.
[https://blog.twitter.com/official/en\_us/topics/company/2018/keeping-your-account-secure.html](https://blog.twitter.com/official/en_us/topics/company/2018/keeping-your-account-secure.html)

## Threats

As an example of hardware as an asset to be attacked, it is believed
that the US created a cyber weapon that came to be known as Stuxnut that
was designed to infect industrial centrifuge equipment and control it in
such a way as to irreparably damage it.

An example of medical device exploits is MEDJACK:
[http://www.cs.tufts.edu/comp/116/archive/fall2018/smeggitt.pdf](http://www.cs.tufts.edu/comp/116/archive/fall2018/smeggitt.pdf)

A list of some prominent threat modeling methodologies:
[https://en.wikipedia.org/wiki/Threat\_model\#Generally\_accepted\_IT\_threat\_modeling\_processes](https://en.wikipedia.org/wiki/Threat_model#Generally_accepted_IT_threat_modeling_processes)

Details of the Facebook Beacon engagement ring fiasco:
[https://money.cnn.com/galleries/2010/technology/1012/gallery.5\_data\_breaches/3.html](https://money.cnn.com/galleries/2010/technology/1012/gallery.5_data_breaches/3.html)

Internet Explorer browser share before 2000:
[https://en.wikipedia.org/wiki/Usage\_share\_of\_web\_browsers\#Older\_reports\_(pre-2000](https://en.wikipedia.org/wiki/Usage_share_of_web_browsers#Older_reports_(pre-2000))

Microsoft threat modeling tools:
[https://docs.microsoft.com/en-us/azure/security/azure-security-threat-modeling-tool](https://docs.microsoft.com/en-us/azure/security/azure-security-threat-modeling-tool)
[https://github.com/Microsoft/threat-modeling-templates](https://github.com/Microsoft/threat-modeling-templates)
[https://en.wikipedia.org/wiki/Data\_flow\_diagram](https://en.wikipedia.org/wiki/Data_flow_diagram)

### STRIDE examples:

BGP hijacking
[https://www.cloudflare.com/learning/security/glossary/bgp-hijacking/](https://www.cloudflare.com/learning/security/glossary/bgp-hijacking/)

Superfish ad injection
[https://en.wikipedia.org/wiki/Superfish](https://en.wikipedia.org/wiki/Superfish)

Spectre/Meltdown
[https://meltdownattack.com/](https://meltdownattack.com/)

MemCrashed
[https://blog.cloudflare.com/memcrashed-major-amplification-attacks-from-port-11211/](https://blog.cloudflare.com/memcrashed-major-amplification-attacks-from-port-11211/)

xkcd's "Little Bobby Tables"
[https://xkcd.com/327/](https://xkcd.com/327/)

Read why Data Flow Diagrams (DFD) may be insufficiently detailed for
threat modeling.
[https://lirias.kuleuven.be/retrieve/571819](https://lirias.kuleuven.be/retrieve/571819)

## Mitigations

Read about Code Access Security, now deprecated by Microsoft.

## Patterns

The term "patterns" is loosely based on the classic architecture text,
"A Pattern Language: Towns, Buildings, Construction" by Christopher
Alexander, Sara Ishikawa and Murray Silverstein (1977).

Read about how a kid went right around Apple's enforcement of screen
time limits.
[https://www.reddit.com/r/ios/comments/g8upnn/i\_saw\_that\_my\_8yearold\_sister\_was\_on\_her\_iphone\_6/](https://www.reddit.com/r/ios/comments/g8upnn/i_saw_that_my_8yearold_sister_was_on_her_iphone_6/)

Learn about Web Assembly technology:
[https://webassembly.org/](https://webassembly.org/)

Read about secure execution of Web Assembly:
[https://webassembly.org/docs/security/](https://webassembly.org/docs/security/)

State of California Senate Bill No. 327 (2018):
[https://leginfo.legislature.ca.gov/faces/billTextClient.xhtml?bill\_id=201720180SB327](https://leginfo.legislature.ca.gov/faces/billTextClient.xhtml?bill_id=201720180SB327)

## Crypto

The Webcomic xkcd \#538 bluntly explains how attacks usually go around
crypto rather than break it.
[https://xkcd.com/538/](https://xkcd.com/538/)

The U.S. National Institute of Standards and Technology (NIST) publishes
detailed recommendations for cryptography algorithms and guidelines.
[https://csrc.nist.gov/Projects/Cryptographic-Standards-and-Guidelines](https://csrc.nist.gov/Projects/Cryptographic-Standards-and-Guidelines)

Read about Cloudflare's lava lamp collection that serves as a random
number generator.
[https://blog.cloudflare.com/randomness-101-lavarand-in-production/](https://blog.cloudflare.com/randomness-101-lavarand-in-production/)

Read about Navajo Code Talkers of WWII.
[https://americanindian.si.edu/education/codetalkers/html/chapter4.html](https://americanindian.si.edu/education/codetalkers/html/chapter4.html)

Read the original RSA paper: *A Method for Obtaining Digital Signatures
and Public-Key Cryptosystems*, R.L. Rivest, A. Shamir, and L. Adleman.
[https://people.csail.mit.edu/rivest/Rsapaper.pdf](https://people.csail.mit.edu/rivest/Rsapaper.pdf)

My published contribution is behind a paywall: *On the signature
reblocking problem in public-key cryptosystems*, L. Kohnfelder,
Communications of ACM 21 (1978), 120--126.

My thesis, *Towards a Practical Public-key Cryptosystem*, is so old I
cannot recommend reading it except as a historical source.
[https://dspace.mit.edu/handle/1721.1/15993]{.underline}

Read a review of popular crypto libraries that evaluates usability,
documentation, and the security of how they are used by test subjects.
[https://www.cs.umd.edu/class/fall2017/cmsc396H/papers/comparing-crypto-apis.pdf](https://www.cs.umd.edu/class/fall2017/cmsc396H/papers/comparing-crypto-apis.pdf)

Read about how security cameras unknowingly breach the privacy of their
customers. Images from motion detecting cameras are encrypted, but by
observing the presence or absence of traffic on the wire, attackers can
infer whether the house is occupied or not, because people inside
trigger the cameras.
[https://qmro.qmul.ac.uk/xmlui/bitstream/handle/123456789/65154/Tyson%20Your%20Privilege%20Gives%202020%20Accepted.pdf](https://qmro.qmul.ac.uk/xmlui/bitstream/handle/123456789/65154/Tyson Your Privilege Gives 2020 Accepted.pdf)

## Secure Design

The company that I refer to in this chapter was Elxsi (electronics ×
silicon).
[https://en.wikipedia.org/wiki/Elxsi](https://en.wikipedia.org/wiki/Elxsi)

Read more about "known knowns" including the original source.
[https://en.wikipedia.org/wiki/There\_are\_known\_knowns](https://en.wikipedia.org/wiki/There_are_known_knowns)
[
](http://archive.defense.gov/Transcripts/Transcript.aspx?TranscriptID=2636)
[]{.underline}

Read about the Equifax breach:
[https://www.equifaxsecurity2017.com/](https://www.equifaxsecurity2017.com/)

A study by Perrow concluded that the failure at Three Mile Island was a
consequence of the system\'s immense complexity.
[https://www.nature.com/articles/477404a](https://www.nature.com/articles/477404a)

## Secure Programming

Read Mitre's top security bug rankings by category.
[https://cwe.mitre.org/top25/archive/2020/2020\_cwe\_top25.html](https://cwe.mitre.org/top25/archive/2019/2019_cwe_top25.html)

See an excellent reference for securely using floating point math.
[https://floating-point-gui.de/](https://floating-point-gui.de/)

An overview of best practice for people's names.
[https://www.w3.org/International/questions/qa-personal-names](https://www.w3.org/International/questions/qa-personal-names)

In the 2020 Pwn2Own event, the \@SSLab\_Gatech team used 6 unique vulns
starting with a JIT bug and ending in a TOCTOU/race condition.
[https://twitter.com/thezdi/status/1240360442310074370](https://twitter.com/thezdi/status/1240360442310074370)

Learn all about Spectre and Meltdown attacks.
[https://meltdownattack.com/](https://meltdownattack.com/)

Learn more about regular expression backtracking that can incur heavy
computation costs.
[https://www.regular-expressions.info/catastrophic.html](https://www.regular-expressions.info/catastrophic.html)

The footgun mentioned as an example (= for ==) was used by an attacker
to insert malicious code into the Linux kernel in 2003. See *The Linux
Backdoor Attempt of 2003*, Felten, Ed, 2013-10-09,
[https://freedom-to-tinker.com/2013/10/09/the-linux-backdoor-attempt-of-2003/](https://freedom-to-tinker.com/2013/10/09/the-linux-backdoor-attempt-of-2003/)

More examples and discussion of footguns in the context of how to write
underhanded code can be found in *Initial Analysis of Underhanded Source
Code*, David A. Wheeler, IDA Document D-13166
[https://www.ida.org/-/media/feature/publications/i/in/initial-analysis-of-underhanded-source-code/d-13166.ashx](https://www.ida.org/-/media/feature/publications/i/in/initial-analysis-of-underhanded-source-code/d-13166.ashx)

Here are some links if you want to learn more about the complete details
of GoToFail, and you can find much more by searching its unique moniker,
or check out
[https://gotofail.com/](https://gotofail.com/)

[NIST vulnerability details:
<https://nvd.nist.gov/vuln/detail/CVE-2014-1266>]{.underline}

[Apple support posting:
<https://support.apple.com/en-us/HT202934>]{.underline}

[Speculation on this bug:
<https://www.cs.columbia.edu/~smb/blog/2014-02/2014-02-23.html>]{.underline}

[Source code (the excerpt in the book is based on the
SSLVerifySignedServerKeyExchange function.):
<https://opensource.apple.com/source/Security/Security-55471/libsecurity_ssl/lib/sslKeyExchange.c>]{.underline}

## Low-Level Flaws

Some C compilers provide non-standard assistance handling arithmetic
overflow. For example, see GNU Compiler Collection (GCC) builtin helper
functions documentation.
[https://gcc.gnu.org/onlinedocs/gcc/Integer-Overflow-Builtins.html](https://gcc.gnu.org/onlinedocs/gcc/Integer-Overflow-Builtins.html)

The JavaScript underflow example code is online.
[https://jsfiddle.net/j0mrLwp7/](https://jsfiddle.net/j0mrLwp7/)

### All about Heartbleed:

The Heartbleed bug has its own website.
[http://heartbleed.com/](http://heartbleed.com/)

The xkcd comic \#1354 has a terrific explanation of Heartbleed.
[https://xkcd.com/1354/](https://xkcd.com/1354/)

Read how "the Heartbleed bug affects 'almost everyone'" at
[https://www.digitaltrends.com/computing/internet-security-cloud-experts-weigh-heartbleed-bug/](https://www.digitaltrends.com/computing/internet-security-cloud-experts-weigh-heartbleed-bug/)

The TLS heartbeat protocol is defined in RFC 6520.
[https://tools.ietf.org/html/rfc6520](https://tools.ietf.org/html/rfc6520)

The fix for Heartbleed.
[https://git.openssl.org/gitweb/?p=openssl.git;a=commit;h=96db9023b881d7cd9f379b0c154650d6c108e9a3](https://git.openssl.org/gitweb/?p=openssl.git;a=commit;h=96db9023b881d7cd9f379b0c154650d6c108e9a3)

## Untrusted Inputs

The Webcomic xkcd \#327 nails a SQL injection example in just four
frames. [https://xkcd.com/327/](https://xkcd.com/327/)

UTR\#36: Unicode Security Considerations is a great detailed reference
of issues mentioned in the book.
[https://www.unicode.org/reports/tr36/](https://www.unicode.org/reports/tr36/)

The source of the story about the intramural softball team named "No
Game Scheduled" is Reddit, but factual or not, it's a brilliant example
of an injection attack. (Please only use this special power for good.)\
https://www.reddit.com/r/WhitePeopleTwitter/comments/lbnln3/name\_of\_our\_intramural\_softball\_team\_is\_no\_game/

## Web Security

There is a terrific compendium of Web browser security technical
details, but sadly it's no longer actively maintained and years behind
the curve. Find the archived Browser Security Handbook at
[https://code.google.com/archive/p/browsersec/wikis/Main.wiki](https://code.google.com/archive/p/browsersec/wikis/Main.wiki)

*The Tangled Web: A Guide to Securing Modern Web Applications* by Michal
Zalewski, based on the Browser Security Handbook, available.
[https://nostarch.com/tangledweb.htm](https://nostarch.com/tangledweb.htm)

Learn about Content Security Policy.
[https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP)

Learn about Cross-Origin Resource Sharing (CORS).
[https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)

Learn about *Let\'s Encrypt*, a free and open certificate authority
operated by the nonprofit Internet Security Research Group (ISRG) that
provides automated certificate issuance and renewal for webservers.
[https://letsencrypt.org](https://letsencrypt.org/)

Statistics showing the rapid growth of HTTPS are available at
[https://letsencrypt.org/stats/](https://letsencrypt.org/stats/)

Web technology is hyper competitive because pushing through a new
feature that gets into the world's browsers provides incredible
leverage. As a result, there are many competing standards bodies:

World Wide Web Consortium (W3C): HTTP, HTML, XHTML, CSS, DOM, PNG, SVG,
and more

Web Hypertext Application Technology Working Group (WHATWG): \"Living
Standard\" (a.k.a. HTML5) and more

Ecma International: JavaScript (also known as Ecma Script)

International Organization for Standardization (ISO): JPEG

Unicode Consortium: The Unicode Standard and Unicode Technical Reports
(UTRs)

Internet Engineering Task Force (IETF): Request for Comments (RFC)
documents

Internet Assigned Numbers Authority (IANA): Name and number registries

Read details on how domain names are treated as independent authorities.
[https://publicsuffix.org/](https://publicsuffix.org/)

## Security Testing

Learn about the MinUnit test framework used in C examples.
[https://github.com/siu/minunit](https://github.com/siu/minunit)

Read about the iOS jailbreak vulnerability regression [mentioned in the
book.
[https://arstechnica.com/information-technology/2019/08/ios-vulnerability-that-let-you-jailbreak-your-iphone-is-once-again-dead/](https://arstechnica.com/information-technology/2019/08/ios-vulnerability-that-let-you-jailbreak-your-iphone-is-once-again-dead/)

The XSS security test case example uses the excellent BeautifulSoup
library for HTML parsing (and it's a great XML parser, too).
[https://www.crummy.com/software/BeautifulSoup/bs4/doc/](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)

The 32 bit counter overflow refers to Basecamp's events table incident
of 2018.
[https://m.signalvnoise.com/postmortem-on-the-read-only-outage-of-basecamp-on-november-9th-2018/](https://m.signalvnoise.com/postmortem-on-the-read-only-outage-of-basecamp-on-november-9th-2018/)

The leak detection example refers to the 2018 Twitter security alert.
[https://blog.twitter.com/en\_us/topics/company/2018/keeping-your-account-secure.html](https://blog.twitter.com/en_us/topics/company/2018/keeping-your-account-secure.html)

## Secure Development Best practices

ARIANE 5 Flight 501 Failure Report:
[http://sunnyday.mit.edu/nasa-class/Ariane5-report.html](http://sunnyday.mit.edu/nasa-class/Ariane5-report.html)

This is why you must check carefully when you copy-paste code from the
Web: the most frequently copied code snippet on GitHub turned out to
have a bug. This wasn't a security bug, but it could have been.

Read about how "dependency confusion" allowed a security researcher to
inject code into dozens of organization's own code.
https://medium.com/\@alex.birsan/dependency-confusion-4a5d60fec610

Here is just one example of how a component intended to bolster security
can weaken it severely.\
https://nvd.nist.gov/vuln/detail/CVE-2019-3597

The Most Dangerous Code in the World:
[https://www.cs.utexas.edu/\~shmat/shmat\_ccs12.pdf](https://www.cs.utexas.edu/~shmat/shmat_ccs12.pdf)

Malicious look-alike libraries in PyPi:
[http://www.nbu.gov.sk/skcsirt-sa-20170909-pypi/](http://www.nbu.gov.sk/skcsirt-sa-20170909-pypi/)

## Looking Ahead

Read *Reflections on Trusting Trust* by Ken Thompson.
[https://www.cs.cmu.edu/\~rdriley/487/papers/Thompson\_1984\_ReflectionsonTrustingTrust.pdf](https://www.cs.cmu.edu/~rdriley/487/papers/Thompson_1984_ReflectionsonTrustingTrust.pdf)

Further detail about the design flaws in the automotive systems network
CAN bus protocol design.
[https://medium.com/\@lorenkohnfelder/unpatchable-security-flaws-8941c0cce790](https://medium.com/@lorenkohnfelder/unpatchable-security-flaws-8941c0cce790)

Examples of unhelpful security fix documentation (see Security section,
and following):
[https://support.apple.com/en-us/HT210919](https://support.apple.com/en-us/HT210919)

Read the full report on effective smartphone data security, "Data
Security on Mobile Devices: Current State of the Art, Open Problems, and
Proposed Solutions," from Johns Hopkins University at
https://securephones.io/

I have written about why the demands of law enforcement to provide
backdoor access only for the good guys is missing the point at
https://medium.com/\@lorenkohnfelder/lets-move-beyond-the-ominous-going-dark-f2f20a932ca9

