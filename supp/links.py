#!/usr/bin/env python3
# links: A web link checker and fixer
# Adapted from dokumi.py for Cory (he didn't want it)
# Copyright 2020 Loren Kohnfelder

import argparse
from bs4 import BeautifulSoup as BSoup
from bs4 import Comment
import csv
import datetime
import json
from http.server import BaseHTTPRequestHandler
import math
from pprint import PrettyPrinter
import re
import sys
import time
import urllib.request
from urllib.error import HTTPError, URLError
from urllib.parse import urlparse, urlunparse


"""This utility checks that links on the Web are working and still viable.
Older links have a way of breaking in various ways as websites are revamped,
change hands, backups lost, domain name registrations change, and many other
bumps in the road.

References:
https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#3xx_Redirection
https://archive.org/help/wayback_api.php

What dokumi does is check an existing web link for viability, including:
(not all implemented yet)
* the domain name still resolves
* the URL returns a valid web page (200)
* redirects may or may not be a sign of obselesence
* compare the contents of contemporaneous archived copy for similarity
* offer a working substitute link to archive.org/web

Given a URL, dokumi reports back what it found:
{original_url: the URL under inquiry,
 assessment: lgtm|working|blocked|changed|broken|lost|invalid,
 final_url: the same URL if it's fine or recommended replacement
}
What the assessment flavors mean:
| lgtm: "looks good to me" (should be a strong signal, minimal false positive)
| working: the URL works (no signal as to content stable or not)
| maybe|iffy: the URL (possibly changed) isn't very similar to archive original
| blocked: some sites give 403 to robots like this (like page works)
| changed: the URL has changed (redirect looks good)
| broken: definitely the old URL is broken and unusable (use archive link)
| lost: broken and no archive alternative was found
| invalid: not a valid HTTP(S) URL

Unique word similarity between page & archive is a strong signal if sim6 > 0.5;
often pages are similar between 0.5 > sim6 > 0.1 but this gray area can be
unreliable for pages with very little text (< 2000 words, because there are
and other cruft in even pure image content pages.

Future features:
* summary assessment
* async fetching of URLs for faster batch runs (may well not be needed)

"""

TIMEOUT_SEC = 15

USER_AGENT = "Mozilla/5.0 (X11; Linux armv7l) AppleWebKit/537.36 (KHTML,\
 like Gecko) Raspbian Chromium/78.0.3904.108 Chrome/78.0.3904.108\
 Safari/537.36"

NO_TYPE = '(none)'
# Hack to pass through failure to resolve domain name.
CONNECTION_ERROR = -99999

# Omit these 352 generic words from unique word list to get distinctive words.
DROP_WORDS = [
    'able', 'access', 'according', 'account', 'across',
    'action', 'actually', 'add', 'additional', 'address',
    'advertise', 'after', 'again', 'against', 'all',
    'allow', 'already', 'also', 'am', 'another',
    'anyone', 'anything', 'around', 'art', 'article',
    'asked', 'available', 'away', 'back', 'based',
    'because', 'become', 'before', 'being', 'best',
    'better', 'between', 'big', 'book', 'books',
    'both', 'buy', 'call', 'called', 'came',
    'can', 'cannot', 'care', 'cases', 'center',
    'change', 'changes', 'check', 'city', 'close',
    'com', 'come', 'coming', 'comments', 'companies',
    'conditions', 'continue', 'copy', 'create', 'created',
    'creative', 'crisis', 'current', 'data', 'day',
    'details', 'did', 'different', 'do', 'does',
    "doesn't", 'doing', 'don', 'donate', 'done', 'down',
    'during', 'each', 'earlier', 'early', 'education',
    'end', 'events', 'ever', 'every', 'everyone',
    'everything', 'example', 'face', 'fact', 'family',
    'far', 'features', 'few', 'find', 'first',
    'follow', 'food', 'form', 'found', 'free',
    'full', 'further', 'general', 'get', 'give',
    'given', 'global', 'go', 'good', 'got',
    'great', 'group', 'had', 'hard', 'has',
    'having', 'he', 'health', 'help', 'her',
    'here', 'high', 'his', 'house', 'how',
    'however', 'https', 'human', 'idea', 'images',
    'important', 'include', 'including', 'industry', 'info',
    'inside', 'instead', 'international', 'into', 'is', 'issues',
    "it's", 'its', 'join', 'just', 'keep', 'key',
    'kind', 'known', 'large', 'last', 'later',
    'latest', 'learn', 'least', 'leave', 'left',
    'legal', 'less', 'let', 'life', 'like',
    'likely', 'line', 'link', 'list', 'little',
    'live', 'living', 'log', 'long', 'look',
    'looking', 'lot', 'made', 'main', 'major',
    'make', 'makes', 'making', 'material', 'may',
    'me', 'menu', 'might', 'million', 'mobile',
    'month', 'more', 'most', 'much', 'must',
    'my', 'name', 'need', 'never', 'new',
    'news', 'next', 'now', 'number', 'off',
    'often', 'once', 'online', 'only', 'open',
    'order', 'original', 'others', 'our', 'out',
    'own', 'page', 'part', 'past', 'pay',
    'personal', 'place', 'please', 'pm', 'point',
    'possible', 'post', 'power', 'press', 'previous',
    'print', 'protect', 'provide', 'put', 'question',
    'questions', 'rather', 'read', 'real', 'really',
    'recent', 'related', 'report', 'required', 'reserved',
    'resources', 'response', 'review', 'right', 'run',
    'same', 'say', 'says', 'second', 'self',
    'sell', 'series', 'services', 'set', 'several',
    'share', 'she', 'should', 'show', 'sign',
    'simple', 'since', 'skip', 'small', 'something',
    'soon', 'source', 'special', 'staff', 'start',
    'state', 'statement', 'states', 'stay', 'still',
    'store', 'stories', 'story', 'subscribe', 'such',
    'support', 'system', 'take', 'taken', 'tech',
    'tell', 'than', 'the', 'them', 'then', 'there',
    'thing', 'things', 'think', 'this', 'those', 'though',
    'thought', 'three', 'through', 'time', 'times',
    'today', 'told', 'too', 'top', 'try',
    'trying', 'under', 'until', 'updated', 'updates',
    'us', 'use', 'used', 'user', 'users',
    'using', 'version', 'very', 'via', 'view',
    'want', 'watch', 'way', 'ways', 'website',
    'week', 'well', 'were', 'when', 'where',
    'whether', 'while', 'white', 'why', 'within',
    'without', 'workers', 'working', 'works', 'written',
    'wrote', 'year', 'years', 'you', 'your']


def vet_url(url):
    """Parse and vet an URL if dokumi will work with it or not.
    >>> vet_url("http://example.com:port/page.html?q=q")
    True
    >>> vet_url("https://example.com")
    True
    >>> vet_url("nothing://example.com")
    False
    >>> vet_url("junk@#)($@#)($*")
    False
    """
    try:
        parts = urlparse(url)
        if parts.scheme in ['http', 'https']:
            return True
    except ValueError:
        pass
    return False


hook_redirect = []


class HookHTTPRedirectHandler(urllib.request.HTTPRedirectHandler):
    """Hook the redirect handler to get the full chain of requests."""

    def redirect_request(self, req, fp, code, msg, headers, newurl):
        global hook_redirect
        hook_redirect.append((code, newurl))
        return super().redirect_request(req, fp, code, msg, headers, newurl)


def fetch_url(url):
    """Fetch the given url synchronously are return details of what happened.
    Returns (content, response): the first is the content at the URL;
    the second has various parameters characterizing aspects of the results.
    'actual_url': the actual URL after redirects
    'code': HTTP response code
    'content_len': HTTP content length
    'content_type': HTTP content type, e.g. 'text/html; charset=utf-8',
    'info': headers and other info
    'redirects': a list of details from any redirects incurred

    NOTE: example.com is special cased and used as a mockup for testing.

    >>> fetch_url("https://example.com")
    ('Testing 1, 2, 3', {'actual_url': 'https://example.com',\
 'redirects': [], 'code': 200, 'info': None})
    """
    if url.startswith('https://example.com'):
        actual_url = url
        return ("Testing 1, 2, 3",
                {'actual_url': actual_url,
                 'redirects': [],
                 'code': 200,
                 'info': None})
    results = {'actual_url': None, 'code': None, 'info': None, 'redirects': []}
    content = None
    try:
        # Hook HTTP redirects
        global hook_redirect
        hook_redirect = []
        opener = urllib.request.build_opener(HookHTTPRedirectHandler)
        urllib.request.install_opener(opener)

        request = urllib.request.Request(url, data=None,
                                         headers={'User-Agent': USER_AGENT})
        response = urllib.request.urlopen(request, None, TIMEOUT_SEC)
        content = response.read()
        results['actual_url'] = response.geturl()
        results['code'] = response.getcode()
        results['redirects'] = hook_redirect
        headers = response.info().items()
        results['info'] = headers
        for header in headers:
            header_name = header[0].casefold()
            header_value = header[1].casefold()
            if header_name == 'content-type':
                results['content_type'] = header_value
            elif header_name == 'content-length':
                results['content_len'] = header_value

    except HTTPError as fail:
        results['code'] = fail.code
        results['info'] = fail.reason if hasattr(
            fail, 'reason') else repr(fail)
    except TimeoutError as oserr:
        results['code'] = 0
        results['info'] = os.strerror if oserr.strerror else "urlopen timeout"
    except URLError as fail:
        results['code'] = CONNECTION_ERROR if (
            fail.errno is None) else fail.errno
        results['info'] = fail.reason if hasattr(
            fail, 'reason') else repr(fail)
    except ConnectionResetError as fail:
        results['code'] = CONNECTION_ERROR
        results['info'] = fail.reason if hasattr(
            fail, 'reason') else repr(fail)

    return (content, results)


def word_sequence(text, filter=None):
    """Break text into word list for fingerprinting.
    Return list of words (lower cased) that pass the filter if provided.
    >>> s = "This isn't not a, test!! This is-only 4 testing."
    >>> word_sequence(s)
    ['this', "isn't", 'not', 'a', 'test', 'this', 'is-only', 'testing']
    >>> word_sequence(s, lambda word: len(word) > 1)
    ['this', "isn't", 'not', 'test', 'this', 'is-only', 'testing']

    """
    regex = re.compile(r"[^-'a-z]+")
    words = []
    for line in text.split():
        for word in regex.split(line.casefold()):
            if filter is None or filter(word):
                if len(word) > 0:
                    words.append(word)
    return words


def try_wayback(url, vintage=None):
    """Try a Wayback machine url alternative, pegged to vintage date.
    Returns alternative available URL or None.
    """
    params = 'url=' + url
    if vintage:
        params += '&timestamp=' + vintage.strftime('%Y%m%d')
    wayback_url = urlunparse(['https', 'archive.org', 'wayback/available',
                              '', params, None])
    (content, results) = fetch_url(wayback_url)
    if results['code'] != 200:
        sys.stderr.write('*** Wayback machine error: %s\n' % repr(results))
        return None
    wayback_api = json.loads(content)
    snapshots = wayback_api['archived_snapshots']
    if 'closest' in snapshots:
        closest = snapshots['closest']
        if closest['available'] and closest['status'] == '200':
            return closest['url']
    return None


def html_parse_lite(html):
    """Parse HTML file and remove extraneous stuff: script and CSS.
    >>> soup = html_parse_lite("<html><body><p>Test, yes!</p>\
<script>script, no thanks!</script><style>Omit CSS!</style></body></html>")
    >>> str(soup)
    '<html><body><p>Test, yes!</p></body></html>'
    """
    soup = BSoup(html, 'html.parser')
    for elt in soup(["script", "style"]):
        elt.decompose()
    return soup


def fingerprint_content(html):
    """Parse content into fingerprint for heft and similarity measure.
    Excises content between Wayback Machine insert if present.
    (<!-- BEGIN/END WAYBACK TOOLBAR INSERT -->)
    >>> fingerprint_content('')
    []
    >>> fingerprint_content('<html><body><p>This is a test</p></body></html>')
    ['test']
    >>> wbm = '<html><body><!-- BEGIN WAYBACK TOOLBAR INSERT -->'
    >>> wbm += '<div id="WBM">blah, blah<p>random</p></div>'
    >>> wbm += '<!-- END WAYBACK TOOLBAR INSERT -->'
    >>> wbm += '<p>This is a 7 harder test</p></body></html>'
    >>> fingerprint_content(''.join(wbm))
    ['harder', 'test']
    >>> fingerprint_content("<html><body><p>Now is the time 4 all...</p>\
<script>window.open('something')</script></body></html>")
    []

    """
    words = []
    soup = html_parse_lite(html)
    html = soup.find('html')
    if html:
        comments = html.find_all(
            string=lambda text: isinstance(text, Comment))
        for comment in comments:
            if comment.string.strip() == 'BEGIN WAYBACK TOOLBAR INSERT':
                elt = comment
                while elt:
                    next = elt.next_sibling
                    elt.extract()
                    elt = next
                    if isinstance(elt.string, Comment):
                        if (elt.string.strip() ==
                                'END WAYBACK TOOLBAR INSERT'):
                            break
                break
        text = html.get_text('\n', strip=True)
        words = word_sequence(text,
                              lambda word: (len(word) > 1 and
                                            word not in DROP_WORDS))
    return words


def dokumi(url, verbose=False):
    """Check an url for viability.
    Assessments: lgtm (positive), working (neutral), blocked (401/403),
                 changed (redirected), broken (negative), lost, invalid.
    The url is first classified as: working, blocked or broken, or changed.
    If working, archive is compared: similar => lgtm, mismatch => different.
    If blocked or broken, archive url should be used; if no archive => lost.
    If changed, archive is compared: if mismatch => different.
    >>> dokumi("nothing://example.com")
    {'original_url': 'nothing://example.com', \
'assessment': 'invalid', 'final_url': None}
    >>> dokumi("junk!")['assessment']
    'invalid'
    """
    asmt = {'original_url': url,
            'assessment': None,
            'final_url': None}
    if not vet_url(url):
        asmt['assessment'] = 'invalid'
        return asmt

    diag = []
    wc = uwc = 0
    time_started = time.time()
    if verbose:
        print(" < " + url)
    (content, response) = fetch_url(url)
    if verbose:
        print(" > " + repr(response))
    time_loaded = time.time()
    asmt['load_time'] = round(time_loaded - time_started, 1)
    if 'redirects' in response:
        diag.append({'redirects': response['redirects']})
    asmt['response'] = response
    asmt['info'] = response['info']
    code = response['code']
    assess = 'broken'
    if code == 200:
        assess = 'working'
        content_type = response.get('content_type', NO_TYPE)
        asmt['content_type'] = content_type
        asmt['content_len'] = response.get('content_len', 0)
        if content_type.startswith('text/html'):
            words = fingerprint_content(content)
            digest = set(words)
            asmt['wc'] = wc = len(words)
            asmt['uwc'] = uwc = len(digest)
        if url != response['actual_url']:
            assess = 'changed'
            asmt['final_url'] = response['actual_url']
        else:
            asmt['final_url'] = url
    else:
        if code > 0:
            msg = BaseHTTPRequestHandler.responses.get(code, '???')
            diag.append('HTTP error %d (%s)' % (code, msg))
        elif code == CONNECTION_ERROR:
            diag.append('Connection error (DNS lookup or timeout)')
        if code in [401, 402, 403, 410]:
            access = 'blocked'  # unauth, pay, forbidden, not found, gone
        elif code in [400, 404, 406]:
            access = 'invalid'
    # Try Wayback machine
    archival = try_wayback(url)
    if archival:
        (content, response) = fetch_url(archival)
        acode = response['code']
        if acode == 200:
            asmt['archive_url'] = archival
            content_type = response.get('content_type', NO_TYPE)
            if content_type.startswith('text/html'):
                arch_words = fingerprint_content(content)
                arch_digest = set(arch_words)
                asmt['awc'] = awc = len(arch_words)
                asmt['auwc'] = auwc = len(arch_digest)
                if wc > 0 and uwc > 0 and awc > 0 and auwc > 0:
                    similar1 = 1 - abs((float(awc)-float(wc)) / float(awc))
                    similar2 = 1 - abs((float(auwc)-float(uwc)) / float(auwc))
                    similar3 = (similar1+similar2) * 0.5
                    set_digest = set(digest)
                    set_arch_digest = set(arch_digest)
                    set_common = set_digest & set_arch_digest
                    similar4 = float(len(set_common)) / float(auwc)
                    similar5 = float(len(set_common)) / float(uwc)
                    similar6 = (similar4+similar5) * 0.5
                    asmt['sim1'] = similar1
                    asmt['sim2'] = similar2
                    asmt['sim3'] = similar3
                    asmt['sim4'] = similar4
                    asmt['sim5'] = similar5
                    asmt['sim6'] = similar6
                    # Fine tune assessment
                    if similar6 > 0.5:
                        assess = 'lgtm'
                    elif similar6 <= 0.15:
                        assess = 'iffy'
                    else:
                        assess = 'maybe'
    else:
        if code != 200:
            assess = 'lost'
    asmt['assessment'] = assess
    asmt['diagnostic'] = diag
    return asmt


def make_word_list(urls, verbose=False):
    """Make a common word list: compute number of files top words are in."""
    doc_words = []
    counts = {}
    for url in urls:
        if verbose:
            print("===== %s =====" % url)
        (content, response) = fetch_url(url)
        code = response['code']
        if code == 200:
            content_type = response.get('content_type', NO_TYPE)
            if content_type.startswith('text/html'):
                words = fingerprint_content(content)
                digest = set(words)
                # Tally up common words document-pairwise
                for word in digest:
                    for i in range(len(doc_words)):
                        if word in doc_words[i]:
                            counts[word] = 1 + counts.get(word, 0)
                doc_words.append(digest)
    if verbose:
        print("===== word list (%d) =====" % len(doc_words))
    ascend_counts = {k: v for k, v in sorted(
        counts.items(), key=lambda item: item[1])}
    cutoff = int(math.sqrt(len(doc_words)))
    for (k, v) in ascend_counts.items():
        if v >= cutoff:
            print("%4d: %s" % (v, k))


def checkfile(handle, quiet=False, dump=False, list=False, verbose=False):
    for line in handle:
        line = line.rstrip('\n')
        m = re.match(r'^https*://', line, re.IGNORECASE)
        # URL at start of line is the whole line
        if m:
            url = line
            checkurl(url, quiet=quiet, dump=dump, list=list, verbose=verbose)
            continue
        # URL within the line, typically at the end of the line
        m = re.search(r'\shttps*://[^\s\)\]]*', line, re.IGNORECASE)
        if m:
            url = m.group(0)[1:]
            checkurl(url, quiet=quiet, dump=dump, list=list, verbose=verbose)
            continue
        # URL markdown embed of form [link text](https://example.com)
        m = re.search(r'\]\((https*://[^\)]*)\)', line, re.IGNORECASE)
        if m:
            url = m.group(1)
            checkurl(url, quiet=quiet, dump=dump, list=list, verbose=verbose)
            continue


def checkurl(url, quiet=False, dump=False, list=False, verbose=False):
    if list:
        print("URL = %s" % url)
        return
    asmt = dokumi(url, verbose=verbose)
    if asmt['assessment'] in ('working', 'lgtm'):
        if not quiet:
            print("OK: %s" % url)
    else:
        print("%s\n%10s %s" %
              (url, asmt['assessment'], asmt['final_url']))
        if dump:
            print(repr(asmt))
    sys.stdout.flush()


def main(argv=None):
    parser = argparse.ArgumentParser(
        description="""Scan markdown files for links and check them.""",
        epilog="Run without arguments for doctests only."
    )
    parser.add_argument('-u', '--url',
                        help="URL mode: arguments are URLs, not pathnames",
                        action='store_true')
    parser.add_argument('-q', '--quiet', help="suppresses error checking",
                        action='store_true')
    parser.add_argument('-d', '--dump', help="dump assessment to output",
                        action='store_true')
    parser.add_argument('-l', '--list', help="list URLs without assessing",
                        action='store_true')
    parser.add_argument('-v', '--verbose', help="verbose output debug aid",
                        action='store_true')
    parser.add_argument('pathnames', nargs='*',
                        help="document (ODT) file path name(s)")
    args = parser.parse_args(argv)

    # -u URL checking
    if args.url:
        for url in args.pathnames:
            checkurl(url, quiet=args.quiet, dump=args.dump,
                     list=args.list, verbose=args.verbose)
    # Markdown file scan for URLs to check
    else:
        for path in args.pathnames:
            if path.endswith('.md'):
                if args.dump:
                    print("===== %s =====" % path)
                with open(path, 'r') as handle:
                    checkfile(handle, quiet=args.quiet, dump=args.dump,
                              list=args.list, verbose=args.verbose)
            else:
                print("%s: Unsupported file extension" % path)


if __name__ == "__main__":
    import doctest
    if doctest.testmod(verbose=False).failed == 0:
        main(sys.argv[1:])
